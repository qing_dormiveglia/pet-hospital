/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.26 : Database - pet_ums
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pet_ums` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `pet_ums`;

/*Table structure for table `gen_table` */

DROP TABLE IF EXISTS `gen_table`;

CREATE TABLE `gen_table` (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成业务表';

/*Table structure for table `gen_table_column` */

DROP TABLE IF EXISTS `gen_table_column`;

CREATE TABLE `gen_table_column` (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成业务表字段';

/*Table structure for table `ums_chat_friends` */

DROP TABLE IF EXISTS `ums_chat_friends`;

CREATE TABLE `ums_chat_friends` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `member_id` bigint DEFAULT NULL COMMENT '用户id',
  `friend_member_id` bigint DEFAULT NULL COMMENT '用户的好友id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `my_user_id` (`member_id`,`friend_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='好友表';

/*Table structure for table `ums_chat_friends_request` */

DROP TABLE IF EXISTS `ums_chat_friends_request`;

CREATE TABLE `ums_chat_friends_request` (
  `id` bigint NOT NULL COMMENT 'id',
  `send_member_id` bigint DEFAULT NULL COMMENT '发送用户id',
  `accept_member_id` bigint DEFAULT NULL COMMENT '接收用户id',
  `request_date_time` datetime DEFAULT NULL COMMENT '发送请求的事件',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='发送事件表';

/*Table structure for table `ums_chat_msg` */

DROP TABLE IF EXISTS `ums_chat_msg`;

CREATE TABLE `ums_chat_msg` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `send_member_id` bigint DEFAULT NULL COMMENT '发送用户id',
  `accept_member_id` bigint DEFAULT NULL COMMENT '接收用户id',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '信息',
  `sign_flag` tinyint(1) DEFAULT NULL COMMENT '消息是否签收状态\r\n1：签收\r\n0：未签收\r\n',
  `create_time` datetime DEFAULT NULL COMMENT '发送请求的事件',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='消息表';

/*Table structure for table `ums_member` */

DROP TABLE IF EXISTS `ums_member`;

CREATE TABLE `ums_member` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户表主键',
  `member_level_id` bigint DEFAULT NULL COMMENT '会员等级id',
  `open_info_id` bigint DEFAULT NULL COMMENT '用户的openid(微信小程序)',
  `union_id` bigint DEFAULT NULL COMMENT '用户的union_id（用户唯一性）',
  `phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '手机号',
  `username` varchar(255) DEFAULT NULL COMMENT '账号(其他类型账号)',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `header_image` varchar(255) DEFAULT NULL COMMENT '头像',
  `gender` tinyint(1) DEFAULT NULL COMMENT '性别',
  `birth` date DEFAULT NULL COMMENT '生日',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  `job` varchar(255) DEFAULT NULL COMMENT '职业',
  `sign` varchar(255) DEFAULT NULL COMMENT '个性签名',
  `integration` int DEFAULT NULL COMMENT '积分',
  `growth` int DEFAULT NULL COMMENT '成长值',
  `status` tinyint(1) DEFAULT NULL COMMENT '启用状态',
  `type` tinyint(1) DEFAULT NULL COMMENT '账号类型',
  `auth_status` tinyint DEFAULT NULL COMMENT '认证状态',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近登录时间',
  `account_permission` tinyint(1) DEFAULT NULL COMMENT '账号权限',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `session_key` varchar(255) DEFAULT NULL COMMENT '用户会话密钥',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `auth_time` datetime DEFAULT NULL COMMENT '授权时间',
  `auth_credential` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '一键登录凭证',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_member_address` */

DROP TABLE IF EXISTS `ums_member_address`;

CREATE TABLE `ums_member_address` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户收货地址表主键',
  `member_id` bigint DEFAULT NULL COMMENT '用户id',
  `name` varchar(255) DEFAULT NULL COMMENT '收货人姓名',
  `phone` char(11) DEFAULT NULL COMMENT '电话',
  `post_code` varchar(255) DEFAULT NULL COMMENT '邮政编码',
  `province` varchar(255) DEFAULT NULL COMMENT '省/直辖市',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  `region` varchar(255) DEFAULT NULL COMMENT '区',
  `detail_address` varchar(255) DEFAULT NULL COMMENT '详细地址(街道)',
  `areacode` varchar(255) DEFAULT NULL COMMENT '省市区代码',
  `default_status` tinyint(1) DEFAULT NULL COMMENT '是否默认',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_member_image_auth` */

DROP TABLE IF EXISTS `ums_member_image_auth`;

CREATE TABLE `ums_member_image_auth` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户认证图集表主键',
  `member_info_id` bigint DEFAULT NULL COMMENT '用户详情表id',
  `url` varchar(500) DEFAULT NULL COMMENT '图片地址',
  `type` tinyint(1) DEFAULT NULL COMMENT '图片类型',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_member_info` */

DROP TABLE IF EXISTS `ums_member_info`;

CREATE TABLE `ums_member_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户详情表主键',
  `member_id` bigint DEFAULT NULL COMMENT '用户id',
  `certificates_type` varchar(3) DEFAULT NULL COMMENT '证件类型',
  `certificates_no` varchar(32) DEFAULT NULL COMMENT '证件编号',
  `certificates_url` varchar(255) DEFAULT NULL COMMENT '证件路径',
  `auth_status` tinyint(1) DEFAULT NULL COMMENT '认证状态',
  `full_name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_member_level` */

DROP TABLE IF EXISTS `ums_member_level`;

CREATE TABLE `ums_member_level` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户等级表主键',
  `name` varchar(255) DEFAULT NULL COMMENT '等级名称',
  `growth_point` int DEFAULT NULL COMMENT '等级需要的成长值',
  `default_status` tinyint(1) DEFAULT NULL COMMENT '默认等级',
  `free_freight_point` decimal(18,4) DEFAULT NULL COMMENT '免运费标准',
  `comment_growth_point` int DEFAULT NULL COMMENT '每次评价获取的成长值',
  `priviledge_free_freight` tinyint(1) DEFAULT NULL COMMENT '是否有免邮特权',
  `priviledge_member_price` tinyint(1) DEFAULT NULL COMMENT '是否有会员价格特权',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_member_login_log` */

DROP TABLE IF EXISTS `ums_member_login_log`;

CREATE TABLE `ums_member_login_log` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户登录日志表主键',
  `member_id` bigint DEFAULT NULL COMMENT '用户id',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip地址',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  `login_type` tinyint(1) DEFAULT NULL COMMENT '登录类型',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_pet_cure` */

DROP TABLE IF EXISTS `ums_pet_cure`;

CREATE TABLE `ums_pet_cure` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '宠物治疗类型表主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_pet_images` */

DROP TABLE IF EXISTS `ums_pet_images`;

CREATE TABLE `ums_pet_images` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '宠物图集表主键',
  `pet_info_id` bigint DEFAULT NULL COMMENT '宠物id',
  `name` varchar(255) DEFAULT NULL COMMENT '图片名称',
  `image` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_pet_kind` */

DROP TABLE IF EXISTS `ums_pet_kind`;

CREATE TABLE `ums_pet_kind` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '宠物种类表主键',
  `name` varchar(255) DEFAULT NULL COMMENT '种类名称',
  `type_name` varchar(255) DEFAULT NULL COMMENT '类型(如哺乳动物)',
  `content` varchar(255) DEFAULT NULL COMMENT '描述',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `ums_pet_label` */

DROP TABLE IF EXISTS `ums_pet_label`;

CREATE TABLE `ums_pet_label` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '宠物标签表主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '标签名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `usm_pet_info` */

DROP TABLE IF EXISTS `usm_pet_info`;

CREATE TABLE `usm_pet_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '宠物基本信息表主键',
  `member_id` bigint DEFAULT NULL COMMENT '用户id',
  `pet_kind_id` bigint DEFAULT NULL COMMENT '宠物种类id',
  `pet_cure_id` bigint DEFAULT NULL COMMENT '治疗类型表id',
  `pet_label_ids` varchar(255) DEFAULT NULL COMMENT '宠物标签ids(通过，相隔)',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `header_image` varchar(255) DEFAULT NULL COMMENT '头像',
  `gender` tinyint(1) DEFAULT NULL COMMENT '性别',
  `vaccine` tinyint(1) DEFAULT NULL COMMENT '疫苗接种状态',
  `birth_type` tinyint(1) DEFAULT NULL COMMENT '成年状态',
  `story` varchar(255) DEFAULT NULL COMMENT '故事',
  `disease` varchar(255) DEFAULT NULL COMMENT '病情描述',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
