/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.26 : Database - pet_hms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pet_hms` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `pet_hms`;

/*Table structure for table `gen_table` */

DROP TABLE IF EXISTS `gen_table`;

CREATE TABLE `gen_table` (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成业务表';

/*Data for the table `gen_table` */

insert  into `gen_table`(`table_id`,`table_name`,`table_comment`,`sub_table_name`,`sub_table_fk_name`,`class_name`,`tpl_category`,`package_name`,`module_name`,`business_name`,`function_name`,`function_author`,`gen_type`,`gen_path`,`options`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values 
(1,'hms_dep','',NULL,NULL,'Dep','crud','com.qing.hospital','hospital','dep',NULL,'zzww','0','/',NULL,'admin','2024-02-07 23:25:31','',NULL,NULL),
(2,'hms_doctor','',NULL,NULL,'Doctor','crud','com.qing.hospital','hospital','doctor',NULL,'zzww','0','/',NULL,'admin','2024-02-07 23:25:31','',NULL,NULL),
(3,'hms_drug','',NULL,NULL,'Drug','crud','com.qing.hospital','hospital','drug',NULL,'zzww','0','/',NULL,'admin','2024-02-07 23:25:31','',NULL,NULL),
(4,'hms_drug_type','',NULL,NULL,'DrugType','crud','com.qing.hospital','hospital','type',NULL,'zzww','0','/',NULL,'admin','2024-02-07 23:25:31','',NULL,NULL),
(5,'hms_hospital','',NULL,NULL,'Hospital','crud','com.qing.hospital','hospital','hospital',NULL,'zzww','0','/',NULL,'admin','2024-02-07 23:25:31','',NULL,NULL),
(6,'hms_images','图集表',NULL,NULL,'Images','crud','com.qing.hospital','hospital','images','图集','zzww','0','/',NULL,'admin','2024-02-07 23:25:32','',NULL,NULL),
(7,'hms_order_info','',NULL,NULL,'OrderInfo','crud','com.qing.hospital','hospital','info',NULL,'zzww','0','/',NULL,'admin','2024-02-07 23:25:32','',NULL,NULL),
(8,'hms_schedule','',NULL,NULL,'Schedule','crud','com.qing.hospital','hospital','schedule',NULL,'zzww','0','/',NULL,'admin','2024-02-07 23:25:32','',NULL,NULL),
(9,'hms_tag','',NULL,NULL,'Tag','crud','com.qing.hospital','hospital','tag',NULL,'zzww','0','/',NULL,'admin','2024-02-07 23:25:32','',NULL,NULL);

/*Table structure for table `gen_table_column` */

DROP TABLE IF EXISTS `gen_table_column`;

CREATE TABLE `gen_table_column` (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成业务表字段';

/*Data for the table `gen_table_column` */

insert  into `gen_table_column`(`column_id`,`table_id`,`column_name`,`column_comment`,`column_type`,`java_type`,`java_field`,`is_pk`,`is_increment`,`is_required`,`is_insert`,`is_edit`,`is_list`,`is_query`,`query_type`,`html_type`,`dict_type`,`sort`,`create_by`,`create_time`,`update_by`,`update_time`) values 
(1,1,'id','科室ID','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:31','',NULL),
(2,1,'name','名称','varchar(255)','String','name','0','0',NULL,'1','1','1','1','LIKE','input','',2,'admin','2024-02-07 23:25:31','',NULL),
(3,1,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',3,'admin','2024-02-07 23:25:31','',NULL),
(4,1,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',4,'admin','2024-02-07 23:25:31','',NULL),
(5,1,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',5,'admin','2024-02-07 23:25:31','',NULL),
(6,1,'is_delete','逻辑删除，0为未删除，1为删除','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',6,'admin','2024-02-07 23:25:31','',NULL),
(7,2,'id','医生ID','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:31','',NULL),
(8,2,'member_id','用户ID','bigint','Long','memberId','0','0',NULL,'1','1','1','1','EQ','input','',2,'admin','2024-02-07 23:25:31','',NULL),
(9,2,'dep_id','科室ID','bigint','Long','depId','0','0',NULL,'1','1','1','1','EQ','input','',3,'admin','2024-02-07 23:25:31','',NULL),
(10,2,'hospital_id','医院ID','bigint','Long','hospitalId','0','0',NULL,'1','1','1','1','EQ','input','',4,'admin','2024-02-07 23:25:31','',NULL),
(11,2,'name','姓名','varchar(255)','String','name','0','0',NULL,'1','1','1','1','LIKE','input','',5,'admin','2024-02-07 23:25:31','',NULL),
(12,2,'position','职位','varchar(255)','String','position','0','0',NULL,'1','1','1','1','EQ','input','',6,'admin','2024-02-07 23:25:31','',NULL),
(13,2,'qualifications','资历','varchar(255)','String','qualifications','0','0',NULL,'1','1','1','1','EQ','input','',7,'admin','2024-02-07 23:25:31','',NULL),
(14,2,'introduce','医生介绍','varchar(255)','String','introduce','0','0',NULL,'1','1','1','1','EQ','input','',8,'admin','2024-02-07 23:25:31','',NULL),
(15,2,'skill','擅长','text','String','skill','0','0',NULL,'1','1','1','1','EQ','textarea','',9,'admin','2024-02-07 23:25:31','',NULL),
(16,2,'amount','挂号费','decimal(10,0)','Long','amount','0','0',NULL,'1','1','1','1','EQ','input','',10,'admin','2024-02-07 23:25:31','',NULL),
(17,2,'header_image','头像','varchar(255)','String','headerImage','0','0',NULL,'1','1','1','1','EQ','imageUpload','',11,'admin','2024-02-07 23:25:31','',NULL),
(18,2,'heat','热度','int','Long','heat','0','0',NULL,'1','1','1','1','EQ','input','',12,'admin','2024-02-07 23:25:31','',NULL),
(19,2,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',13,'admin','2024-02-07 23:25:31','',NULL),
(20,2,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',14,'admin','2024-02-07 23:25:31','',NULL),
(21,2,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',15,'admin','2024-02-07 23:25:31','',NULL),
(22,2,'is_delete','逻辑删除，0为未删除，1为删除','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',16,'admin','2024-02-07 23:25:31','',NULL),
(23,3,'id','药品id','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:31','',NULL),
(24,3,'city_id','城市id','bigint','Long','cityId','0','0',NULL,'1','1','1','1','EQ','input','',2,'admin','2024-02-07 23:25:31','',NULL),
(25,3,'hospital_id','医院id','bigint','Long','hospitalId','0','0',NULL,'1','1','1','1','EQ','input','',3,'admin','2024-02-07 23:25:31','',NULL),
(26,3,'pet_kind_id','适用宠物类型（0为通用）id','bigint','Long','petKindId','0','0',NULL,'1','1','1','1','EQ','input','',4,'admin','2024-02-07 23:25:31','',NULL),
(27,3,'drug_type_id','药品类型id','bigint','Long','drugTypeId','0','0',NULL,'1','1','1','1','EQ','input','',5,'admin','2024-02-07 23:25:31','',NULL),
(28,3,'tag_ids','逗号隔开','varchar(1024)','String','tagIds','0','0',NULL,'1','1','1','1','EQ','textarea','',6,'admin','2024-02-07 23:25:31','',NULL),
(29,3,'name','名称','varchar(255)','String','name','0','0',NULL,'1','1','1','1','LIKE','input','',7,'admin','2024-02-07 23:25:31','',NULL),
(30,3,'english_name','英文名','varchar(255)','String','englishName','0','0',NULL,'1','1','1','1','LIKE','input','',8,'admin','2024-02-07 23:25:31','',NULL),
(31,3,'head_image','头图地址','varchar(255)','String','headImage','0','0',NULL,'1','1','1','1','EQ','imageUpload','',9,'admin','2024-02-07 23:25:31','',NULL),
(32,3,'price','价格','decimal(10,2)','BigDecimal','price','0','0',NULL,'1','1','1','1','EQ','input','',10,'admin','2024-02-07 23:25:31','',NULL),
(33,3,'type','（0处方药1非处方药）','tinyint(1)','Integer','type','0','0',NULL,'1','1','1','1','EQ','select','',11,'admin','2024-02-07 23:25:31','',NULL),
(34,3,'medical','(0医保范围1不在医保范围)','tinyint(1)','Integer','medical','0','0',NULL,'1','1','1','1','EQ','input','',12,'admin','2024-02-07 23:25:31','',NULL),
(35,3,'usage','用法用量','varchar(1024)','String','usage','0','0',NULL,'1','1','1','1','EQ','textarea','',13,'admin','2024-02-07 23:25:31','',NULL),
(36,3,'taboo','禁忌(逗号隔开)','varchar(1024)','String','taboo','0','0',NULL,'1','1','1','1','EQ','textarea','',14,'admin','2024-02-07 23:25:31','',NULL),
(37,3,'approval_number','药品批准文号','varchar(255)','String','approvalNumber','0','0',NULL,'1','1','1','1','EQ','input','',15,'admin','2024-02-07 23:25:31','',NULL),
(38,3,'expiration_date','保质期','varchar(255)','String','expirationDate','0','0',NULL,'1','1','1','1','EQ','input','',16,'admin','2024-02-07 23:25:31','',NULL),
(39,3,'storage_method','储存方法','varchar(255)','String','storageMethod','0','0',NULL,'1','1','1','1','EQ','input','',17,'admin','2024-02-07 23:25:31','',NULL),
(40,3,'adverse_reaction','不良反应','varchar(255)','String','adverseReaction','0','0',NULL,'1','1','1','1','EQ','input','',18,'admin','2024-02-07 23:25:31','',NULL),
(41,3,'specification','规格','varchar(255)','String','specification','0','0',NULL,'1','1','1','1','EQ','input','',19,'admin','2024-02-07 23:25:31','',NULL),
(42,3,'ingredient','成分','varchar(255)','String','ingredient','0','0',NULL,'1','1','1','1','EQ','input','',20,'admin','2024-02-07 23:25:31','',NULL),
(43,3,'cure','主治','varchar(255)','String','cure','0','0',NULL,'1','1','1','1','EQ','input','',21,'admin','2024-02-07 23:25:31','',NULL),
(44,3,'coactions','药品相互作用','varchar(255)','String','coactions','0','0',NULL,'1','1','1','1','EQ','input','',22,'admin','2024-02-07 23:25:31','',NULL),
(45,3,'content','描述','text','String','content','0','0',NULL,'1','1','1','1','EQ','editor','',23,'admin','2024-02-07 23:25:31','',NULL),
(46,3,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',24,'admin','2024-02-07 23:25:31','',NULL),
(47,3,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',25,'admin','2024-02-07 23:25:31','',NULL),
(48,3,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',26,'admin','2024-02-07 23:25:31','',NULL),
(49,3,'is_delete','逻辑删除，0为未删除，1为删除','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',27,'admin','2024-02-07 23:25:31','',NULL),
(50,4,'id','药品类型id','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:31','',NULL),
(51,4,'p_id','父id','bigint','Long','pId','0','0',NULL,'1','1','1','1','EQ','input','',2,'admin','2024-02-07 23:25:31','',NULL),
(52,4,'value','值','varchar(255)','String','value','0','0',NULL,'1','1','1','1','EQ','input','',3,'admin','2024-02-07 23:25:31','',NULL),
(53,4,'type','类型（0一级父类1二级父类2子类）','tinyint(1)','Integer','type','0','0',NULL,'1','1','1','1','EQ','select','',4,'admin','2024-02-07 23:25:31','',NULL),
(54,4,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',5,'admin','2024-02-07 23:25:31','',NULL),
(55,4,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',6,'admin','2024-02-07 23:25:31','',NULL),
(56,4,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',7,'admin','2024-02-07 23:25:31','',NULL),
(57,4,'is_delete','逻辑删除，0为未删除，1为删除','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',8,'admin','2024-02-07 23:25:31','',NULL),
(58,5,'id','医院id','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:31','',NULL),
(59,5,'city_id','城市id','bigint','Long','cityId','0','0',NULL,'1','1','1','1','EQ','input','',2,'admin','2024-02-07 23:25:31','',NULL),
(60,5,'grade_id','等级id','bigint','Long','gradeId','0','0',NULL,'1','1','1','1','EQ','input','',3,'admin','2024-02-07 23:25:31','',NULL),
(61,5,'dep_ids','科室表id（逗号隔开）','varchar(1024)','String','depIds','0','0',NULL,'1','1','1','1','EQ','textarea','',4,'admin','2024-02-07 23:25:31','',NULL),
(62,5,'tag','标签id（逗号隔开）','varchar(255)','String','tag','0','0',NULL,'1','1','1','1','EQ','input','',5,'admin','2024-02-07 23:25:32','',NULL),
(63,5,'name','名称','varchar(255)','String','name','0','0',NULL,'1','1','1','1','LIKE','input','',6,'admin','2024-02-07 23:25:32','',NULL),
(64,5,'grade','等级','varchar(255)','String','grade','0','0',NULL,'1','1','1','1','EQ','input','',7,'admin','2024-02-07 23:25:32','',NULL),
(65,5,'introduce','介绍','varchar(255)','String','introduce','0','0',NULL,'1','1','1','1','EQ','input','',8,'admin','2024-02-07 23:25:32','',NULL),
(66,5,'address','地址','varchar(255)','String','address','0','0',NULL,'1','1','1','1','EQ','input','',9,'admin','2024-02-07 23:25:32','',NULL),
(67,5,'heat','热度','int','Long','heat','0','0',NULL,'1','1','1','1','EQ','input','',10,'admin','2024-02-07 23:25:32','',NULL),
(68,5,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',11,'admin','2024-02-07 23:25:32','',NULL),
(69,5,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',12,'admin','2024-02-07 23:25:32','',NULL),
(70,5,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',13,'admin','2024-02-07 23:25:32','',NULL),
(71,5,'is_delete','逻辑删除，0为未删除，1为删除','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',14,'admin','2024-02-07 23:25:32','',NULL),
(72,6,'id','图集ID','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:32','',NULL),
(73,6,'name','名称','varchar(255)','String','name','0','0',NULL,'1','1','1','1','LIKE','input','',2,'admin','2024-02-07 23:25:32','',NULL),
(74,6,'url','路径','varchar(255)','String','url','0','0',NULL,'1','1','1','1','EQ','input','',3,'admin','2024-02-07 23:25:32','',NULL),
(75,6,'type','类型','tinyint(1)','Integer','type','0','0',NULL,'1','1','1','1','EQ','select','',4,'admin','2024-02-07 23:25:32','',NULL),
(76,6,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',5,'admin','2024-02-07 23:25:32','',NULL),
(77,6,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',6,'admin','2024-02-07 23:25:32','',NULL),
(78,6,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',7,'admin','2024-02-07 23:25:32','',NULL),
(79,6,'is_delete','逻辑删除，0为未删除，1为删除。','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',8,'admin','2024-02-07 23:25:32','',NULL),
(80,7,'id','挂号订单详情ID','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:32','',NULL),
(81,7,'schedule_id','排班ID','bigint','Long','scheduleId','0','0',NULL,'1','1','1','1','EQ','input','',2,'admin','2024-02-07 23:25:32','',NULL),
(82,7,'member_id','就诊动物主人ID','bigint','Long','memberId','0','0',NULL,'1','1','1','1','EQ','input','',3,'admin','2024-02-07 23:25:32','',NULL),
(83,7,'number','预约号序','int','Long','number','0','0',NULL,'1','1','1','1','EQ','input','',4,'admin','2024-02-07 23:25:32','',NULL),
(84,7,'fetch_time','建议取票时间','varchar(255)','String','fetchTime','0','0',NULL,'1','1','1','1','EQ','input','',5,'admin','2024-02-07 23:25:32','',NULL),
(85,7,'fetch_address','取票地址','varchar(255)','String','fetchAddress','0','0',NULL,'1','1','1','1','EQ','input','',6,'admin','2024-02-07 23:25:32','',NULL),
(86,7,'amount','服务费用','decimal(10,0)','Long','amount','0','0',NULL,'1','1','1','1','EQ','input','',7,'admin','2024-02-07 23:25:32','',NULL),
(87,7,'pay_time','支付时间','datetime','Date','payTime','0','0',NULL,'1','1','1','1','EQ','datetime','',8,'admin','2024-02-07 23:25:32','',NULL),
(88,7,'quit_time','退号时间','datetime','Date','quitTime','0','0',NULL,'1','1','1','1','EQ','datetime','',9,'admin','2024-02-07 23:25:32','',NULL),
(89,7,'order_status','订单状态','tinyint(1)','Integer','orderStatus','0','0',NULL,'1','1','1','1','EQ','radio','',10,'admin','2024-02-07 23:25:32','',NULL),
(90,7,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',11,'admin','2024-02-07 23:25:32','',NULL),
(91,7,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',12,'admin','2024-02-07 23:25:32','',NULL),
(92,7,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',13,'admin','2024-02-07 23:25:32','',NULL),
(93,7,'is_delete','逻辑删除，0为未删除，1为删除','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',14,'admin','2024-02-07 23:25:32','',NULL),
(94,8,'id','排班ID','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:32','',NULL),
(95,8,'hospital_id','医院ID','bigint','Long','hospitalId','0','0',NULL,'1','1','1','1','EQ','input','',2,'admin','2024-02-07 23:25:32','',NULL),
(96,8,'dep_id','科室ID','bigint','Long','depId','0','0',NULL,'1','1','1','1','EQ','input','',3,'admin','2024-02-07 23:25:32','',NULL),
(97,8,'work_date','排班日期','date','Date','workDate','0','0',NULL,'1','1','1','1','EQ','datetime','',4,'admin','2024-02-07 23:25:32','',NULL),
(98,8,'work_time','排班时间（0上午1下午）','tinyint(1)','Integer','workTime','0','0',NULL,'1','1','1','1','EQ','input','',5,'admin','2024-02-07 23:25:32','',NULL),
(99,8,'reserved_number','可以预约数','int','Long','reservedNumber','0','0',NULL,'1','1','1','1','EQ','input','',6,'admin','2024-02-07 23:25:32','',NULL),
(100,8,'available_number','剩余预约数','int','Long','availableNumber','0','0',NULL,'1','1','1','1','EQ','input','',7,'admin','2024-02-07 23:25:32','',NULL),
(101,8,'status','排班状态（-1：停诊 0：停约 1：可约）','tinyint(1)','Integer','status','0','0',NULL,'1','1','1','1','EQ','radio','',8,'admin','2024-02-07 23:25:32','',NULL),
(102,8,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',9,'admin','2024-02-07 23:25:32','',NULL),
(103,8,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',10,'admin','2024-02-07 23:25:32','',NULL),
(104,8,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',11,'admin','2024-02-07 23:25:32','',NULL),
(105,8,'is_delete','逻辑删除，0为未删除，1为删除','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',12,'admin','2024-02-07 23:25:32','',NULL),
(106,9,'id','标签id','bigint','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2024-02-07 23:25:32','',NULL),
(107,9,'name','名称','varchar(255)','String','name','0','0',NULL,'1','1','1','1','LIKE','input','',2,'admin','2024-02-07 23:25:32','',NULL),
(108,9,'type','类型（0医生1医院2药品）','tinyint(1)','Integer','type','0','0',NULL,'1','1','1','1','EQ','select','',3,'admin','2024-02-07 23:25:32','',NULL),
(109,9,'remark','备注','varchar(255)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',4,'admin','2024-02-07 23:25:32','',NULL),
(110,9,'update_time','修改时间','datetime','Date','updateTime','0','0',NULL,'1','1',NULL,NULL,'EQ','datetime','',5,'admin','2024-02-07 23:25:32','',NULL),
(111,9,'create_time','创建时间','datetime','Date','createTime','0','0',NULL,'1',NULL,NULL,NULL,'EQ','datetime','',6,'admin','2024-02-07 23:25:32','',NULL),
(112,9,'is_delete','逻辑删除，0为未删除，1为删除','tinyint(1)','Integer','isDelete','0','0',NULL,'1','1','1','1','EQ','input','',7,'admin','2024-02-07 23:25:32','',NULL);

/*Table structure for table `hms_dep` */

DROP TABLE IF EXISTS `hms_dep`;

CREATE TABLE `hms_dep` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '科室ID',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1790621286364454914 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `hms_dep` */

insert  into `hms_dep`(`id`,`name`,`remark`,`create_time`,`update_time`,`is_delete`) values 
(1,'骨科','测试','2024-02-12 16:08:46',NULL,0),
(2,'皮肤科',NULL,'2024-02-12 17:48:17',NULL,0),
(3,'妇科',NULL,'2024-02-12 17:48:20',NULL,0),
(4,'眼科',NULL,'2024-02-12 17:48:22',NULL,0),
(5,'口腔科',NULL,'2024-02-12 17:48:23',NULL,0),
(6,'性病科',NULL,'2024-02-12 17:48:25',NULL,0),
(7,'神经外科',NULL,'2024-02-12 17:48:27',NULL,0),
(8,'儿科',NULL,'2024-02-12 17:48:29',NULL,0),
(9,'内科',NULL,'2024-02-12 17:48:31',NULL,0),
(10,'肛肠科',NULL,'2024-02-12 17:48:33',NULL,0),
(1790621286364454913,'宠物科','测试','2024-02-12 16:08:46',NULL,0);

/*Table structure for table `hms_doctor` */

DROP TABLE IF EXISTS `hms_doctor`;

CREATE TABLE `hms_doctor` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '医生ID',
  `member_id` bigint NOT NULL COMMENT '用户ID',
  `dep_id` bigint NOT NULL COMMENT '科室ID',
  `hospital_id` bigint NOT NULL COMMENT '医院ID',
  `city_id` bigint NOT NULL COMMENT '城市id',
  `tag` varchar(255) DEFAULT NULL COMMENT '标签（用逗号隔开）(1,2,5)',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `region` varchar(255) DEFAULT NULL COMMENT '区',
  `position` varchar(255) DEFAULT NULL COMMENT '职位',
  `qualifications` varchar(255) DEFAULT NULL COMMENT '资历',
  `introduce` varchar(255) DEFAULT NULL COMMENT '医生介绍',
  `skill` text COMMENT '擅长',
  `amount` decimal(10,0) DEFAULT NULL COMMENT '挂号费',
  `header_image` varchar(255) DEFAULT NULL COMMENT '头像',
  `heat` int DEFAULT NULL COMMENT '热度',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `hms_doctor` */

insert  into `hms_doctor`(`id`,`member_id`,`dep_id`,`hospital_id`,`city_id`,`tag`,`name`,`region`,`position`,`qualifications`,`introduce`,`skill`,`amount`,`header_image`,`heat`,`remark`,`create_time`,`update_time`,`is_delete`) values 
(1,1,1,1,2,'1,2','赵医生','花果园区·第一人民医院','猫科动物主治医生','中国中医药大学硕士研究生毕业,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗','中国中医药大学硕士研究生毕业,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗','猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育.阿莫西林（羟氨苄青霉素）用于治疗伤寒、其他沙门菌感染和伤寒带菌者，敏感菌引起的尿路感染以及肺炎球菌、不产青霉素酶的金黄色葡萄球菌、溶血性链球菌、流 …\r\n临床表现,阿莫西林（羟氨苄青霉素）用于治疗伤寒、其他沙门菌感染和伤寒带菌者，敏感菌引起的尿路感染以及肺炎球菌、不产青霉素酶的金黄色葡萄球菌、溶血性链球菌、流 …\r\n临床表现',10,'https://img3.redocn.com/20110413/Redocn_2011041114222070.jpg',1,'aa','2024-02-10 15:46:45',NULL,0),
(2,2,1,1,2,'1,2','刘孜然','花果园区·第一人民医院','哺乳动物主治医师','中国中医药大学硕士研究生毕业,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗','中国中医药大学硕士研究生毕业,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗','猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育.阿莫西林（羟氨苄青霉素）用于治疗伤寒、其他沙门菌感染和伤寒带菌者，敏感菌引起的尿路感染以及肺炎球菌、不产青霉素酶的金黄色葡萄球菌、溶血性链球菌、流 …\r\n临床表现,阿莫西林（羟氨苄青霉素）用于治疗伤寒、其他沙门菌感染和伤寒带菌者，敏感菌引起的尿路感染以及肺炎球菌、不产青霉素酶的金黄色葡萄球菌、溶血性链球菌、流 …\r\n临床表现',12,'https://photo-static-api.fotomore.com/creative/vcg/veer/612/veer-311053606.jpg',2,NULL,'2024-03-12 00:13:01',NULL,0),
(3,3,3,1,2,'1,2','刘明伟','花果园区·第一人民医院','哺乳动物副主治医师','中国中医药大学硕士研究生毕业,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗','中国中医药大学硕士研究生毕业,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗','猫科动物绝育、接生、猫科动物胸腔医疗、骨子康复治疗,猫科动物绝育.阿莫西林（羟氨苄青霉素）用于治疗伤寒、其他沙门菌感染和伤寒带菌者，敏感菌引起的尿路感染以及肺炎球菌、不产青霉素酶的金黄色葡萄球菌、溶血性链球菌、流 …\r\n临床表现,阿莫西林（羟氨苄青霉素）用于治疗伤寒、其他沙门菌感染和伤寒带菌者，敏感菌引起的尿路感染以及肺炎球菌、不产青霉素酶的金黄色葡萄球菌、溶血性链球菌、流 …\r\n临床表现',8,'http://img.aiimg.com/uploads/userup/0909/091AK54103.jpg',1,NULL,'2024-03-12 01:26:29',NULL,0);

/*Table structure for table `hms_drug` */

DROP TABLE IF EXISTS `hms_drug`;

CREATE TABLE `hms_drug` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '药品id',
  `city_id` bigint DEFAULT NULL COMMENT '城市id',
  `hospital_id` bigint DEFAULT NULL COMMENT '医院id',
  `pet_kind_id` bigint DEFAULT NULL COMMENT '适用宠物类型（0为通用）id',
  `drug_type_id` bigint DEFAULT NULL COMMENT '药品类型id',
  `tag_ids` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '逗号隔开',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `region` varchar(255) DEFAULT NULL COMMENT '区',
  `english_name` varchar(255) DEFAULT NULL COMMENT '英文名',
  `head_image` varchar(255) DEFAULT NULL COMMENT '头图地址',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `type` tinyint(1) DEFAULT NULL COMMENT '（0处方药1非处方药）',
  `medical` tinyint(1) DEFAULT NULL COMMENT '(0医保范围1不在医保范围)',
  `usage` varchar(1024) DEFAULT NULL COMMENT '用法用量',
  `taboo` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '禁忌(逗号隔开)',
  `approval_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '药品批准文号',
  `expiration_date` varchar(255) DEFAULT NULL COMMENT '保质期',
  `storage_method` varchar(255) DEFAULT NULL COMMENT '储存方法',
  `adverse_reaction` varchar(255) DEFAULT NULL COMMENT '不良反应',
  `specification` varchar(255) DEFAULT NULL COMMENT '规格',
  `ingredient` varchar(255) DEFAULT NULL COMMENT '成分',
  `cure` varchar(255) DEFAULT NULL COMMENT '主治',
  `coactions` varchar(255) DEFAULT NULL COMMENT '药品相互作用',
  `content` text COMMENT '描述',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `hms_drug` */

insert  into `hms_drug`(`id`,`city_id`,`hospital_id`,`pet_kind_id`,`drug_type_id`,`tag_ids`,`name`,`region`,`english_name`,`head_image`,`price`,`type`,`medical`,`usage`,`taboo`,`approval_number`,`expiration_date`,`storage_method`,`adverse_reaction`,`specification`,`ingredient`,`cure`,`coactions`,`content`,`remark`,`update_time`,`create_time`,`is_delete`) values 
(1,2,1,1,2,'5,6','阿莫西林','南明区·第一人民医院','amxlabkamasl','https://qing-gulimall-upload.oss-cn-fuzhou.aliyuncs.com/2024/05/16/9b666d27a54448f6a3dc67b8105c4d431715814198557.jpeg',23.30,1,0,'一天三次，口服，一次一粒','治疗伤寒、其他沙门菌感染和伤寒带菌者，敏感菌引起的尿路感染以及肺炎球菌、不产青霉素酶的金黄色葡萄球菌、溶血性链球菌','12321452amxla1132','520天','常温储存','无','一盒5片，一片15粒','阿莫西林','治疗伤寒、其他沙门菌感染和伤寒带菌者，敏感菌引起的尿路感染以及肺炎球菌、不产青霉素酶的金黄色葡萄球菌、溶血性链球菌','无','阿莫西林治疗消炎药','测试',NULL,'2024-02-12 22:19:18',0);

/*Table structure for table `hms_drug_type` */

DROP TABLE IF EXISTS `hms_drug_type`;

CREATE TABLE `hms_drug_type` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '药品类型id',
  `p_id` bigint DEFAULT NULL COMMENT '父id',
  `value` varchar(255) DEFAULT NULL COMMENT '值',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型（0一级父类1二级父类2子类）',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1790861814855106563 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `hms_drug_type` */

insert  into `hms_drug_type`(`id`,`p_id`,`value`,`type`,`remark`,`create_time`,`update_time`,`is_delete`) values 
(1,0,'消炎药',0,NULL,'2024-02-12 22:19:54',NULL,0),
(2,1,'口服药',1,NULL,'2024-02-12 22:20:25',NULL,0),
(3,1,'贴膏',1,'',NULL,NULL,NULL);

/*Table structure for table `hms_hospital` */

DROP TABLE IF EXISTS `hms_hospital`;

CREATE TABLE `hms_hospital` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '医院id',
  `city_id` bigint DEFAULT NULL COMMENT '城市id',
  `grade_id` bigint DEFAULT NULL COMMENT '等级id',
  `dep_ids` varchar(1024) DEFAULT NULL COMMENT '科室表id（逗号隔开）',
  `tag` varchar(255) DEFAULT NULL COMMENT '标签id（逗号隔开）',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `head_image` varchar(255) DEFAULT NULL COMMENT '头图',
  `region` varchar(255) DEFAULT NULL COMMENT '区',
  `grade` varchar(255) DEFAULT NULL COMMENT '等级',
  `introduce` varchar(255) DEFAULT NULL COMMENT '介绍',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `heat` int DEFAULT NULL COMMENT '热度',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `hms_hospital` */

insert  into `hms_hospital`(`id`,`city_id`,`grade_id`,`dep_ids`,`tag`,`name`,`head_image`,`region`,`grade`,`introduce`,`address`,`heat`,`remark`,`update_time`,`create_time`,`is_delete`) values 
(1,2,1,'1,2,3,4,5,6,7,8,9,10,1790621286364454913','3,4','第一人民医院','https://www.womanhospital.cn/upload/images/2019/9/29142747700.jpg','南明区·第一人民医院','省一级','对医院分级管理的依据是医院的功能、任务、设施条件、技术建设、医疗服务质量和科学管理的综合水平。','贵州省贵阳市南明区第一人民医院',1,'测试','2024-02-12 16:11:20',NULL,0);

/*Table structure for table `hms_images` */

DROP TABLE IF EXISTS `hms_images`;

CREATE TABLE `hms_images` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '图集ID',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型（0-广告）',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '逻辑删除，0为未删除，1为删除。',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='图集表';

/*Data for the table `hms_images` */

insert  into `hms_images`(`id`,`name`,`url`,`type`,`remark`,`create_time`,`update_time`,`is_delete`) values 
(1,'广告1','https://img.redocn.com/sheji/20151023/yiyuanxuanchuanguanggaozhanban_5167367.jpg',0,NULL,'2024-02-08 23:54:58',NULL,0),
(2,'广告2','http://img.aiimg.com/uploads/userup/1203/051143234034.jpg',0,NULL,'2024-02-13 17:05:21',NULL,0);

/*Table structure for table `hms_medical_record` */

DROP TABLE IF EXISTS `hms_medical_record`;

CREATE TABLE `hms_medical_record` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `hospital_id` bigint DEFAULT NULL COMMENT '医院id',
  `ID_code` varchar(255) DEFAULT NULL COMMENT '病例编号',
  `pet_id` bigint DEFAULT NULL COMMENT '宠物id',
  `member_id` bigint DEFAULT NULL COMMENT '用户id',
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `gender` tinyint(1) DEFAULT NULL COMMENT '性别(0-男1-女)',
  `age` int DEFAULT NULL COMMENT '年龄',
  `category` varchar(50) DEFAULT NULL COMMENT '类别',
  `chief_complaint` text COMMENT '主诉',
  `past_history` text COMMENT '过去史',
  `family_history` text COMMENT '家族史',
  `allergy_history` text COMMENT '过敏史',
  `present_illness` text COMMENT '现病史',
  `physical_examination` text COMMENT '体检',
  `diagnosis` text COMMENT '诊断',
  `suggestion` text COMMENT '建议',
  `attending_physician` varchar(50) DEFAULT NULL COMMENT '就诊医生',
  `date` date DEFAULT NULL COMMENT '日期',
  `treatment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '处置',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `logical_delete` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `hms_medical_record` */

insert  into `hms_medical_record`(`id`,`hospital_id`,`ID_code`,`pet_id`,`member_id`,`name`,`gender`,`age`,`category`,`chief_complaint`,`past_history`,`family_history`,`allergy_history`,`present_illness`,`physical_examination`,`diagnosis`,`suggestion`,`attending_physician`,`date`,`treatment`,`create_time`,`update_time`,`logical_delete`,`remark`) values 
(1,1,'RMYY_3001ZG',NULL,NULL,'橘子',0,2,'急诊','心脏暂停','无','无','无','无','无','感冒','休息','赵医生','2024-04-26','一瓶5cc点滴','2024-04-26 12:51:37','2024-04-26 14:06:30',0,NULL),
(2,1,'RMYY_3001ZG',NULL,NULL,'小庙',1,2,'住院','绝育','无','无','无','无','无','发情期','一瓶5cc点滴','黄医生','2024-04-26','一瓶5cc点滴','2024-04-26 12:54:21','2024-04-26 14:07:00',0,NULL);

/*Table structure for table `hms_order_info` */

DROP TABLE IF EXISTS `hms_order_info`;

CREATE TABLE `hms_order_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '挂号订单详情ID',
  `schedule_id` bigint DEFAULT NULL COMMENT '排班ID',
  `member_id` bigint DEFAULT NULL COMMENT '就诊动物主人ID',
  `number` int DEFAULT NULL COMMENT '预约号序',
  `fetch_time` varchar(255) DEFAULT NULL COMMENT '建议取票时间',
  `fetch_address` varchar(255) DEFAULT NULL COMMENT '取票地址',
  `amount` decimal(10,0) DEFAULT NULL COMMENT '服务费用',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `quit_time` datetime DEFAULT NULL COMMENT '退号时间',
  `order_status` tinyint(1) DEFAULT NULL COMMENT '订单状态',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `hms_order_info` */

insert  into `hms_order_info`(`id`,`schedule_id`,`member_id`,`number`,`fetch_time`,`fetch_address`,`amount`,`pay_time`,`quit_time`,`order_status`,`remark`,`update_time`,`create_time`,`is_delete`) values 
(4,1,1,1,'2024-05-01上午10点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-04-30 02:43:24',0),
(6,1,1,2,'2024-05-01上午12点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-04-30 03:00:03',0),
(7,3,1,1,'2024-05-17下午4点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:33:23',0),
(8,1,1,1,'2024-05-17上午10点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:34:07',0),
(9,2,1,1,'2024-05-18下午3点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:05',0),
(10,2,1,2,'2024-05-18下午4点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:08',0),
(11,2,1,3,'2024-05-18下午5点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:08',0),
(12,2,1,4,'2024-05-18下午6点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:16',0),
(13,2,1,5,'2024-05-18下午7点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:16',0),
(14,1,1,2,'2024-05-17上午12点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:16',0),
(15,2,1,6,'2024-05-18下午8点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:16',0),
(16,2,1,7,'2024-05-18下午9点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:20',0),
(17,2,1,8,'2024-05-18下午10点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:20',0),
(18,1,1,3,'2024-05-17上午14点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:20',0),
(19,2,1,9,'2024-05-18下午11点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:20',0),
(20,2,1,10,'2024-05-18下午12点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 02:42:20',0),
(21,2,1,1,'2024-05-19下午3点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 04:39:13',0),
(22,2,1,2,'2024-05-19下午4点0分','贵州省贵阳市南明区第一人民医院，按照指示牌提示取票',12,NULL,NULL,0,NULL,NULL,'2024-05-16 09:37:36',0);

/*Table structure for table `hms_schedule` */

DROP TABLE IF EXISTS `hms_schedule`;

CREATE TABLE `hms_schedule` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '排班ID',
  `hospital_id` bigint DEFAULT NULL COMMENT '医院ID',
  `dep_id` bigint DEFAULT NULL COMMENT '科室ID',
  `doctor_id` bigint DEFAULT NULL COMMENT '医生id',
  `work_date` date DEFAULT NULL COMMENT '排班日期',
  `work_time` tinyint(1) DEFAULT NULL COMMENT '排班时间（0上午1下午）',
  `reserved_number` int DEFAULT NULL COMMENT '可以预约数',
  `available_number` int DEFAULT NULL COMMENT '剩余预约数',
  `work` varchar(255) DEFAULT NULL COMMENT '周',
  `begin_time` date DEFAULT NULL COMMENT '预计开始时间',
  `end_time` date DEFAULT NULL COMMENT '预计结束时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '排班状态（-1：停诊 0：停约 1：可约）',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='医生排班表';

/*Data for the table `hms_schedule` */

insert  into `hms_schedule`(`id`,`hospital_id`,`dep_id`,`doctor_id`,`work_date`,`work_time`,`reserved_number`,`available_number`,`work`,`begin_time`,`end_time`,`status`,`remark`,`create_time`,`update_time`,`is_delete`) values 
(1,1,1,2,'2024-05-21',0,2,2,NULL,NULL,NULL,1,NULL,'2024-02-12 16:13:08','2024-05-16 02:42:20',0),
(2,1,1,2,'2024-05-19',1,4,2,NULL,NULL,NULL,1,NULL,'2024-02-12 16:13:43','2024-05-16 09:37:36',0),
(3,1,1,2,'2024-05-20',1,2,2,NULL,NULL,NULL,1,NULL,NULL,'2024-05-16 02:33:23',0);

/*Table structure for table `hms_tag` */

DROP TABLE IF EXISTS `hms_tag`;

CREATE TABLE `hms_tag` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '标签id',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型（0医生1医院2药品）',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除，0为未删除，1为删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `hms_tag` */

insert  into `hms_tag`(`id`,`name`,`type`,`remark`,`update_time`,`create_time`,`is_delete`) values 
(1,'热门专家',0,'aaa',NULL,'2024-02-12 16:15:02',0),
(2,'骨科治疗',0,NULL,NULL,'2024-02-12 16:15:27',0),
(3,'医保',1,NULL,NULL,'2024-02-12 17:05:24',0),
(4,'中医（综合）',1,NULL,NULL,'2024-02-12 17:06:08',0),
(5,'医保',2,NULL,NULL,'2024-02-12 22:21:14',0),
(6,'西药',2,'测试',NULL,'2024-02-12 22:21:26',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
