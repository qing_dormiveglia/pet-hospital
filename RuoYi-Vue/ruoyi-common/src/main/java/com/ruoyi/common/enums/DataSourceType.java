package com.ruoyi.common.enums;

/**
 * 数据源
 * 
 * @author ruoyi
 */
public enum DataSourceType
{
    /**
     * 主库  框架库
     */
    MASTER,

    /**
     * 从库  医院模块
     */
    SLAVE_DB_PET_HMS,
    /**
     * 从库  用户模块
     */
    SLAVE_DB_PET_UMS,
    /**
     * 从库  数据字典模块
     */
    SLAVE_DB_PET_DMS,
    /**
     * 从库  订单模块
     */
    SLAVE_DB_PET_OMS

}
