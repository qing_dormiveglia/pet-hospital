package com.pool.springboot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

 /**
 * @Author zzww
 * @Date 2023/4/30 19:39
 */

@ConfigurationProperties(prefix = "pool.thread")
@Component
@Data
public class ThreadPoolPropertiesConfig {

    private Integer coreSize;

    private Integer maxSize;

    private Integer keepAliveTime;


}
