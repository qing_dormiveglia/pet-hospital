package com.pool.springboot.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author zzww
 * @Date 2024/3/5 14:10
 */
@Configuration
@EnableConfigurationProperties(ThreadPoolPropertiesConfig.class)
public class PoolsAutoConfigure
{
    @Resource
    private ThreadPoolPropertiesConfig thread;


    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "pool.thread", value = "enabled", havingValue = "true")
    ThreadPoolExecutor threadPoolExecutor() {
        return new ThreadPoolExecutor(
                thread.getCoreSize(),
                thread.getMaxSize(),
                thread.getKeepAliveTime(),
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100000),
                new ThreadPoolExecutor.AbortPolicy());
    }
}
