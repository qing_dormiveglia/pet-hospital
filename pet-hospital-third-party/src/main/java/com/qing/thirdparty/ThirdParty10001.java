package com.qing.thirdparty;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author zzww
 * @Date 2024/2/6 17:43
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class , SecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableDubbo(scanBasePackages = "com.qing")
public class ThirdParty10001 {
    public static void main(String[] args) {
        SpringApplication.run(ThirdParty10001.class,args);
    }
}
