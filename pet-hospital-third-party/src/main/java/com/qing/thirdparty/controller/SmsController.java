package com.qing.thirdparty.controller;

import com.qing.common.constant.PetHospitalConstant;
import com.qing.common.constant.RedisEnum;
import com.qing.common.exception.customizationException.FailException;
import com.qing.common.utils.RandomUtil;
import com.qing.thirdparty.api.SendSmsApi;
import com.qing.thirdparty.config.SendSmsPropertiesConfig;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @Author zzww
 * @Date 2023/1/12 10:30
 */
@RestController
@RequestMapping("/thirdParty/sms")
public class SmsController {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private SendSmsApi sendSmsApi;

    @Resource
    private SendSmsPropertiesConfig sendSmsPropertiesConfig;



    @GetMapping("/send")
    public AjaxResult sendSms(@RequestParam("phone") String phone)
    {
        String keycode = stringRedisTemplate.opsForValue().get(RedisEnum.SMS_CODE.getValue()+phone);
        if (!ObjectUtils.isEmpty(keycode)){
            return AjaxResult.success("请勿连续点击");
        }

        String code = RandomUtil.randomNumbers(6);
        String param = "{\"code\":\""+code+"\"}";
        sendSmsPropertiesConfig.setPhoneNumbers(phone);
        sendSmsPropertiesConfig.setTemplateParam(param);
        boolean sendSmsResponse= sendSmsApi.sendSms(sendSmsPropertiesConfig);
        if (!sendSmsResponse) {
            throw new FailException(PetHospitalConstant.ExceptionEum.SMS_SEND);
        }
        stringRedisTemplate.opsForValue().set(RedisEnum.SMS_CODE.getValue()+phone,code,
                sendSmsPropertiesConfig.getTimeValue(),TimeUnit.MINUTES);
        return AjaxResult.success();
    }
}
