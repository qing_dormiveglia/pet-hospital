package com.qing.thirdparty.controller;

import com.qing.thirdparty.api.OssServiceApi;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @Author zzww
 * @Date 2022/12/10 19:07
 */
@RestController
@RequestMapping("/thirdParty/oss")
public class OssController {

    @Resource
    private OssServiceApi ossServiceApi;

    @RequestMapping("/policy")
    public AjaxResult up(MultipartFile multipartFile, String bucketName){
        String url = ossServiceApi.OssUpload(multipartFile,bucketName);
        return AjaxResult.success().put("url",url);
    }
}
