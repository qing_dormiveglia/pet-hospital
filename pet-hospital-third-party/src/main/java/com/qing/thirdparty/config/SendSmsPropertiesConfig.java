package com.qing.thirdparty.config;

//import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author zzww
 * @Date 2022/11/30 18:53
 */
@Data
@Component
@Configuration
@Accessors(chain = true)
@ConfigurationProperties("aliyun.sms.message")
public class SendSmsPropertiesConfig {

//    @ApiModelProperty("accessKeyId")
    private String accessKeyId; //LTAI5tKBs2soCAuQ6qGwoyV3

//    @ApiModelProperty("accessKeySecret")
    private String accessKeySecret;
    //YmDnNSoilEMLAAhGvSSXlT0EY4HJ2D
//    LTAI5tKBs2soCAuQ6qGwoyV3
//            YmDnNSoilEMLAAhGvSSXlT0EY4HJ2D

//    @ApiModelProperty("地区")
    private String region; //cn-hangzhou


    private String EndpointOverride;

//    @ApiModelProperty("所属账户")
    private String resourceOwnerAccount; //1418658395293437

//    @ApiModelProperty("电话号码")
    private String phoneNumbers;

//    @ApiModelProperty("短信签名名称")
    private String signName; //阿里云短信测试

//    @ApiModelProperty("短信签名模板")
    private String TemplateCode; //SMS_154950909

//    @ApiModelProperty("false短信模板变量对应的实际值。" +
//            "支持传入多个参数，" +
//            "示例：{\"name\":\"张三\",\"number\":\"1390000****\"}。")
    private String TemplateParam;

//    @ApiModelProperty("false上行短信扩展码。" +
//            "上行短信指发送给通信服务提供商的短信，" +
//            "用于定制某种服务、完成查询，或是办理某种业务等，需要收费，" +
//            "按运营商普通短信资费进行扣费。")
    private String SmsUpExtendCode;

//    @ApiModelProperty("false外部流水扩展字段。")
    private String OutId;

    private Integer timeValue;
}
