package com.qing.thirdparty.api;

import com.aliyun.auth.credentials.Credential;
import com.aliyun.auth.credentials.provider.StaticCredentialProvider;
import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponseBody;
import com.qing.thirdparty.config.SendSmsPropertiesConfig;
import darabonba.core.client.ClientOverrideConfiguration;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @Author zzww
 * @Date 2022/11/30 19:09
 */
@Service
public class SendSmsApi {
    public boolean sendSms(SendSmsPropertiesConfig sendSmsMessage) {
        if (sendSmsMessage.getPhoneNumbers()==null){
            return false;
        }
        SendSmsResponse resp = null;
        AsyncClient client = null;
        try {
            StaticCredentialProvider provider = StaticCredentialProvider.create(Credential.builder()
                    .accessKeyId(sendSmsMessage.getAccessKeyId())
                    .accessKeySecret(sendSmsMessage.getAccessKeySecret())
                    .build());
            client = AsyncClient.builder()
                    .region(sendSmsMessage.getRegion()) // Region ID   ("cn-hangzhou") // Region ID 访问的服务器
                    .credentialsProvider(provider)
                    .overrideConfiguration(
                            ClientOverrideConfiguration.create()
                                    .setEndpointOverride(sendSmsMessage.getEndpointOverride())// 访问的域名
                    )
                    .build();
            SendSmsRequest sendSmsRequest = SendSmsRequest.builder()
                    .resourceOwnerAccount(sendSmsMessage.getResourceOwnerAccount())//1418658395293437
                    .resourceOwnerId(1L)
                    .phoneNumbers(sendSmsMessage.getPhoneNumbers())
                    .signName(sendSmsMessage.getSignName())
                    .templateParam(sendSmsMessage.getTemplateParam())
                    .templateCode(sendSmsMessage.getTemplateCode())
                    .build();
            CompletableFuture<SendSmsResponse> response = client.sendSms(sendSmsRequest);
            resp = response.get();
            SendSmsResponseBody body = resp.getBody();
            String code = body.getCode();
            if ("OK".equals(code)){
                return true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            client.close();
        }
        return false;
    }
}
