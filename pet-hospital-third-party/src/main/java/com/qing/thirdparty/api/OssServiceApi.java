package com.qing.thirdparty.api;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectResult;
import com.qing.common.dubbo.ThirdPartyApi;
import com.qing.thirdparty.config.OssMessageConfig;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.UUID;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * @Author zzww
 * @Date 2022/12/9 21:10
 */
@DubboService(interfaceClass = ThirdPartyApi.class)
@Service
public class OssServiceApi implements ThirdPartyApi {

    @Resource
    private OssMessageConfig ossMessageConfig;


    /**
     * 文件上传
     * @return
     */
    public String OssUpload(MultipartFile multipartFile, String bucketName) {
        if (ObjectUtils.isEmpty(multipartFile)){
            return null;
        }

        String filename = multipartFile.getOriginalFilename();
        if (ObjectUtils.isEmpty(filename)){
            return null;
        }
        if (ObjectUtils.isEmpty(bucketName)){
            bucketName = ossMessageConfig.getBucketName();
        }
        ArrayList<Object> objects = new ArrayList<>();
        //获取文件后缀
        String substring = filename.substring(filename.lastIndexOf("."));
        //拼接发送的地址
        //目录结构   年/月/日/uuid+时间搓+文件后缀名
        String objectName = DateUtils.datePath()+"/"+
                UUID.randomUUID().toString().trim().replaceAll("-", "") +
                System.currentTimeMillis()+substring;
        // 填写本地文件的完整路径，例如D:\\localpath\\examplefile.txt。
        // 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
        OSS ossClient = null;
        // 创建OSSClient实例。

        try {
            InputStream inputStream = multipartFile.getInputStream();
            ossClient = new OSSClientBuilder().build(ossMessageConfig.getEndpoint(),
                    ossMessageConfig.getAccessKeyId(),ossMessageConfig.getAccessKeySecret());
//            InputStream inputStream = new FileInputStream(filePath);
            // 创建PutObject请求。
            PutObjectResult result = ossClient.putObject(bucketName, objectName, inputStream);
//https://crowd-photo-upload.oss-cn-fuzhou.aliyuncs.com/crowd-upload/details/061faf0b5ccc4fa18f739db6fd9894631659717415.jpg
            String urlFile = "https://"+bucketName+"."+ossMessageConfig.getEndpoint()+"/"+objectName;
            return urlFile;
        }catch (Exception ce) {
            return null;
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    @Override
    public String upLoadDuBboApi(MultipartFile multipartFile, String bucketName) {
        String s = OssUpload(multipartFile, bucketName);
        return s;
    }
}
