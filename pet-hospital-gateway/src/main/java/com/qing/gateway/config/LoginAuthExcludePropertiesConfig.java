package com.qing.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/11/6 16:41
 */
@Component
@ConfigurationProperties(prefix = "login.auth.exclude")
public class LoginAuthExcludePropertiesConfig
{
    private List<String> paths;

    public List<String> getPaths() {
        return paths;
    }

    public void setPaths(List<String> paths) {
        this.paths = paths;
    }
}
