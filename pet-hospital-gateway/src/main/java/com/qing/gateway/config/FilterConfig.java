package com.qing.gateway.config;

import com.qing.gateway.filter.GrayScalePerFilter;
import com.qing.gateway.filter.TokenValidationFilter;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.ServerHttpRequest;

/**
 * @Author zzww
 * @Date 2024/2/25 16:00
 * 过滤器配置
 */
@Configuration
public class FilterConfig {

}
