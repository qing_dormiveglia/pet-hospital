package com.qing.gateway.filter;

import com.qing.common.constant.TokenEnum;
import com.qing.common.utils.jwt.JwtUtils;
import com.qing.gateway.config.LoginAuthExcludePropertiesConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.PathContainer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.pattern.PathPattern;
import reactor.core.publisher.Mono;
import java.util.List;
import java.util.stream.Collectors;
import static org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration.pathPatternParser;

/**
 * @Author zzww
 * @Date 2024/5/16 4:49
 * 登录校验过滤器
 */
@Component
public class TokenValidationFilter implements GlobalFilter, Ordered
{
    private final LoginAuthExcludePropertiesConfig loginAuthExcludePropertiesConfig;
    @Autowired
    public TokenValidationFilter(LoginAuthExcludePropertiesConfig loginAuthExcludePropertiesConfig) {
        this.loginAuthExcludePropertiesConfig = loginAuthExcludePropertiesConfig;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //获取请求响应实体
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //获取url
        String uri = request.getURI().getPath();
        //匹配是否放行
        if (isPathExcluded(uri)){
            return chain.filter(exchange);
        }
            // 获取请求头中的Authorization信息
            String accessToken = request.getHeaders().getFirst(TokenEnum.ACCESS_TOKEN.getValue());
            String refreshToken = request.getHeaders().getFirst(TokenEnum.REFRESH_TOKEN.getValue());
            Boolean aBoolean = false;
        Boolean bBoolean = false;
            try {
                aBoolean = JwtUtils.validateToken(accessToken);
            }catch (Exception e){
                e.printStackTrace();
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }
            try {
                bBoolean = JwtUtils.validateToken(refreshToken);
            }catch (Exception e){
                e.printStackTrace();
                response.setStatusCode(HttpStatus.FORBIDDEN);
                return response.setComplete();
            }
            if (aBoolean) {
                if (bBoolean){
                    return chain.filter(exchange);
                }else{
                    response.setStatusCode(HttpStatus.FORBIDDEN);
                    return response.setComplete();
                }
                // Token有效，继续请求链
            } else {
                // Token无效，返回未授权状态码
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }
    }
    @Override
    public int getOrder() {
        // 设置过滤器的优先级，数字越小，优先级越高
        return Ordered.HIGHEST_PRECEDENCE;
    }

    public boolean isPathExcluded(String uri) {
        List<PathPattern> pathPatterns = loginAuthExcludePropertiesConfig.getPaths().stream()
                .map(pathPatternParser::parse)
                .collect(Collectors.toList());

        for (PathPattern pathPattern : pathPatterns) {
            if (pathPattern.matches(PathContainer.parsePath(uri))) {
                return true;
            }
        }
        return false;
    }
}
