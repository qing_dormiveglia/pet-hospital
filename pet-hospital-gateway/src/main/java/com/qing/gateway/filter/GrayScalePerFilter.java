package com.qing.gateway.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qing.gateway.gray.GrayRule;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.security.access.prepost.PreFilter;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;

import javax.annotation.Resource;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * 灰度前置过滤器
 * @Author zzww
 * @Date 2024/2/25 15:56
 */
@Slf4j
@Component
public class GrayScalePerFilter implements GlobalFilter, Ordered
{
    @Resource
    private GrayRule grayRule;

    /**
     * 过滤器执行的内容
     * @return
     */
    public boolean run(ServerHttpRequest request) {
        //是否开启灰度
        if (!grayRule.isEnabled()) {
            return true;
        }

        if (!grayRule.hitRule(getClientIp(request))) {
            return false;
        }


        // 第二步: 获取请求头(包括请求的来源url和method)
//        HttpHeaders headers = request.getHeaders();

//        Map<String, String> headerMap = getHeadersInfo();
//        log.info("headerMap:{},grayRules:{}", headerMap, grayRules);

        // 删除之前的路由到灰度的标记
       /* if (RibbonFilterContextHolder.getCurrentContext().getAttributes().get(GrayConstant.GRAY_TAG) != null) {
            RibbonFilterContextHolder.getCurrentContext().remove(GrayConstant.GRAY_TAG);
        }*/
        //灰度开关关闭 -- 无需走灰度, 执行正常的ribbon负载均衡转发策略
//        if (grayEnable == 0) {
//            log.info("灰度开关已关闭");
//            return null;
//        }
//        if (!grayRules.isEmpty()) {
//
//            for (Map<String, String> grayRuleMap : grayRules) {
//                try {
//                    // 获取本次灰度的标签,标签的内容是灰度的规则内容
//                    String grayTag = grayRuleMap.get(GrayConstant.GRAY_TAG);
//
//                    // 第三步: 过滤有效的灰度标签
//                    Map<String, String> resultGrayRuleMap = new HashMap<>();
//                    //去掉值为空的灰度规则
//                    grayRuleMap.forEach((K, V) -> {
//                        if (StringUtils.isNotBlank(V)) {
//                            resultGrayRuleMap.put(K, V);
//                        }
//                    });
//                    resultGrayRuleMap.remove(GrayConstant.GRAY_TAG);
//
//
//                    //将灰度标签(规则)小写化
//                    Map<String, String> lowerGrayRuleMap = transformUpperCase(resultGrayRuleMap);
//
//                    // 第四步: 判断请求头是否匹配灰度规则
//                    if (headerMap.entrySet().containsAll(resultGrayRuleMap.entrySet()) || headerMap.entrySet().containsAll(lowerGrayRuleMap.entrySet())) {
//                        // 这是网关通讯使用的全局对象RequestContext
//                        RequestContext requestContext = RequestContext.getCurrentContext();
//                        // 把灰度规则添加到网关请求头, 后面的请求都可以使用该参数
//                        requestContext.addZuulRequestHeader(GrayConstant.GRAY_HEADER, grayTag);
//                        // 将灰度规则添加到ribbon的上下文
//                        RibbonFilterContextHolder.getCurrentContext().add(GrayConstant.GRAY_TAG, grayTag);
//                        log.info("添加灰度tag成功:lowerGrayRuleMap:{},grayTag:{}", lowerGrayRuleMap, grayTag);
//                    }
//                } catch (Exception e) {
//                    log.error("灰度匹配失败", e);
//                }
//            }
//        }
        return true;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if (run(exchange.getRequest())) {
            return chain.filter(exchange);
        }
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.BAD_GATEWAY);
        String body = "系统灰度验证失败";
        DataBuffer wrap = response.bufferFactory().wrap(body.getBytes());
        response.writeWith(Mono.just(wrap));
        return response.setComplete();
    }


    @Override
    public int getOrder() {
        //在登录校验结束进行灰度校验
        return 1;
    }


    private String getClientIp(ServerHttpRequest request) {
        HttpHeaders headers = request.getHeaders();
        String ip = headers.getFirst("x-forwarded-for");
        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
            // 多次反向代理后会有多个ip值，第一个ip才是真实ip
            if (ip.indexOf(",") != -1) {
                ip = ip.split(",")[0];
            }
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = headers.getFirst("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = headers.getFirst("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = headers.getFirst("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = headers.getFirst("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = headers.getFirst("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddress().getAddress().getHostAddress();
        }
        return ip;
    }
}
