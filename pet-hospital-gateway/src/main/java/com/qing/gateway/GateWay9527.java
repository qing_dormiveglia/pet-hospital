package com.qing.gateway;

import com.qing.gateway.filter.TokenValidationFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

/**
 * @Author zzww
 * @Date 2023/11/15 22:37
 */
@EnableDiscoveryClient
@SpringBootApplication
public class GateWay9527 {

    public static void main(String[] args) {
        SpringApplication.run(GateWay9527.class,args);
    }

//    @Bean
//    public RouteLocator tokenValidationFilter(RouteLocatorBuilder builder) {
//        return builder.routes()
//                // token校验2
//                .route(predicateSpec -> predicateSpec
//
//                        .path("/gateway/order-audit/**", "/gateway/order/**", "/gateway/order-payment/**")
//                        .filters(gatewayFilterSpec -> gatewayFilterSpec.stripPrefix(1).filter(new TokenValidationFilter()))
//                        .uri("lb://OLOAN-ORDER-SERVICE")
//                        .id("OLOAN-ORDER-ORDER-token"))
//                .build();
//    }

}
