package com.qing.hospital.dto.hospital;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/12 16:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalDto {

    private Long cityId;

    private Long gradeId;

    private String keyWord;
}
