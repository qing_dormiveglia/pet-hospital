package com.qing.hospital.dto.drug;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/12 17:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DrugDto
{

    /** 城市id */
    private Long cityId;

    /** 医院id */
    private Long hospitalId;

    /** 适用宠物类型（0为通用）id */
    private Long petKindId;

    /** 药品类型id */
    private Long drugTypeId;

    /** （0处方药1非处方药） */
    private Integer type;

    /** (0医保范围1不在医保范围) */
    private Integer medical;

    /** 检索值 */
    private String keyWord;
}
