package com.qing.hospital.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/10 15:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorDto {

    private Long cityId;

    private Long hospitalId;

    private Long depId;

    private String keyWord;
}
