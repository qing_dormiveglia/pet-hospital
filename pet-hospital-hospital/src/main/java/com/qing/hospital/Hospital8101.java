package com.qing.hospital;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author zzww
 * @Date 2024/2/7 23:45
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@MapperScan("com.qing.**.mapper")
@EnableDubbo(scanBasePackages = "com.qing")
public class Hospital8101 {
    public static void main(String[] args) {
        SpringApplication.run(Hospital8101.class,args);
    }
}
