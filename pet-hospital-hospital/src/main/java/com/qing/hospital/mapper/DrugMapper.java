package com.qing.hospital.mapper;

import java.util.List;
import com.qing.hospital.domain.Drug;
import com.qing.hospital.dto.drug.DrugDto;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface DrugMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Drug selectDrugById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param drug 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Drug> selectDrugList(Drug drug);

    /**
     * 新增【请填写功能名称】
     * 
     * @param drug 【请填写功能名称】
     * @return 结果
     */
    public int insertDrug(Drug drug);

    /**
     * 修改【请填写功能名称】
     * 
     * @param drug 【请填写功能名称】
     * @return 结果
     */
    public int updateDrug(Drug drug);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDrugById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDrugByIds(Long[] ids);

    /**
     * 查询药品列表
     * @param drugDto
     * @return
     */
    List<Drug> selectDrugByDrugDto(DrugDto drugDto);
}
