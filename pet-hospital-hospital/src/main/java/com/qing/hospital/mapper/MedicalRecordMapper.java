package com.qing.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.hospital.domain.MedicalRecordEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zzww
 * @email qing@163.com
 * @date 2024-04-26 12:01:15
 */
@Mapper
public interface MedicalRecordMapper extends BaseMapper<MedicalRecordEntity> {
	
}
