package com.qing.hospital.mapper;

import java.util.List;
import com.qing.hospital.domain.Dep;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface DepMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Dep selectDepById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dep 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Dep> selectDepList(Dep dep);

    /**
     * 新增【请填写功能名称】
     * 
     * @param dep 【请填写功能名称】
     * @return 结果
     */
    public int insertDep(Dep dep);

    /**
     * 修改【请填写功能名称】
     * 
     * @param dep 【请填写功能名称】
     * @return 结果
     */
    public int updateDep(Dep dep);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDepById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDepByIds(Long[] ids);

    /**
     * 批量查询科室信息
     * @param longs
     * @return
     */
    List<Dep> selectDepBatchByIds(List<Long> longs);
}
