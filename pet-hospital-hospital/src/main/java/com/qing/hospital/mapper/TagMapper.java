package com.qing.hospital.mapper;

import java.util.List;
import com.qing.hospital.domain.Tag;
import org.apache.ibatis.annotations.Param;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface TagMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Tag selectTagById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tag 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Tag> selectTagList(Tag tag);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tag 【请填写功能名称】
     * @return 结果
     */
    public int insertTag(Tag tag);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tag 【请填写功能名称】
     * @return 结果
     */
    public int updateTag(Tag tag);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTagById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTagByIds(Long[] ids);

    /**
     * 批量查询标签（根据不同类型）
     * @param ids
     * @param type 类型
     * @return
     */
    List<Tag> selectTagBatchByIds(@Param("ids") List<Long> ids,@Param("type") Integer type);
}
