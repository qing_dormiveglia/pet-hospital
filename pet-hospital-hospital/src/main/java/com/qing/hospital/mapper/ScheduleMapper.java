package com.qing.hospital.mapper;

import java.util.List;
import com.qing.hospital.domain.Schedule;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface ScheduleMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Schedule selectScheduleById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param schedule 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Schedule> selectScheduleList(Schedule schedule);

    /**
     * 新增【请填写功能名称】
     * 
     * @param schedule 【请填写功能名称】
     * @return 结果
     */
    public int insertSchedule(Schedule schedule);

    /**
     * 修改【请填写功能名称】
     * 
     * @param schedule 【请填写功能名称】
     * @return 结果
     */
    public int updateSchedule(Schedule schedule);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteScheduleById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteScheduleByIds(Long[] ids);

    /**
     * 根据医生id查询该医生今天（包含今天）以后的排班安排
     * @param id
     * @return
     */
    List<Schedule> selectScheduleListByDoctorId(Long id);
}
