package com.qing.hospital.mapper;

import java.util.List;
import com.qing.hospital.domain.Hospital;
import com.qing.hospital.dto.hospital.HospitalDto;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface HospitalMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Hospital selectHospitalById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hospital 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Hospital> selectHospitalList(Hospital hospital);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hospital 【请填写功能名称】
     * @return 结果
     */
    public int insertHospital(Hospital hospital);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hospital 【请填写功能名称】
     * @return 结果
     */
    public int updateHospital(Hospital hospital);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteHospitalById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHospitalByIds(Long[] ids);

    /**
     * 查询医院列表（根据dto中属性检索）
     * @param hospitalDto
     * @return
     */
    List<Hospital> selectHospitalListByHospitalDto(HospitalDto hospitalDto);
}
