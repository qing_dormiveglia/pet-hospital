package com.qing.hospital.mapper;

import java.util.List;
import com.qing.hospital.domain.Doctor;
import com.qing.hospital.dto.doctor.DoctorDto;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface DoctorMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Doctor selectDoctorById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param doctor 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Doctor> selectDoctorList(Doctor doctor);

    /**
     * 新增【请填写功能名称】
     * 
     * @param doctor 【请填写功能名称】
     * @return 结果
     */
    public int insertDoctor(Doctor doctor);

    /**
     * 修改【请填写功能名称】
     * 
     * @param doctor 【请填写功能名称】
     * @return 结果
     */
    public int updateDoctor(Doctor doctor);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDoctorById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDoctorByIds(Long[] ids);

    /**
     * 根据检索条件查询医生列表
     * @param doctorDto
     * @return
     */
    List<Doctor> selectDoctorVoList(DoctorDto doctorDto);
}
