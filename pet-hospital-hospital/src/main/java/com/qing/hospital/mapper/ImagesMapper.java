package com.qing.hospital.mapper;

import java.util.List;
import com.qing.hospital.domain.Images;

/**
 * 图集Mapper接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface ImagesMapper 
{
    /**
     * 查询图集
     * 
     * @param id 图集主键
     * @return 图集
     */
    public Images selectImagesById(Long id);

    /**
     * 查询图集列表
     * 
     * @param images 图集
     * @return 图集集合
     */
    public List<Images> selectImagesList(Images images);

    /**
     * 新增图集
     * 
     * @param images 图集
     * @return 结果
     */
    public int insertImages(Images images);

    /**
     * 修改图集
     * 
     * @param images 图集
     * @return 结果
     */
    public int updateImages(Images images);

    /**
     * 删除图集
     * 
     * @param id 图集主键
     * @return 结果
     */
    public int deleteImagesById(Long id);

    /**
     * 批量删除图集
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteImagesByIds(Long[] ids);
}
