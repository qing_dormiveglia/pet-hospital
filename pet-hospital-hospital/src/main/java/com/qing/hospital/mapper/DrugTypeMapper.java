package com.qing.hospital.mapper;

import java.util.List;
import com.qing.hospital.domain.DrugType;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface DrugTypeMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public DrugType selectDrugTypeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param drugType 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<DrugType> selectDrugTypeList(DrugType drugType);

    /**
     * 新增【请填写功能名称】
     * 
     * @param drugType 【请填写功能名称】
     * @return 结果
     */
    public int insertDrugType(DrugType drugType);

    /**
     * 修改【请填写功能名称】
     * 
     * @param drugType 【请填写功能名称】
     * @return 结果
     */
    public int updateDrugType(DrugType drugType);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDrugTypeById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDrugTypeByIds(Long[] ids);
}
