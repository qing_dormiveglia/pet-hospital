package com.qing.hospital.vo.hospital;

import com.qing.hospital.domain.Schedule;
import com.qing.hospital.vo.doctor.ScheduleVo;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author zzww
 * @Date 2024/4/27 15:25
 */
@Data
public class ScheduleInfoVo
{
    private List<ScheduleVo> times;
    private Date time;
    private String dateNameWeek;

}
