package com.qing.hospital.vo.order;

import com.qing.hospital.domain.OrderInfo;
import com.qing.hospital.domain.Schedule;
import lombok.Data;

/**
 * @Author zzww
 * @Date 2024/4/30 14:37
 */
@Data
public class OrderInfoVo extends OrderInfo
{
    private Schedule schedule;
}
