package com.qing.hospital.vo.hospital;

import com.qing.hospital.vo.dep.DepVo;
import com.qing.hospital.vo.doctor.DoctorVo;
import com.qing.hospital.vo.tag.TagVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/2/12 17:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalInfoVo {
    /** 医院id */
    private Long id;

    /** 科室 */
    private List<DepVo> depVos;

    /** 标签 */
    private List<TagVo> tagVos;

    /** 名称 */
    private String name;

    /** 头图 */
    private String headImage;

    /** 区 */
    private String region;

    /** 等级 */
    private String grade;

    /** 介绍 */
    private String introduce;

    /** 地址 */
    private String address;
}
