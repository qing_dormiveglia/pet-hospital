package com.qing.hospital.vo.doctor;

import com.qing.hospital.vo.dep.DepVo;
import com.qing.hospital.vo.hospital.HospitalByDoctorVo;
import com.qing.hospital.vo.hospital.ScheduleInfoVo;
import com.qing.hospital.vo.schedule.ScheduleVo;
import com.qing.hospital.vo.tag.TagVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/2/10 15:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorInfoVo {

    /** 医生ID */
    private Long id;

    /** 科室信息 */
    private DepVo dep;

    /** 医院信息 */
    private HospitalByDoctorVo hospital;

    /**
     * 标签
     */
    private List<TagVo> tagList;

    /**
     * 排班信息
     */
    private List<ScheduleInfoVo> scheduleVoList;

    /** 姓名 */
    private String name;

    /** 区 */
    private String region;

    /** 职位 */
    private String position;

    /** 资历 */
    private String qualifications;

    /** 医生介绍 */
    private String introduce;

    /** 擅长 */
    private String skill;

    /** 挂号费 */
    private Long amount;

    /** 头像 */
    private String headerImage;
}
