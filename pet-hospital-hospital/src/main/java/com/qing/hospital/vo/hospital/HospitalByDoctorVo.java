package com.qing.hospital.vo.hospital;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/10 16:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalByDoctorVo {

    /** 医院id */
    private Long id;

    /** 名称 */
    private String name;

    /** 头图 */
    private String headImage;

    /** 等级 */
    private String grade;

    /** 地址 */
    private String address;
}
