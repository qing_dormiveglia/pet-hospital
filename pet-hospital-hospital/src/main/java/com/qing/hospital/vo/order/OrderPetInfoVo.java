package com.qing.hospital.vo.order;

import com.qing.hospital.domain.Doctor;
import com.qing.hospital.domain.Hospital;
import com.qing.hospital.vo.doctor.DoctorInfoVo;
import com.qing.hospital.vo.doctor.DoctorVo;
import lombok.Data;

/**
 * @Author zzww
 * @Date 2024/4/30 15:27
 */
@Data
public class OrderPetInfoVo extends OrderInfoVo
{
    private Hospital hospital;
    private DoctorInfoVo doctor;

}
