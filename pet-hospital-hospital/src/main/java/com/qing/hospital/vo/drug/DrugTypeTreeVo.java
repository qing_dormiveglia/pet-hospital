package com.qing.hospital.vo.drug;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/12 22:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DrugTypeTreeVo {
    /** 药品类型id */
    private Long id;

    /** 父id */
    private Long pId;

    /** 值 */
    private String value;

    /** 类型（0一级父类1二级父类2子类） */
    private Integer type;

    /** 子节点 */
    private DrugTypeTreeVo children;
}
