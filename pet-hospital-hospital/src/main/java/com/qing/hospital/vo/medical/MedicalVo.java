package com.qing.hospital.vo.medical;

import com.qing.hospital.domain.Hospital;
import com.qing.hospital.domain.MedicalRecordEntity;
import lombok.Data;

/**
 * @Author zzww
 * @Date 2024/4/26 13:49
 */
@Data
public class MedicalVo extends MedicalRecordEntity
{
    private Hospital hospital;
}
