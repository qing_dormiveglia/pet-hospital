package com.qing.hospital.vo.tag;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/10 16:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagVo {

    /** 标签id */
    private Long id;

    /** 名称 */
    private String name;

    /** 类型（0医生1医院2药品） */
    private Integer type;
}
