package com.qing.hospital.vo.schedule;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/2/10 16:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleVo {

    /** 排班ID */
    private Long id;

    /** 排班日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date workDate;

    /** 排班时间（0上午1下午） */
    private Integer workTime;

    /** 可以预约数 */
    private Long reservedNumber;

    /** 剩余预约数 */
    private Long availableNumber;

    /** 排班状态（-1：停诊 0：停约 1：可约） */
    private Integer status;
}
