package com.qing.hospital.vo.advertising;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/2/9 2:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisingVo {

    /** 图集ID */
    private Long id;

    /** 名称 */
    private String name;

    /** 路径 */
    private String url;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
