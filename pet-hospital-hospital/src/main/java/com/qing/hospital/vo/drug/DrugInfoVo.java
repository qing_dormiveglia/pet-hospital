package com.qing.hospital.vo.drug;

import com.qing.hospital.vo.hospital.HospitalVo;
import com.qing.hospital.vo.tag.TagVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author zzww
 * @Date 2024/2/12 18:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DrugInfoVo {
    /** 药品id */
    private Long id;

    /** 医院 */
    private HospitalVo hospital;

    /** 药品类型 */
    private DrugTypeTreeVo children;

    /** 标签 */
    private List<TagVo> tags;

    /** 名称 */
    private String name;

    /** 名称 */
    private String region;

    /** 英文名 */
    private String englishName;

    /** 头图地址 */
    private String headImage;

    /** 价格 */
    private BigDecimal price;

    /** （0处方药1非处方药） */
    private Integer type;

    /** (0医保范围1不在医保范围) */
    private Integer medical;

    /** 用法用量 */
    private String usage;

    /** 禁忌(逗号隔开) */
    private String taboo;

    /** 药品批准文号 */
    private String approvalNumber;

    /** 保质期 */
    private String expirationDate;

    /** 储存方法 */
    private String storageMethod;

    /** 不良反应 */
    private String adverseReaction;

    /** 规格 */
    private String specification;

    /** 成分 */
    private String ingredient;

    /** 主治 */
    private String cure;

    /** 药品相互作用 */
    private String coactions;

    /** 描述 */
    private String content;

    /** 备注 */
    private String remark;
}
