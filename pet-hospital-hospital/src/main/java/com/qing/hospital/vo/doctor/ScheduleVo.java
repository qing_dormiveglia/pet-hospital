package com.qing.hospital.vo.doctor;

import com.qing.hospital.domain.Schedule;
import lombok.Data;

/**
 * @Author zzww
 * @Date 2024/4/29 22:37
 */
@Data
public class ScheduleVo extends Schedule
{
    private Boolean bool = false;
}
