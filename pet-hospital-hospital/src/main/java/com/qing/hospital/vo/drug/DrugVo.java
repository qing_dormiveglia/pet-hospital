package com.qing.hospital.vo.drug;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author zzww
 * @Date 2024/2/12 17:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DrugVo
{

    /** 药品id */
    private Long id;

    /** 药品类型id */
    private DrugTypeVo drugTypeVo;

    /** 名称 */
    private String name;

    /** 名称 */
    private String region;

    /** 头图地址 */
    private String headImage;

    /** 价格 */
    private BigDecimal price;

    /** （0处方药1非处方药） */
    private Integer type;

    /** (0医保范围1不在医保范围) */
    private Integer medical;

    /** 主治 */
    private String cure;

}
