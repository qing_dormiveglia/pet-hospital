package com.qing.hospital.vo.hospital;

import com.qing.hospital.vo.tag.TagVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/2/12 16:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalVo {

    /** 医院id */
    private Long id;

    /** 标签 */
    private List<TagVo> tagByHospitalVos;

    /** 名称 */
    private String name;

    /** 头图 */
    private String headImage;

    /** 区 */
    private String region;

    /** 等级 */
    private String grade;

    /** 介绍 */
    private String introduce;

    /** 地址 */
    private String address;
}
