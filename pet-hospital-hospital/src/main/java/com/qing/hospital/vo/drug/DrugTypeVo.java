package com.qing.hospital.vo.drug;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/12 18:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DrugTypeVo {

    /** 药品类型id */
    private Long id;

    /** 父id */
    private Long pId;

    /** 值 */
    private String value;

    /** 类型（0一级父类1二级父类2子类） */
    private Integer type;
}
