package com.qing.hospital.vo.doctor;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/10 15:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorVo {

    /** 医生ID */
    private Long id;

    /** 姓名 */
    private String name;

    /** 区 */
    private String region;

    /** 职位 */
    private String position;

    /** 擅长 */
    private String skill;

    /** 挂号费 */
    private Long amount;

    /** 头像 */
    private String headerImage;

}
