package com.qing.hospital.vo.dep;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/10 15:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepVo {
    /** 科室ID */
    private Long id;

    /** 名称 */
    private String name;
}
