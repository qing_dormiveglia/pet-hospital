package com.qing.hospital.controller;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.qing.hospital.dto.hospital.HospitalDto;
import com.qing.hospital.vo.hospital.HospitalVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.hospital.domain.Hospital;
import com.qing.hospital.service.IHospitalService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author zzww
 * @date 2024-02-07
 */
@RestController
@RequestMapping("/hospital/hospital")
public class HospitalController extends BaseController
{
    @Resource
    private IHospitalService hospitalService;

    /**
     * 查询医院列表
     */
    @GetMapping("/page/list")
    public TableDataInfo list(@RequestParam Map<String,Object> params)
    {
        startPage();
        HospitalDto hospitalDto = JSONObject.parseObject(JSONObject.toJSONString(params), HospitalDto.class);
        List<HospitalVo> list = hospitalService.findHospitalVoList(hospitalDto);
        return getDataTable(list);
    }

    /**
     * 获取医院详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(hospitalService.selectHospitalVoById(id));
    }


    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('hospital:hospital:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Hospital hospital)
    {
        List<Hospital> list = hospitalService.selectHospitalList(hospital);
        ExcelUtil<Hospital> util = new ExcelUtil<Hospital>(Hospital.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('hospital:hospital:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Hospital hospital)
    {
        return toAjax(hospitalService.insertHospital(hospital));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('hospital:hospital:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Map<String,Object> map)
    {
        Hospital hospital = JSONObject.parseObject(JSON.toJSONString(map), Hospital.class);
        return toAjax(hospitalService.updateHospital(hospital));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('hospital:hospital:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(hospitalService.deleteHospitalByIds(ids));
    }
}
