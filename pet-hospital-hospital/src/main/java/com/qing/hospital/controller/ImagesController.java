package com.qing.hospital.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.qing.hospital.vo.advertising.AdvertisingVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.hospital.domain.Images;
import com.qing.hospital.service.IImagesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 图集Controller
 * 
 * @author zzww
 * @date 2024-02-07
 */
@RestController
@RequestMapping("/hospital/images")
public class ImagesController extends BaseController
{
    @Autowired
    private IImagesService imagesService;

    /**
     * 查询广告
     */
//    @PreAuthorize("@ss.hasPermi('hospital:images:list')")
    @GetMapping("/advertising")
    public TableDataInfo advertising()
    {
        startPage();
        List<AdvertisingVo> list = imagesService.selectImagesAdvertising();
        return getDataTable(list);
    }

    /**
     * 查询图集列表
     */
    @PreAuthorize("@ss.hasPermi('hospital:images:list')")
    @GetMapping("/list")
    public TableDataInfo list(Images images)
    {
        startPage();
        List<Images> list = imagesService.selectImagesList(images);
        return getDataTable(list);
    }

    /**
     * 导出图集列表
     */
    @PreAuthorize("@ss.hasPermi('hospital:images:export')")
    @Log(title = "图集", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Images images)
    {
        List<Images> list = imagesService.selectImagesList(images);
        ExcelUtil<Images> util = new ExcelUtil<Images>(Images.class);
        util.exportExcel(response, list, "图集数据");
    }

    /**
     * 获取图集详细信息
     */
    @PreAuthorize("@ss.hasPermi('hospital:images:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(imagesService.selectImagesById(id));
    }

    /**
     * 新增图集
     */
    @PreAuthorize("@ss.hasPermi('hospital:images:add')")
    @Log(title = "图集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Images images)
    {
        return toAjax(imagesService.insertImages(images));
    }

    /**
     * 修改图集
     */
    @PreAuthorize("@ss.hasPermi('hospital:images:edit')")
    @Log(title = "图集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Images images)
    {
        return toAjax(imagesService.updateImages(images));
    }

    /**
     * 删除图集
     */
    @PreAuthorize("@ss.hasPermi('hospital:images:remove')")
    @Log(title = "图集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(imagesService.deleteImagesByIds(ids));
    }
}
