package com.qing.hospital.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson2.JSONObject;
import com.qing.hospital.domain.MedicalRecordEntity;
import com.qing.hospital.dto.doctor.DoctorDto;
import com.qing.hospital.service.MedicalRecordService;
import com.qing.hospital.vo.doctor.DoctorVo;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author zzww
 * @email qing@163.com
 * @date 2024-04-26 12:01:15
 */
@RestController
@RequestMapping("hospital/medicalrecord")
public class MedicalRecordController extends BaseController {
    @Autowired
    private MedicalRecordService medicalRecordService;

    /**
     * 查询医生列表
     */
    @GetMapping("/list/{id}")
    public TableDataInfo pageList(@RequestParam Map<String,Object> params,@PathVariable("id") Long id)
    {
        startPage();
        List<MedicalRecordEntity> list = medicalRecordService.listPage(params,id);
        return getDataTable(list);
    }

//    info-pet
    /**
     * 获取【请填写功能名称】详细信息
     */
    @GetMapping(value = "/info-pet/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(medicalRecordService.selectInfoVoById(id));
    }

//    /**
//     * 列表
//     */
//    @RequestMapping("/list")
//    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = medicalRecordService.queryPage(params);
//
//        return R.ok().put("page", page);
//    }
//
//
//    /**
//     * 信息
//     */
//    @RequestMapping("/info/{id}")
//    public R info(@PathVariable("id") Long id){
//		MedicalRecordEntity medicalRecord = medicalRecordService.getById(id);
//
//        return R.ok().put("medicalRecord", medicalRecord);
//    }
//
//    /**
//     * 保存
//     */
//    @RequestMapping("/save")
//    public R save(@RequestBody MedicalRecordEntity medicalRecord){
//		medicalRecordService.save(medicalRecord);
//
//        return R.ok();
//    }
//
//    /**
//     * 修改
//     */
//    @RequestMapping("/update")
//    public R update(@RequestBody MedicalRecordEntity medicalRecord){
//		medicalRecordService.updateById(medicalRecord);
//
//        return R.ok();
//    }
//
//    /**
//     * 删除
//     */
//    @RequestMapping("/delete")
//    public R delete(@RequestBody Long[] ids){
//		medicalRecordService.removeByIds(Arrays.asList(ids));
//
//        return R.ok();
//    }

}
