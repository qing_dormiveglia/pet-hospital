package com.qing.hospital.service.impl;

import java.util.List;

import com.qing.hospital.vo.hospital.ScheduleInfoVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.ScheduleMapper;
import com.qing.hospital.domain.Schedule;
import com.qing.hospital.service.IScheduleService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@Service
public class ScheduleServiceImpl implements IScheduleService 
{
    @Resource
    private ScheduleMapper scheduleMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Schedule selectScheduleById(Long id)
    {
        return scheduleMapper.selectScheduleById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param schedule 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Schedule> selectScheduleList(Schedule schedule)
    {
        return scheduleMapper.selectScheduleList(schedule);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param schedule 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertSchedule(Schedule schedule)
    {
        schedule.setCreateTime(DateUtils.getNowDate());
        return scheduleMapper.insertSchedule(schedule);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param schedule 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateSchedule(Schedule schedule)
    {
        schedule.setUpdateTime(DateUtils.getNowDate());
        return scheduleMapper.updateSchedule(schedule);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteScheduleByIds(Long[] ids)
    {
        return scheduleMapper.deleteScheduleByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteScheduleById(Long id)
    {
        return scheduleMapper.deleteScheduleById(id);
    }

    /**
     * 根据医生id查询近七日的排班信息
     * @param id 医生id
     * @return
     */
    @Override
    public List<Schedule> selectScheduleListByDoctorId(Long id) {
        List<Schedule> list = scheduleMapper.selectScheduleListByDoctorId(id);
        return list;
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public List<ScheduleInfoVo> selectScheduleByDoctorId(Long id) {

        return null;
    }
}
