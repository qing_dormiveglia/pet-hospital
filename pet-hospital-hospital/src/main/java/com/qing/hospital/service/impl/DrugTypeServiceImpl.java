package com.qing.hospital.service.impl;

import java.util.List;

import com.qing.common.enums.HospitalDrugTypeEnum;
import com.qing.hospital.vo.drug.DrugTypeTreeVo;
import com.qing.hospital.vo.drug.DrugTypeVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.DrugTypeMapper;
import com.qing.hospital.domain.DrugType;
import com.qing.hospital.service.IDrugTypeService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@Service
public class DrugTypeServiceImpl implements IDrugTypeService 
{
    @Resource
    private DrugTypeMapper drugTypeMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public DrugType selectDrugTypeById(Long id)
    {
        return drugTypeMapper.selectDrugTypeById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param drugType 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<DrugType> selectDrugTypeList(DrugType drugType)
    {
        return drugTypeMapper.selectDrugTypeList(drugType);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param drugType 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertDrugType(DrugType drugType)
    {
        drugType.setCreateTime(DateUtils.getNowDate());
        return drugTypeMapper.insertDrugType(drugType);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param drugType 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateDrugType(DrugType drugType)
    {
        drugType.setUpdateTime(DateUtils.getNowDate());
        return drugTypeMapper.updateDrugType(drugType);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDrugTypeByIds(Long[] ids)
    {
        return drugTypeMapper.deleteDrugTypeByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDrugTypeById(Long id)
    {
        return drugTypeMapper.deleteDrugTypeById(id);
    }

    /**
     * 获取药品类型（封装为树形返回）;  递归方法实现树形封装
     * @param drugTypeId
     * @return
     */
    @Override
    public DrugTypeTreeVo selectDrugTypeTreeById(Long drugTypeId) {
        DrugTypeTreeVo drugTypeTreeVo1 = new DrugTypeTreeVo();
        DrugType drugType = drugTypeMapper.selectDrugTypeById(drugTypeId);
        BeanUtils.copyProperties(drugType,drugTypeTreeVo1);
        if (drugType.getType()!= HospitalDrugTypeEnum.DRUG_TYPE_1_ENUM.getCode()){
            DrugTypeTreeVo drugTypeTreeVo = selectDrugTypeTreeById(drugType.getpId());
            if (drugTypeTreeVo.getChildren()==null){
                drugTypeTreeVo.setChildren(drugTypeTreeVo1);
            }else {
                DrugTypeTreeVo children = drugTypeTreeVo.getChildren();
                children.setChildren(drugTypeTreeVo1);
            }
            return drugTypeTreeVo;
        }
        return drugTypeTreeVo1;
    }

    /**
     * 获取药品一级类型
     * @param id 药品类型id（一级，二级，三级）
     * @return 获取药品类型一级类型
     */
    @Override
    public DrugTypeVo selectStairDrugType(Long id) {
        DrugType drugType = selectDrugTypeById(id);
        DrugTypeVo drugTypeVo = new DrugTypeVo();
        if (drugType.getType()== HospitalDrugTypeEnum.DRUG_TYPE_1_ENUM.getCode()){
            BeanUtils.copyProperties(drugType,drugTypeVo);
        }else if (drugType.getType()== HospitalDrugTypeEnum.DRUG_TYPE_2_ENUM.getCode()){
            DrugType drugType1 = selectDrugTypeById(drugType.getpId());
            BeanUtils.copyProperties(drugType1,drugTypeVo);
        }else {
            DrugType drugType1 = selectDrugTypeById(drugType.getpId());
            DrugType drugType2 = selectDrugTypeById(drugType1.getpId());
            BeanUtils.copyProperties(drugType2,drugTypeVo);
        }
        return drugTypeVo;
    }
}
