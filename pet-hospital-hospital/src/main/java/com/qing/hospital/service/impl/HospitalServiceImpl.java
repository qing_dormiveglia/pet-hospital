package com.qing.hospital.service.impl;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.qing.common.dubbo.HospitalApi;
import com.qing.common.enums.HospitalTagEnum;
import com.qing.common.to.HospitalTo;
import com.qing.common.utils.StringUtil;
import com.qing.hospital.dto.hospital.HospitalDto;
import com.qing.hospital.vo.dep.DepVo;
import com.qing.hospital.vo.hospital.HospitalInfoVo;
import com.qing.hospital.vo.hospital.HospitalVo;
import com.qing.hospital.vo.tag.TagVo;
import com.ruoyi.common.utils.DateUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.HospitalMapper;
import com.qing.hospital.domain.Hospital;
import com.qing.hospital.service.IHospitalService;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@DubboService(interfaceClass = HospitalApi.class)
@Component
public class HospitalServiceImpl implements IHospitalService, HospitalApi
{


    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private HospitalMapper hospitalMapper;
    @Resource
    private TagServiceImpl tagService;
    @Resource
    private DepServiceImpl depService;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Hospital selectHospitalById(Long id)
    {
        return hospitalMapper.selectHospitalById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hospital 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Hospital> selectHospitalList(Hospital hospital)
    {
        return hospitalMapper.selectHospitalList(hospital);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param hospital 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertHospital(Hospital hospital)
    {
        hospital.setCreateTime(DateUtils.getNowDate());
        return hospitalMapper.insertHospital(hospital);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param hospital 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateHospital(Hospital hospital)
    {
        hospital.setUpdateTime(DateUtils.getNowDate());
        return hospitalMapper.updateHospital(hospital);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHospitalByIds(Long[] ids)
    {
        return hospitalMapper.deleteHospitalByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHospitalById(Long id)
    {
        return hospitalMapper.deleteHospitalById(id);
    }

    /**
     * 查询医院列表
     * @param hospitalDto
     * @return
     */
    @Override
    public List<HospitalVo> findHospitalVoList(HospitalDto hospitalDto) {
        List<Hospital> hospitalList = hospitalMapper.selectHospitalListByHospitalDto(hospitalDto);
        List<HospitalVo> list = hospitalList.stream().map(item -> {
            List<Long> longs = StringUtil.tagSplit(item.getTag());
            HospitalVo hospitalVo = new HospitalVo();
            BeanUtils.copyProperties(item, hospitalVo);
            List<TagVo> tagList = tagService.selectTagBatchByIds(longs, HospitalTagEnum.HOSPITAL_TAG_ENUM.getCode());
            hospitalVo.setTagByHospitalVos(tagList);
            return hospitalVo;
        }).collect(Collectors.toList());
        return list;
    }

    /**
     * 获取医院详情信息
     * @param id
     * @return
     */
    @Override
    public HospitalInfoVo selectHospitalVoById(Long id) {
        String s = stringRedisTemplate.opsForValue().get("HospitalInfoVo:" + id);
        if (!ObjectUtils.isEmpty(s)){
            HospitalInfoVo hospitalInfoVo = JSONObject.parseObject(s, HospitalInfoVo.class);
            return hospitalInfoVo;
        }
        //查询医院详情信息
        Hospital hospital = hospitalMapper.selectHospitalById(id);
        HospitalInfoVo hospitalInfoVo = new HospitalInfoVo();
        BeanUtils.copyProperties(hospital,hospitalInfoVo);
        //查询科室信息
        List<Long> longs = StringUtil.tagSplit(hospital.getDepIds());
        List<DepVo>  depVos = depService.selectDepBatchByIds(longs);
        DepVo depVo = new DepVo(0L,"全部");
        depVos.add(0,depVo);
        hospitalInfoVo.setDepVos(depVos);
//        查询标签信息
        List<Long> ids = StringUtil.tagSplit(hospital.getTag());
        List<TagVo> tagList = tagService.selectTagBatchByIds(ids, HospitalTagEnum.HOSPITAL_TAG_ENUM.getCode());
        hospitalInfoVo.setTagVos(tagList);
        String s1 = JSON.toJSONString(hospitalInfoVo);
        stringRedisTemplate.opsForValue().set("HospitalInfoVo:" + id,s1, 1,TimeUnit.DAYS);
        return hospitalInfoVo;
    }

    @Override
    public HospitalTo getHospitalDuBboApi(Long id) {
        Hospital hospital = hospitalMapper.selectHospitalById(id);
        HospitalTo hospitalTo = new HospitalTo();
        BeanUtils.copyProperties(hospital,hospitalTo);
        return hospitalTo;
    }
}


