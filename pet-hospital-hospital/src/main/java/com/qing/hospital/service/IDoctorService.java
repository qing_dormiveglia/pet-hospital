package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.Doctor;
import com.qing.hospital.dto.doctor.DoctorDto;
import com.qing.hospital.vo.doctor.DoctorInfoVo;
import com.qing.hospital.vo.doctor.DoctorVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface IDoctorService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Doctor selectDoctorById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param doctor 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Doctor> selectDoctorList(Doctor doctor);

    /**
     * 新增【请填写功能名称】
     * 
     * @param doctor 【请填写功能名称】
     * @return 结果
     */
    public int insertDoctor(Doctor doctor);

    /**
     * 修改【请填写功能名称】
     * 
     * @param doctor 【请填写功能名称】
     * @return 结果
     */
    public int updateDoctor(Doctor doctor);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteDoctorByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDoctorById(Long id);

    /**
     * 查询医生列表信息
     * @param doctorDto
     * @return
     */
    List<DoctorVo> findPageDoctorList(DoctorDto doctorDto);

    /**
     * 查询医生详情信息
     * @param id
     * @return
     */
    DoctorInfoVo selectDoctorInfoById(Long id);
    /**
     * 查询医生基本详情信息
     * @param id
     * @return
     */
    public DoctorInfoVo selectDoctorInfoByOrderById(Long id);
}
