package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.Tag;
import com.qing.hospital.vo.tag.TagVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface ITagService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Tag selectTagById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tag 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Tag> selectTagList(Tag tag);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tag 【请填写功能名称】
     * @return 结果
     */
    public int insertTag(Tag tag);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tag 【请填写功能名称】
     * @return 结果
     */
    public int updateTag(Tag tag);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteTagByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTagById(Long id);

    /**
     * 批量查询标签信息
     * @param ids
     * @return
     */
    List<TagVo> selectTagBatchByIds(List<Long> ids, Integer type);
}
