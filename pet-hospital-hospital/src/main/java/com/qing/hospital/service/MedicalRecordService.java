package com.qing.hospital.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qing.hospital.domain.MedicalRecordEntity;
import com.qing.hospital.vo.medical.MedicalVo;
import com.ruoyi.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zzww
 * @email qing@163.com
 * @date 2024-04-26 12:01:15
 */
public interface MedicalRecordService extends IService<MedicalRecordEntity> {

    /**
     * 获取列表信息
     * @param params
     * @param id
     * @return
     */
    List<MedicalRecordEntity> listPage(Map<String, Object> params,Long id);

    /**
     * 获取详情信息
     * @param id
     * @return
     */
    MedicalVo selectInfoVoById(Long id);

//    PageUtils queryPage(Map<String, Object> params);
}

