package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.DrugType;
import com.qing.hospital.vo.drug.DrugTypeTreeVo;
import com.qing.hospital.vo.drug.DrugTypeVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface IDrugTypeService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public DrugType selectDrugTypeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param drugType 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<DrugType> selectDrugTypeList(DrugType drugType);

    /**
     * 新增【请填写功能名称】
     * 
     * @param drugType 【请填写功能名称】
     * @return 结果
     */
    public int insertDrugType(DrugType drugType);

    /**
     * 修改【请填写功能名称】
     * 
     * @param drugType 【请填写功能名称】
     * @return 结果
     */
    public int updateDrugType(DrugType drugType);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteDrugTypeByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDrugTypeById(Long id);

    /**
     * 获取药品类型（封装为树形返回）;  递归方法实现树形封装
     * @param drugTypeId
     * @return
     */
    DrugTypeTreeVo selectDrugTypeTreeById(Long drugTypeId);

    /**
     * 获取药品一级类型
     * @param id 药品类型id（一级，二级，三级）
     * @return 获取药品类型一级类型
     */
    DrugTypeVo selectStairDrugType(Long id);
}
