package com.qing.hospital.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.qing.hospital.vo.tag.TagVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.TagMapper;
import com.qing.hospital.domain.Tag;
import com.qing.hospital.service.ITagService;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@Service
public class TagServiceImpl implements ITagService 
{
    @Resource
    private TagMapper tagMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Tag selectTagById(Long id)
    {
        return tagMapper.selectTagById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tag 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Tag> selectTagList(Tag tag)
    {
        return tagMapper.selectTagList(tag);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param tag 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTag(Tag tag)
    {
        tag.setCreateTime(DateUtils.getNowDate());
        return tagMapper.insertTag(tag);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param tag 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTag(Tag tag)
    {
        tag.setUpdateTime(DateUtils.getNowDate());
        return tagMapper.updateTag(tag);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTagByIds(Long[] ids)
    {
        return tagMapper.deleteTagByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTagById(Long id)
    {
        return tagMapper.deleteTagById(id);
    }

    /**
     * 批量查询标签1信息
     * @param ids ids
     * @param type 类型（0-医生类型 ）
     * @return
     */
    @Override
    public List<TagVo> selectTagBatchByIds(List<Long> ids, Integer type) {
        if (ObjectUtils.isEmpty(ids)||ObjectUtils.isEmpty(type)||ids.isEmpty()){
            throw new RuntimeException("缺少行参参数");
        }
        List<Tag> list=tagMapper.selectTagBatchByIds(ids,type);
        List<TagVo> collect = list.stream().map(item -> {
            TagVo tagVo = new TagVo();
            BeanUtils.copyProperties(item, tagVo);
            return tagVo;
        }).collect(Collectors.toList());
        return collect;
    }
}
