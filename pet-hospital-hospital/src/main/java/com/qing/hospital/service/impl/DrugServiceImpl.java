package com.qing.hospital.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.qing.common.enums.HospitalDrugTypeEnum;
import com.qing.common.enums.HospitalTagEnum;
import com.qing.common.utils.StringUtil;
import com.qing.hospital.domain.DrugType;
import com.qing.hospital.domain.Hospital;
import com.qing.hospital.dto.drug.DrugDto;
import com.qing.hospital.vo.drug.DrugInfoVo;
import com.qing.hospital.vo.drug.DrugTypeTreeVo;
import com.qing.hospital.vo.drug.DrugTypeVo;
import com.qing.hospital.vo.drug.DrugVo;
import com.qing.hospital.vo.hospital.HospitalVo;
import com.qing.hospital.vo.tag.TagVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.DrugMapper;
import com.qing.hospital.domain.Drug;
import com.qing.hospital.service.IDrugService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@Service
public class DrugServiceImpl implements IDrugService 
{
    @Resource
    private DrugMapper drugMapper;
    @Resource
    private DrugTypeServiceImpl drugTypeService;
    @Resource
    private HospitalServiceImpl hospitalService;
    @Resource
    private TagServiceImpl tagService;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Drug selectDrugById(Long id)
    {
        return drugMapper.selectDrugById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param drug 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Drug> selectDrugList(Drug drug)
    {
        return drugMapper.selectDrugList(drug);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param drug 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertDrug(Drug drug)
    {
        drug.setCreateTime(DateUtils.getNowDate());
        return drugMapper.insertDrug(drug);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param drug 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateDrug(Drug drug)
    {
        drug.setUpdateTime(DateUtils.getNowDate());
        return drugMapper.updateDrug(drug);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDrugByIds(Long[] ids)
    {
        return drugMapper.deleteDrugByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDrugById(Long id)
    {
        return drugMapper.deleteDrugById(id);
    }

    /**
     * 查询药品列表
     * @param drugDto
     * @return
     */
    @Override
    public List<DrugVo> findDrugVoList(DrugDto drugDto) {
        List<Drug> drugs = drugMapper.selectDrugByDrugDto(drugDto);
        List<DrugVo> collect = drugs.stream().map(item -> {
            DrugVo drugVo = new DrugVo();
            DrugTypeVo stairDrugType = drugTypeService.selectStairDrugType(item.getDrugTypeId());
            drugVo.setDrugTypeVo(stairDrugType);
            BeanUtils.copyProperties(item, drugVo);
            return drugVo;
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 查询药品详情信息
     * @param id 药品id
     * @return
     */
    @Override
    public DrugInfoVo selectDrugInfoVoById(Long id) {
        //获取药品基本信息
        Drug drug = drugMapper.selectDrugById(id);
        DrugInfoVo drugInfoVo = new DrugInfoVo();
        BeanUtils.copyProperties(drug,drugInfoVo);
        //获取医院信息
        Hospital hospital = hospitalService.selectHospitalById(drug.getHospitalId());
        HospitalVo hospitalVo = new HospitalVo();
        BeanUtils.copyProperties(hospital,hospitalVo);
        drugInfoVo.setHospital(hospitalVo);
        //药品类型
        DrugTypeTreeVo drugTypeTreeVo = drugTypeService.selectDrugTypeTreeById(drug.getDrugTypeId());
        drugInfoVo.setChildren(drugTypeTreeVo);
        //标签
        List<Long> longs = StringUtil.tagSplit(drug.getTagIds());
        List<TagVo> tagVos = tagService.selectTagBatchByIds(longs, HospitalTagEnum.DRUG_TAG_ENUM.getCode());
        drugInfoVo.setTags(tagVos);
        return drugInfoVo;
    }

}
