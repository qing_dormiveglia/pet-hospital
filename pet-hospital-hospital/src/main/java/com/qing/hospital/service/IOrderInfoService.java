package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.OrderInfo;
import com.qing.hospital.vo.order.OrderInfoVo;
import com.qing.hospital.vo.order.OrderPetInfoVo;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface IOrderInfoService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public OrderInfo selectOrderInfoById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param orderInfo 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<OrderInfo> selectOrderInfoList(OrderInfo orderInfo);

    /**
     * 新增【请填写功能名称】
     * 
     * @param orderInfo 【请填写功能名称】
     * @return 结果
     */
    public int insertOrderInfo(OrderInfo orderInfo);

    /**
     * 修改【请填写功能名称】
     * 
     * @param orderInfo 【请填写功能名称】
     * @return 结果
     */
    public int updateOrderInfo(OrderInfo orderInfo);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteOrderInfoByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteOrderInfoById(Long id);

    /**
     * 预约挂号
     * @param list
     * @return
     */
    void insertOrderScheduleInfo(List<Long> list);

    /**
     * 获取预约列表
     * @return
     */
    TableDataInfo selectOrderInfoVoList();

    /**
     * 获取预约详情
     * @param id
     * @return
     */
    OrderPetInfoVo selectOrderInfoVoById(Long id);
}
