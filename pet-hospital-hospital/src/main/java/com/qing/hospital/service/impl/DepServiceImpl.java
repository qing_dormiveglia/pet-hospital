package com.qing.hospital.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.qing.hospital.vo.dep.DepVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.DepMapper;
import com.qing.hospital.domain.Dep;
import com.qing.hospital.service.IDepService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@Service
public class DepServiceImpl implements IDepService 
{
    @Resource
    private DepMapper depMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Dep selectDepById(Long id)
    {
        return depMapper.selectDepById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dep 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Dep> selectDepList(Dep dep)
    {
        return depMapper.selectDepList(dep);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param dep 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertDep(Dep dep)
    {
        dep.setCreateTime(DateUtils.getNowDate());
        return depMapper.insertDep(dep);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param dep 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateDep(Dep dep)
    {
        dep.setUpdateTime(DateUtils.getNowDate());
        return depMapper.updateDep(dep);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDepByIds(Long[] ids)
    {
        return depMapper.deleteDepByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDepById(Long id)
    {
        return depMapper.deleteDepById(id);
    }

    /**
     * 批量查询（根据id批量查询科室信息）
     * @param longs
     * @return
     */
    @Override
    public List<DepVo> selectDepBatchByIds(List<Long> longs) {
        List<Dep> list = depMapper.selectDepBatchByIds(longs);
        List<DepVo> collect = list.stream().map(item -> {
            DepVo depVo = new DepVo();
            BeanUtils.copyProperties(item, depVo);
            return depVo;
        }).collect(Collectors.toList());
        return collect;

    }
}
