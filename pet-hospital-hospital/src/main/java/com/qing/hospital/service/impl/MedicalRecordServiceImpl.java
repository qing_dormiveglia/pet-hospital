package com.qing.hospital.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qing.hospital.domain.Hospital;
import com.qing.hospital.domain.MedicalRecordEntity;
import com.qing.hospital.mapper.MedicalRecordMapper;
import com.qing.hospital.service.MedicalRecordService;
import com.qing.hospital.vo.medical.MedicalVo;
import com.sun.javafx.applet.HostServicesImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Service("medicalRecordService")
public class MedicalRecordServiceImpl extends ServiceImpl<MedicalRecordMapper, MedicalRecordEntity> implements MedicalRecordService {

    @Resource
    private HospitalServiceImpl hospitalService;

    /**
     * 获取列表
     * @param params
     * @param id
     * @return
     */
    @Override
    public List<MedicalRecordEntity> listPage(Map<String, Object> params,Long id) {
        Object keyWord = params.get("keyWord");
        LambdaQueryWrapper<MedicalRecordEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(MedicalRecordEntity::getHospitalId,id);
        if (!StringUtils.isEmpty(keyWord)){
            wrapper.like(MedicalRecordEntity::getName,keyWord).or().like(MedicalRecordEntity::getIdCode,keyWord)
                    .or().like(MedicalRecordEntity::getChiefComplaint,keyWord);
        }
        wrapper.orderByDesc(MedicalRecordEntity::getCreateTime);
        List<MedicalRecordEntity> list = list(wrapper);
        return list;
    }

    /**
     * 获取详情
     * @param id
     * @return
     */
    @Override
    public MedicalVo selectInfoVoById(Long id) {
        MedicalRecordEntity medicalRecordEntity = getById(id);
        MedicalVo medicalVo = new MedicalVo();
        BeanUtils.copyProperties(medicalRecordEntity,medicalVo);
        Hospital hospital = hospitalService.selectHospitalById(medicalVo.getHospitalId());
        medicalVo.setHospital(hospital);
        return medicalVo;
    }

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<MedicalRecordEntity> page = this.page(
//                new Query<MedicalRecordEntity>().getPage(params),
//                new QueryWrapper<MedicalRecordEntity>()
//        );
//
//        return new PageUtils(page);
//    }

}