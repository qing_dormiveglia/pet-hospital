package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.Schedule;
import com.qing.hospital.vo.hospital.ScheduleInfoVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface IScheduleService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Schedule selectScheduleById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param schedule 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Schedule> selectScheduleList(Schedule schedule);

    /**
     * 新增【请填写功能名称】
     * 
     * @param schedule 【请填写功能名称】
     * @return 结果
     */
    public int insertSchedule(Schedule schedule);

    /**
     * 修改【请填写功能名称】
     * 
     * @param schedule 【请填写功能名称】
     * @return 结果
     */
    public int updateSchedule(Schedule schedule);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteScheduleByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteScheduleById(Long id);

    /**
     * 根据医生id查询近七日的排班信息
     * @param id 医生id
     * @return
     */
    List<Schedule> selectScheduleListByDoctorId(Long id);

    /**
     * 获取医生排班详情
     * @param id
     * @return
     */
    List<ScheduleInfoVo> selectScheduleByDoctorId(Long id);
}
