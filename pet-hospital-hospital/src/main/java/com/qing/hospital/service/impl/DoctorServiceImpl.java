package com.qing.hospital.service.impl;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

import com.qing.common.enums.HospitalTagEnum;
import com.qing.common.utils.StringUtil;
import com.qing.hospital.domain.*;
import com.qing.hospital.dto.doctor.DoctorDto;
import com.qing.hospital.vo.dep.DepVo;
import com.qing.hospital.vo.doctor.DoctorInfoVo;
import com.qing.hospital.vo.doctor.DoctorVo;
import com.qing.hospital.vo.doctor.ScheduleVo;
import com.qing.hospital.vo.hospital.HospitalByDoctorVo;
import com.qing.hospital.vo.hospital.ScheduleInfoVo;
import com.qing.hospital.vo.tag.TagVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.DoctorMapper;
import com.qing.hospital.service.IDoctorService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@Service
public class DoctorServiceImpl implements IDoctorService 
{
    @Resource
    private DoctorMapper doctorMapper;
    @Resource
    private DepServiceImpl depService;
    @Resource
    private HospitalServiceImpl hospitalService;
    @Resource
    private TagServiceImpl tagService;
    @Resource
    private ScheduleServiceImpl scheduleService;


    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Doctor selectDoctorById(Long id)
    {
        return doctorMapper.selectDoctorById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param doctor 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Doctor> selectDoctorList(Doctor doctor)
    {
        return doctorMapper.selectDoctorList(doctor);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param doctor 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertDoctor(Doctor doctor)
    {
        doctor.setCreateTime(DateUtils.getNowDate());
        return doctorMapper.insertDoctor(doctor);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param doctor 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateDoctor(Doctor doctor)
    {
        doctor.setUpdateTime(DateUtils.getNowDate());
        return doctorMapper.updateDoctor(doctor);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDoctorByIds(Long[] ids)
    {
        return doctorMapper.deleteDoctorByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDoctorById(Long id)
    {
        return doctorMapper.deleteDoctorById(id);
    }

    /**
     * 查询医生列表（分页）
     * @param doctorDto
     * @return
     */
    @Override
    public List<DoctorVo> findPageDoctorList(DoctorDto doctorDto) {
        List<Doctor> doctorList = doctorMapper.selectDoctorVoList(doctorDto);
        List<DoctorVo> collect = doctorList.stream().map(item -> {
            DoctorVo doctorVo = new DoctorVo();
            BeanUtils.copyProperties(item, doctorVo);
            return doctorVo;
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 查询医生详情信息
     * @param id
     * @return
     */
    @Override
    public DoctorInfoVo selectDoctorInfoById(Long id) {
        //获取医生基本信息
        Doctor doctor = doctorMapper.selectDoctorById(id);
        DoctorInfoVo doctorInfoVo = new DoctorInfoVo();
        BeanUtils.copyProperties(doctor,doctorInfoVo);
        //获取科室基本信息
        Long depId = doctor.getDepId();
        DepVo depVo = new DepVo();
        Dep dep = depService.selectDepById(depId);
        BeanUtils.copyProperties(dep,depVo);
        doctorInfoVo.setDep(depVo);
        //获取医院基本信息
        Long hospitalId = doctor.getHospitalId();
        Hospital hospital = hospitalService.selectHospitalById(hospitalId);
        HospitalByDoctorVo hospitalByDoctorVo = new HospitalByDoctorVo();
        BeanUtils.copyProperties(hospital,hospitalByDoctorVo);
        doctorInfoVo.setHospital(hospitalByDoctorVo);
        //获取标签列表
        List<Long> longs = StringUtil.tagSplit(doctor.getTag());
        List<TagVo> tagList= tagService.selectTagBatchByIds(longs, HospitalTagEnum.DOCTOR_TAG_ENUM.getCode());
        doctorInfoVo.setTagList(tagList);
        //获取排班信息
        List<Schedule> scheduleList = scheduleService.selectScheduleListByDoctorId(doctor.getId());
        Date date = new Date();
        ArrayList<ScheduleInfoVo> scheduleInfoVos = new ArrayList<>();
        for (int i = 0;i<=6;i++){
            ScheduleInfoVo scheduleInfoVo = new ScheduleInfoVo();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, i);
            Date nextDate = calendar.getTime();
            ArrayList<ScheduleVo> schedules = new ArrayList<>();
            for (Schedule schedule: scheduleList
            ) {
                Date workDate = schedule.getWorkDate();
                boolean bool = DateUtils.isSameDay(workDate, nextDate);
                if(bool){
                    ScheduleVo scheduleVo = new ScheduleVo();
                    BeanUtils.copyProperties(schedule,scheduleVo);
                    schedules.add(scheduleVo);
                }
            }
            scheduleInfoVo.setTimes(schedules);
            scheduleInfoVo.setTime(nextDate);
            // 获取当前日期
            Instant instant = nextDate.toInstant();
            ZoneId zoneId = ZoneId.systemDefault();
            // atZone()方法返回在指定时区从此Instant生成的ZonedDateTime。
            LocalDate localDate = instant.atZone(zoneId).toLocalDate();
            // 获取当前日期是周几
            DayOfWeek dayOfWeek = localDate.getDayOfWeek();
            String dayOfWeekChinese = dayOfWeek.getDisplayName(TextStyle.FULL_STANDALONE, Locale.CHINA);
            scheduleInfoVo.setDateNameWeek(dayOfWeekChinese);
            scheduleInfoVos.add(scheduleInfoVo);
        }
        doctorInfoVo.setScheduleVoList(scheduleInfoVos);
        return doctorInfoVo;
    }

    /**
     * 查询医生详情信息
     * @param id
     * @return
     */
    @Override
    public DoctorInfoVo selectDoctorInfoByOrderById(Long id) {
        //获取医生基本信息
        Doctor doctor = doctorMapper.selectDoctorById(id);
        DoctorInfoVo doctorInfoVo = new DoctorInfoVo();
        BeanUtils.copyProperties(doctor,doctorInfoVo);
        //获取科室基本信息
        Long depId = doctor.getDepId();
        DepVo depVo = new DepVo();
        Dep dep = depService.selectDepById(depId);
        BeanUtils.copyProperties(dep,depVo);
        doctorInfoVo.setDep(depVo);
        //获取标签列表
        List<Long> longs = StringUtil.tagSplit(doctor.getTag());
        List<TagVo> tagList= tagService.selectTagBatchByIds(longs, HospitalTagEnum.DOCTOR_TAG_ENUM.getCode());
        doctorInfoVo.setTagList(tagList);
        return doctorInfoVo;
    }
}
