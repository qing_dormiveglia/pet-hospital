package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.Hospital;
import com.qing.hospital.dto.hospital.HospitalDto;
import com.qing.hospital.vo.hospital.HospitalInfoVo;
import com.qing.hospital.vo.hospital.HospitalVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface IHospitalService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Hospital selectHospitalById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hospital 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Hospital> selectHospitalList(Hospital hospital);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hospital 【请填写功能名称】
     * @return 结果
     */
    public int insertHospital(Hospital hospital);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hospital 【请填写功能名称】
     * @return 结果
     */
    public int updateHospital(Hospital hospital);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteHospitalByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteHospitalById(Long id);

    /**
     * 查询医院列表
     * @param hospitalDto
     * @return
     */
    List<HospitalVo> findHospitalVoList(HospitalDto hospitalDto);

    /**
     * 获取医院详情信息
     * @param id
     * @return
     */
    HospitalInfoVo selectHospitalVoById(Long id);
}
