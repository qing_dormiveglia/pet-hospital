package com.qing.hospital.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.qing.hospital.vo.advertising.AdvertisingVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.ImagesMapper;
import com.qing.hospital.domain.Images;
import com.qing.hospital.service.IImagesService;

import javax.annotation.Resource;

/**
 * 图集Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@Service
public class ImagesServiceImpl implements IImagesService 
{
    @Resource
    private ImagesMapper imagesMapper;

    /**
     * 查询图集
     * 
     * @param id 图集主键
     * @return 图集
     */
    @Override
    public Images selectImagesById(Long id)
    {
        return imagesMapper.selectImagesById(id);
    }

    /**
     * 查询图集列表
     * 
     * @param images 图集
     * @return 图集
     */
    @Override
    public List<Images> selectImagesList(Images images)
    {
        return imagesMapper.selectImagesList(images);
    }

    /**
     * 新增图集
     * 
     * @param images 图集
     * @return 结果
     */
    @Override
    public int insertImages(Images images)
    {
        images.setCreateTime(DateUtils.getNowDate());
        return imagesMapper.insertImages(images);
    }

    /**
     * 修改图集
     * 
     * @param images 图集
     * @return 结果
     */
    @Override
    public int updateImages(Images images)
    {
        images.setUpdateTime(DateUtils.getNowDate());
        return imagesMapper.updateImages(images);
    }

    /**
     * 批量删除图集
     * 
     * @param ids 需要删除的图集主键
     * @return 结果
     */
    @Override
    public int deleteImagesByIds(Long[] ids)
    {
        return imagesMapper.deleteImagesByIds(ids);
    }

    /**
     * 删除图集信息
     * 
     * @param id 图集主键
     * @return 结果
     */
    @Override
    public int deleteImagesById(Long id)
    {
        return imagesMapper.deleteImagesById(id);
    }

    /**
     * 查询广告
     * @return
     */
    @Override
    public List<AdvertisingVo> selectImagesAdvertising() {
        Images images = new Images();
        images.setType(0);
        List<Images> imagesList = imagesMapper.selectImagesList(images);
        List<AdvertisingVo> collect = imagesList.stream().map(item -> {
            AdvertisingVo advertisingVo = new AdvertisingVo();
            BeanUtils.copyProperties(item, advertisingVo);
            return advertisingVo;
        }).collect(Collectors.toList());
        return collect;
    }
}
