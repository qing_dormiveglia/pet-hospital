package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.Drug;
import com.qing.hospital.dto.drug.DrugDto;
import com.qing.hospital.vo.drug.DrugInfoVo;
import com.qing.hospital.vo.drug.DrugVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface IDrugService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Drug selectDrugById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param drug 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Drug> selectDrugList(Drug drug);

    /**
     * 新增【请填写功能名称】
     * 
     * @param drug 【请填写功能名称】
     * @return 结果
     */
    public int insertDrug(Drug drug);

    /**
     * 修改【请填写功能名称】
     * 
     * @param drug 【请填写功能名称】
     * @return 结果
     */
    public int updateDrug(Drug drug);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteDrugByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDrugById(Long id);

    /**
     * 查询药品列表
     * @param drugDto
     * @return
     */
    List<DrugVo> findDrugVoList(DrugDto drugDto);

    /**
     * 查询药品详情信息
     * @param id
     * @return
     */
    DrugInfoVo selectDrugInfoVoById(Long id);
}
