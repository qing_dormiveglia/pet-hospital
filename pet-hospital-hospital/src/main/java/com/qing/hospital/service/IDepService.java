package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.Dep;
import com.qing.hospital.vo.dep.DepVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface IDepService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Dep selectDepById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dep 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Dep> selectDepList(Dep dep);

    /**
     * 新增【请填写功能名称】
     * 
     * @param dep 【请填写功能名称】
     * @return 结果
     */
    public int insertDep(Dep dep);

    /**
     * 修改【请填写功能名称】
     * 
     * @param dep 【请填写功能名称】
     * @return 结果
     */
    public int updateDep(Dep dep);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteDepByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDepById(Long id);

    /**
     * 批量查询（根据id批量查询科室信息）
     * @param longs
     * @return
     */
    List<DepVo> selectDepBatchByIds(List<Long> longs);
}
