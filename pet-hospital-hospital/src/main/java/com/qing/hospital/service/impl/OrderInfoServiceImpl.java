package com.qing.hospital.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.github.pagehelper.PageInfo;
import com.qing.common.utils.WebUtils;
import com.qing.hospital.domain.Doctor;
import com.qing.hospital.domain.Hospital;
import com.qing.hospital.domain.Schedule;
import com.qing.hospital.vo.doctor.DoctorInfoVo;
import com.qing.hospital.vo.order.OrderInfoVo;
import com.qing.hospital.vo.order.OrderPetInfoVo;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.hospital.mapper.OrderInfoMapper;
import com.qing.hospital.domain.OrderInfo;
import com.qing.hospital.service.IOrderInfoService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-07
 */
@Service
public class OrderInfoServiceImpl implements IOrderInfoService 
{
    @Resource
    private OrderInfoMapper orderInfoMapper;
    @Resource
    private ScheduleServiceImpl scheduleService;
    @Resource
    private HospitalServiceImpl hospitalService;
    @Resource
    private DoctorServiceImpl doctorService;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public OrderInfo selectOrderInfoById(Long id)
    {
        return orderInfoMapper.selectOrderInfoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param orderInfo 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<OrderInfo> selectOrderInfoList(OrderInfo orderInfo)
    {
        return orderInfoMapper.selectOrderInfoList(orderInfo);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param orderInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertOrderInfo(OrderInfo orderInfo)
    {
        orderInfo.setCreateTime(DateUtils.getNowDate());
        return orderInfoMapper.insertOrderInfo(orderInfo);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param orderInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateOrderInfo(OrderInfo orderInfo)
    {
        orderInfo.setUpdateTime(DateUtils.getNowDate());
        return orderInfoMapper.updateOrderInfo(orderInfo);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteOrderInfoByIds(Long[] ids)
    {
        return orderInfoMapper.deleteOrderInfoByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteOrderInfoById(Long id)
    {
        return orderInfoMapper.deleteOrderInfoById(id);
    }

    /**
     * 预约挂号
     * @param list
     */
    @Override
    @Transactional
    public void insertOrderScheduleInfo(List<Long> list) {
        Long memberId = WebUtils.getMemberIdByAccessToken();
        for (Long id:list
             ) {
            OrderInfo orderInfo = new OrderInfo();
            Schedule schedule = scheduleService.selectScheduleById(id);
            Long hospitalId = schedule.getHospitalId();
            Hospital hospital = hospitalService.selectHospitalById(hospitalId);

            Long doctorId = schedule.getDoctorId();
            //用户id
            orderInfo.setMemberId(memberId);
            //排班id
            orderInfo.setScheduleId(id);
            //预约号
            Long reservedNumber = schedule.getReservedNumber();
            Long availableNumber = schedule.getAvailableNumber();
            orderInfo.setNumber((reservedNumber-availableNumber)+1);
            //取票地址
            orderInfo.setFetchAddress(hospital.getAddress()+"，按照指示牌提示取票");
            //费用
            Doctor doctor = doctorService.selectDoctorById(doctorId);
            orderInfo.setAmount(doctor.getAmount());
            orderInfo.setOrderStatus(0);
            Date workDate = schedule.getWorkDate();
            LocalDate localDate = workDate.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = localDate.format(formatter);
            //fetch_time建议取票时间
            //计算取号时间
            if (reservedNumber==0||reservedNumber==null) {
                if (schedule.getWorkTime()==0) {
                    orderInfo.setFetchTime(formattedDate+"上午8:00");
                }else{
                    orderInfo.setFetchTime(formattedDate+"下午2:00");
                }

            }else{
                Long startTimeHour = 8l; // 开始时间（小时）
//                Long startTimeMinute = 0l;
                String timed= "上午";
                if (schedule.getWorkTime()==1){
//                    startTimeMinute = 0
                    timed= "下午";
                    startTimeHour = 2l;
                }


            Long time  = 240/reservedNumber;
            Long timeData = time*orderInfo.getNumber();
            Long hours = timeData / 60; // 小时数
            Long minutes = timeData % 60; // 分钟数
            Long timeDAteHours = (startTimeHour+hours);

            orderInfo.setFetchTime(formattedDate+
                    timed+timeDAteHours.toString()+"点"+minutes.toString()+"分");

            }
            int info = insertOrderInfo(orderInfo);
            if (info!=1){
                throw new RuntimeException("失败");
            }
            schedule.setAvailableNumber(schedule.getAvailableNumber()-1);
            int i = scheduleService.updateSchedule(schedule);
            if (i!=1){
                throw new RuntimeException("失败");
            }
        }

    }

    /**
     * 获取预约列表
     * @return
     */
    @Override
    public TableDataInfo selectOrderInfoVoList() {
        Long memberId = WebUtils.getMemberIdByAccessToken();
        List<OrderInfo> orderInfoList = orderInfoMapper.selectOrderInfoListByMemberId(memberId);
        List<OrderInfoVo> collect = orderInfoList.stream().map(item -> {
            OrderInfoVo orderInfoVo = new OrderInfoVo();
            BeanUtils.copyProperties(item, orderInfoVo);
            Schedule schedule = scheduleService.selectScheduleById(item.getScheduleId());
            orderInfoVo.setSchedule(schedule);
            return orderInfoVo;
        }).collect(Collectors.toList());
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(collect);
        rspData.setTotal(new PageInfo(orderInfoList).getTotal());
        return rspData;
    }

    /**
     * 获取预约详情
     * @param id
     * @return
     */
    @Override
    public OrderPetInfoVo selectOrderInfoVoById(Long id) {
        OrderInfo orderInfo = orderInfoMapper.selectOrderInfoById(id);
        Schedule schedule = scheduleService.selectScheduleById(orderInfo.getScheduleId());
        OrderPetInfoVo orderPetInfoVo = new OrderPetInfoVo();
        BeanUtils.copyProperties(orderInfo,orderPetInfoVo);
        orderPetInfoVo.setSchedule(schedule);
        DoctorInfoVo doctorInfoVo = doctorService.selectDoctorInfoByOrderById(schedule.getDoctorId());
        orderPetInfoVo.setDoctor(doctorInfoVo);
        Hospital hospital = hospitalService.selectHospitalById(schedule.getHospitalId());
        orderPetInfoVo.setHospital(hospital);
        return orderPetInfoVo;
    }
}
