package com.qing.hospital.service;

import java.util.List;
import com.qing.hospital.domain.Images;
import com.qing.hospital.vo.advertising.AdvertisingVo;

/**
 * 图集Service接口
 * 
 * @author zzww
 * @date 2024-02-07
 */
public interface IImagesService 
{
    /**
     * 查询图集
     * 
     * @param id 图集主键
     * @return 图集
     */
    public Images selectImagesById(Long id);

    /**
     * 查询图集列表
     * 
     * @param images 图集
     * @return 图集集合
     */
    public List<Images> selectImagesList(Images images);

    /**
     * 新增图集
     * 
     * @param images 图集
     * @return 结果
     */
    public int insertImages(Images images);

    /**
     * 修改图集
     * 
     * @param images 图集
     * @return 结果
     */
    public int updateImages(Images images);

    /**
     * 批量删除图集
     * 
     * @param ids 需要删除的图集主键集合
     * @return 结果
     */
    public int deleteImagesByIds(Long[] ids);

    /**
     * 删除图集信息
     * 
     * @param id 图集主键
     * @return 结果
     */
    public int deleteImagesById(Long id);

    /**
     * 查询广告（首页轮播图）
     * @return
     */
    List<AdvertisingVo> selectImagesAdvertising();
}
