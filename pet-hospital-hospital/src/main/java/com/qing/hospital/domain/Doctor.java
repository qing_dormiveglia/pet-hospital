package com.qing.hospital.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hms_doctor
 * 
 * @author zzww
 * @date 2024-02-07
 */
public class Doctor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 医生ID */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long memberId;

    /** 科室ID */
    @Excel(name = "科室ID")
    private Long depId;

    /** 医院ID */
    @Excel(name = "医院ID")
    private Long hospitalId;

    /** 医院ID */
    @Excel(name = "城市ID")
    private Long cityId;

    /** 用逗号隔开 */
    @Excel(name = "标签id")
    private String tag;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 医院ID */
    @Excel(name = "区")
    private String region;

    /** 职位 */
    @Excel(name = "职位")
    private String position;

    /** 资历 */
    @Excel(name = "资历")
    private String qualifications;

    /** 医生介绍 */
    @Excel(name = "医生介绍")
    private String introduce;

    /** 擅长 */
    @Excel(name = "擅长")
    private String skill;

    /** 挂号费 */
    @Excel(name = "挂号费")
    private Long amount;

    /** 头像 */
    @Excel(name = "头像")
    private String headerImage;

    /** 热度 */
    @Excel(name = "热度")
    private Long heat;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getId()
    {
        return id;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setDepId(Long depId) 
    {
        this.depId = depId;
    }

    public Long getDepId() 
    {
        return depId;
    }
    public void setHospitalId(Long hospitalId) 
    {
        this.hospitalId = hospitalId;
    }

    public Long getHospitalId() 
    {
        return hospitalId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }
    public void setQualifications(String qualifications) 
    {
        this.qualifications = qualifications;
    }

    public String getQualifications() 
    {
        return qualifications;
    }
    public void setIntroduce(String introduce) 
    {
        this.introduce = introduce;
    }

    public String getIntroduce() 
    {
        return introduce;
    }
    public void setSkill(String skill) 
    {
        this.skill = skill;
    }

    public String getSkill() 
    {
        return skill;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setHeaderImage(String headerImage) 
    {
        this.headerImage = headerImage;
    }

    public String getHeaderImage() 
    {
        return headerImage;
    }
    public void setHeat(Long heat) 
    {
        this.heat = heat;
    }

    public Long getHeat() 
    {
        return heat;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("depId", getDepId())
            .append("hospitalId", getHospitalId())
                .append("cityId",getCityId())
                .append("tag",getTag())
            .append("name", getName())
                .append("region",getRegion())
            .append("position", getPosition())
            .append("qualifications", getQualifications())
            .append("introduce", getIntroduce())
            .append("skill", getSkill())
            .append("amount", getAmount())
            .append("headerImage", getHeaderImage())
            .append("heat", getHeat())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
