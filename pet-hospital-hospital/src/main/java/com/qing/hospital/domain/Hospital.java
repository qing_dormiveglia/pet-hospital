package com.qing.hospital.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hms_hospital
 * 
 * @author zzww
 * @date 2024-02-07
 */
public class Hospital extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 医院id */
    private Long id;

    /** 城市id */
    @Excel(name = "城市id")
    private Long cityId;

    /** 等级id */
    @Excel(name = "等级id")
    private Long gradeId;

    /** 科室表id（逗号隔开） */
    @Excel(name = "科室表id", readConverterExp = "逗=号隔开")
    private String depIds;

    /** 标签id（逗号隔开） */
    @Excel(name = "标签id", readConverterExp = "逗=号隔开")
    private String tag;

    /** 名称 */
    @Excel(name = "名称")
    private String name;


    /** 头图 */
    @Excel(name = "头图")
    private String headImage;

    /** 医院ID */
    @Excel(name = "区")
    private String region;

    /** 等级 */
    @Excel(name = "等级")
    private String grade;

    /** 介绍 */
    @Excel(name = "介绍")
    private String introduce;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 热度 */
    @Excel(name = "热度")
    private Long heat;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }

    public Long getId()
    {
        return id;
    }
    public void setCityId(Long cityId) 
    {
        this.cityId = cityId;
    }

    public Long getCityId() 
    {
        return cityId;
    }
    public void setGradeId(Long gradeId) 
    {
        this.gradeId = gradeId;
    }

    public Long getGradeId() 
    {
        return gradeId;
    }
    public void setDepIds(String depIds) 
    {
        this.depIds = depIds;
    }

    public String getDepIds() 
    {
        return depIds;
    }
    public void setTag(String tag) 
    {
        this.tag = tag;
    }

    public String getTag() 
    {
        return tag;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGrade(String grade) 
    {
        this.grade = grade;
    }

    public String getGrade() 
    {
        return grade;
    }
    public void setIntroduce(String introduce) 
    {
        this.introduce = introduce;
    }

    public String getIntroduce() 
    {
        return introduce;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setHeat(Long heat) 
    {
        this.heat = heat;
    }

    public Long getHeat() 
    {
        return heat;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cityId", getCityId())
            .append("gradeId", getGradeId())
            .append("depIds", getDepIds())
            .append("tag", getTag())
            .append("name", getName())
            .append("headImage", getHeadImage())
            .append("region", getRegion())
            .append("grade", getGrade())
            .append("introduce", getIntroduce())
            .append("address", getAddress())
            .append("heat", getHeat())
            .append("remark", getRemark())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
