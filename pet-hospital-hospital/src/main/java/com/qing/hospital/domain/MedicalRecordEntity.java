package com.qing.hospital.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author zzww
 * @email qing@163.com
 * @date 2024-04-26 12:01:15
 */
@Data
@TableName("hms_medical_record")
public class MedicalRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 医院id
	 */
	private Long hospitalId;
	/**
	 * 病例编号
	 */
	private String idCode;
	/**
	 * 宠物id
	 */
	private Long petId;
	/**
	 * 用户id
	 */
	private Long memberId;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 性别(0-男1-女)
	 */
	private Integer gender;
	/**
	 * 年龄
	 */
	private Integer age;
	/**
	 * 类别
	 */
	private String category;
	/**
	 * 主诉
	 */
	private String chiefComplaint;
	/**
	 * 过去史
	 */
	private String pastHistory;
	/**
	 * 家族史
	 */
	private String familyHistory;
	/**
	 * 过敏史
	 */
	private String allergyHistory;
	/**
	 * 现病史
	 */
	private String presentIllness;
	/**
	 * 体检
	 */
	private String physicalExamination;
	/**
	 * 诊断
	 */
	private String diagnosis;
	/**
	 * 建议
	 */
	private String suggestion;
	/**
	 * 就诊医生
	 */
	private String attendingPhysician;
	/**
	 * 日期
	 */
	private Date date;
	/**
	 * 处置
	 */
	private String treatment;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Integer logicalDelete;
	/**
	 * 备注
	 */
	private String remark;

}
