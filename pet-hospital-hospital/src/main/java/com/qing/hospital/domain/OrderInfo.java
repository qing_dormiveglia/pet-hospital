package com.qing.hospital.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hms_order_info
 * 
 * @author zzww
 * @date 2024-02-07
 */
public class OrderInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 挂号订单详情ID */
    private Long id;

    /** 排班ID */
    @Excel(name = "排班ID")
    private Long scheduleId;

    /** 就诊动物主人ID */
    @Excel(name = "就诊动物主人ID")
    private Long memberId;

    /** 预约号序 */
    @Excel(name = "预约号序")
    private Long number;

    /** 建议取票时间 */
    @Excel(name = "建议取票时间")
    private String fetchTime;

    /** 取票地址 */
    @Excel(name = "取票地址")
    private String fetchAddress;

    /** 服务费用 */
    @Excel(name = "服务费用")
    private Long amount;

    /** 支付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 退号时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "退号时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date quitTime;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private Integer orderStatus;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setScheduleId(Long scheduleId) 
    {
        this.scheduleId = scheduleId;
    }

    public Long getScheduleId() 
    {
        return scheduleId;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setNumber(Long number) 
    {
        this.number = number;
    }

    public Long getNumber() 
    {
        return number;
    }
    public void setFetchTime(String fetchTime) 
    {
        this.fetchTime = fetchTime;
    }

    public String getFetchTime() 
    {
        return fetchTime;
    }
    public void setFetchAddress(String fetchAddress) 
    {
        this.fetchAddress = fetchAddress;
    }

    public String getFetchAddress() 
    {
        return fetchAddress;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setPayTime(Date payTime) 
    {
        this.payTime = payTime;
    }

    public Date getPayTime() 
    {
        return payTime;
    }
    public void setQuitTime(Date quitTime) 
    {
        this.quitTime = quitTime;
    }

    public Date getQuitTime() 
    {
        return quitTime;
    }
    public void setOrderStatus(Integer orderStatus) 
    {
        this.orderStatus = orderStatus;
    }

    public Integer getOrderStatus() 
    {
        return orderStatus;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("scheduleId", getScheduleId())
            .append("memberId", getMemberId())
            .append("number", getNumber())
            .append("fetchTime", getFetchTime())
            .append("fetchAddress", getFetchAddress())
            .append("amount", getAmount())
            .append("payTime", getPayTime())
            .append("quitTime", getQuitTime())
            .append("orderStatus", getOrderStatus())
            .append("remark", getRemark())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
