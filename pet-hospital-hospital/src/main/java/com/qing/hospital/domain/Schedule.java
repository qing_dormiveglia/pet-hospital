package com.qing.hospital.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hms_schedule
 * 
 * @author zzww
 * @date 2024-02-07
 */
public class Schedule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 排班ID */
    private Long id;

    /** 医院ID */
    @Excel(name = "医院ID")
    private Long hospitalId;

    /** 科室ID */
    @Excel(name = "科室ID")
    private Long depId;

    /** 医生ID */
    @Excel(name = "医生ID")
    private Long doctorId;

    /** 排班日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "排班日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date workDate;

    /** 排班时间（0上午1下午） */
    @Excel(name = "排班时间", readConverterExp = "0=上午1下午")
    private Integer workTime;

    /** 可以预约数 */
    @Excel(name = "可以预约数")
    private Long reservedNumber;

    /** 剩余预约数 */
    @Excel(name = "剩余预约数")
    private Long availableNumber;

    /** 周 */
    @Excel(name = "周")
    private String work;

    /** 预计开始时间 */
    @Excel(name = "预计开始时间")
    @JsonFormat(pattern = "HH:mm")
    private Date beginTime;

    /** 预计结束时间 */
    @Excel(name = "预计结束时间")
    @JsonFormat(pattern = "HH:mm")
    private Date endTime;

    /** 排班状态（-1：停诊 0：停约 1：可约） */
    @Excel(name = "排班状态", readConverterExp = "-=1：停诊,0=：停约,1=：可约")
    private Integer status;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public Long getId()
    {
        return id;
    }
    public void setHospitalId(Long hospitalId) 
    {
        this.hospitalId = hospitalId;
    }

    public Long getHospitalId() 
    {
        return hospitalId;
    }
    public void setDepId(Long depId) 
    {
        this.depId = depId;
    }

    public Long getDepId() 
    {
        return depId;
    }
    public void setWorkDate(Date workDate) 
    {
        this.workDate = workDate;
    }

    public Date getWorkDate() 
    {
        return workDate;
    }
    public void setWorkTime(Integer workTime) 
    {
        this.workTime = workTime;
    }

    public Integer getWorkTime() 
    {
        return workTime;
    }
    public void setReservedNumber(Long reservedNumber) 
    {
        this.reservedNumber = reservedNumber;
    }

    public Long getReservedNumber() 
    {
        return reservedNumber;
    }
    public void setAvailableNumber(Long availableNumber) 
    {
        this.availableNumber = availableNumber;
    }

    public Long getAvailableNumber() 
    {
        return availableNumber;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("hospitalId", getHospitalId())
            .append("depId", getDepId())
            .append("doctorId", getDoctorId())
            .append("workDate", getWorkDate())
            .append("workTime", getWorkTime())
            .append("reservedNumber", getReservedNumber())
            .append("availableNumber", getAvailableNumber())
            .append("work", getWork())
            .append("begin_time", getBeginTime())
            .append("end_time", getEndTime())

            .append("status", getStatus())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
