package com.qing.hospital.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 药品对象 hms_drug
 * 
 * @author zzww
 * @date 2024-02-07
 */
public class Drug extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 药品id */
    private Long id;

    /** 城市id */
    @Excel(name = "城市id")
    private Long cityId;

    /** 医院id */
    @Excel(name = "医院id")
    private Long hospitalId;

    /** 适用宠物类型（0为通用）id */
    @Excel(name = "适用宠物类型", readConverterExp = "0=为通用")
    private Long petKindId;

    /** 药品类型id */
    @Excel(name = "药品类型id")
    private Long drugTypeId;

    /** 逗号隔开 */
    @Excel(name = "逗号隔开")
    private String tagIds;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 名称 */
    @Excel(name = "区")
    private String region;

    /** 英文名 */
    @Excel(name = "英文名")
    private String englishName;

    /** 头图地址 */
    @Excel(name = "头图地址")
    private String headImage;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** （0处方药1非处方药） */
    @Excel(name = "", readConverterExp = "0=处方药1非处方药")
    private Integer type;

    /** (0医保范围1不在医保范围) */
    @Excel(name = "(0医保范围1不在医保范围)")
    private Integer medical;

    /** 用法用量 */
    @Excel(name = "用法用量")
    private String usage;

    /** 禁忌(逗号隔开) */
    @Excel(name = "禁忌(逗号隔开)")
    private String taboo;

    /** 药品批准文号 */
    @Excel(name = "药品批准文号")
    private String approvalNumber;

    /** 保质期 */
    @Excel(name = "保质期")
    private String expirationDate;

    /** 储存方法 */
    @Excel(name = "储存方法")
    private String storageMethod;

    /** 不良反应 */
    @Excel(name = "不良反应")
    private String adverseReaction;

    /** 规格 */
    @Excel(name = "规格")
    private String specification;

    /** 成分 */
    @Excel(name = "成分")
    private String ingredient;

    /** 主治 */
    @Excel(name = "主治")
    private String cure;

    /** 药品相互作用 */
    @Excel(name = "药品相互作用")
    private String coactions;

    /** 描述 */
    @Excel(name = "描述")
    private String content;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getId()
    {
        return id;
    }
    public void setCityId(Long cityId) 
    {
        this.cityId = cityId;
    }

    public Long getCityId() 
    {
        return cityId;
    }
    public void setHospitalId(Long hospitalId) 
    {
        this.hospitalId = hospitalId;
    }

    public Long getHospitalId() 
    {
        return hospitalId;
    }
    public void setPetKindId(Long petKindId) 
    {
        this.petKindId = petKindId;
    }

    public Long getPetKindId() 
    {
        return petKindId;
    }
    public void setDrugTypeId(Long drugTypeId) 
    {
        this.drugTypeId = drugTypeId;
    }

    public Long getDrugTypeId() 
    {
        return drugTypeId;
    }
    public void setTagIds(String tagIds) 
    {
        this.tagIds = tagIds;
    }

    public String getTagIds() 
    {
        return tagIds;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setEnglishName(String englishName) 
    {
        this.englishName = englishName;
    }

    public String getEnglishName() 
    {
        return englishName;
    }
    public void setHeadImage(String headImage) 
    {
        this.headImage = headImage;
    }

    public String getHeadImage() 
    {
        return headImage;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setMedical(Integer medical) 
    {
        this.medical = medical;
    }

    public Integer getMedical() 
    {
        return medical;
    }
    public void setUsage(String usage) 
    {
        this.usage = usage;
    }

    public String getUsage() 
    {
        return usage;
    }
    public void setTaboo(String taboo) 
    {
        this.taboo = taboo;
    }

    public String getTaboo() 
    {
        return taboo;
    }
    public void setApprovalNumber(String approvalNumber) 
    {
        this.approvalNumber = approvalNumber;
    }

    public String getApprovalNumber() 
    {
        return approvalNumber;
    }
    public void setExpirationDate(String expirationDate) 
    {
        this.expirationDate = expirationDate;
    }

    public String getExpirationDate() 
    {
        return expirationDate;
    }
    public void setStorageMethod(String storageMethod) 
    {
        this.storageMethod = storageMethod;
    }

    public String getStorageMethod() 
    {
        return storageMethod;
    }
    public void setAdverseReaction(String adverseReaction) 
    {
        this.adverseReaction = adverseReaction;
    }

    public String getAdverseReaction() 
    {
        return adverseReaction;
    }
    public void setSpecification(String specification) 
    {
        this.specification = specification;
    }

    public String getSpecification() 
    {
        return specification;
    }
    public void setIngredient(String ingredient) 
    {
        this.ingredient = ingredient;
    }

    public String getIngredient() 
    {
        return ingredient;
    }
    public void setCure(String cure) 
    {
        this.cure = cure;
    }

    public String getCure() 
    {
        return cure;
    }
    public void setCoactions(String coactions) 
    {
        this.coactions = coactions;
    }

    public String getCoactions() 
    {
        return coactions;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cityId", getCityId())
            .append("hospitalId", getHospitalId())
            .append("petKindId", getPetKindId())
            .append("drugTypeId", getDrugTypeId())
            .append("tagIds", getTagIds())
            .append("name", getName())
            .append("region", getRegion())
            .append("englishName", getEnglishName())
            .append("headImage", getHeadImage())
            .append("price", getPrice())
            .append("type", getType())
            .append("medical", getMedical())
            .append("usage", getUsage())
            .append("taboo", getTaboo())
            .append("approvalNumber", getApprovalNumber())
            .append("expirationDate", getExpirationDate())
            .append("storageMethod", getStorageMethod())
            .append("adverseReaction", getAdverseReaction())
            .append("specification", getSpecification())
            .append("ingredient", getIngredient())
            .append("cure", getCure())
            .append("coactions", getCoactions())
            .append("content", getContent())
            .append("remark", getRemark())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
