package com.qing.common.exception.customizationException;

import com.qing.common.constant.PetHospitalConstant;

/**
 * @Author zzww
 * @Date 2023/11/28 23:59
 */
public class VerificationCodeException extends RuntimeException{

    private Integer code = PetHospitalConstant.ExceptionEum.SMS_CODE.getCode();;
    private String message = PetHospitalConstant.ExceptionEum.SMS_CODE.getMsg();

    public VerificationCodeException(PetHospitalConstant.ResultEnum refreshToken) {
        this.code = refreshToken.getCode();
        this.message = refreshToken.getMsg();
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }


    public VerificationCodeException(PetHospitalConstant.ExceptionEum exceptionEum) {
        this.code = exceptionEum.getCode();
        this.message = exceptionEum.getMsg();
    }

    public VerificationCodeException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public VerificationCodeException() {
    }

    public VerificationCodeException(String message) {
        this.message = message;
    }

}
