package com.qing.common.exception.customizationException;

import com.qing.common.constant.PetHospitalConstant;
import org.springframework.stereotype.Component;

/**
 * @Author zzww
 * @Date 2023/11/28 2:32
 */
@Component
public class DataInputException extends RuntimeException {

    private Integer code = PetHospitalConstant.ExceptionEum.DATA_INPUT.getCode();;
    private String message = PetHospitalConstant.ExceptionEum.DATA_INPUT.getMsg();

    public DataInputException(PetHospitalConstant.ExceptionEum resultEnum) {
        this.code = resultEnum.getCode();
        this.message = resultEnum.getMsg();
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }

    public DataInputException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public DataInputException() {
    }

    public DataInputException(String message) {
        this.message = message;
    }

}
