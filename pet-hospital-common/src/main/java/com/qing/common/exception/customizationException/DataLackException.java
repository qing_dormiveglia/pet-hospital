package com.qing.common.exception.customizationException;

import com.qing.common.constant.PetHospitalConstant;
import org.springframework.stereotype.Component;

/**
 * @Author zzww
 * @Date 2023/11/23 9:46
 */
@Component
public class DataLackException extends RuntimeException{

    private Integer code = PetHospitalConstant.ExceptionEum.DATA_LACK.getCode();;
    private String message = PetHospitalConstant.ExceptionEum.DATA_LACK.getMsg();

    public DataLackException(PetHospitalConstant.ExceptionEum resultEnum) {
        this.code = resultEnum.getCode();
        this.message = resultEnum.getMsg();
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }

    public DataLackException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public DataLackException() {
    }

    public DataLackException(String message) {
        this.message = message;
    }

}
