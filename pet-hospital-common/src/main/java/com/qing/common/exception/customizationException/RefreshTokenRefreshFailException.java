package com.qing.common.exception.customizationException;

import com.qing.common.constant.PetHospitalConstant;
import org.springframework.stereotype.Component;

/**
 * @Author zzww
 * @Date 2023/3/18 15:17
 *
 * 此类为自定义refreshToken刷新失败异常
 *
 *
 */
@Component
public class RefreshTokenRefreshFailException extends RuntimeException{

    private Integer code = PetHospitalConstant.ExceptionEum.REFRESH_TOKEN.getCode();;
    private String message = PetHospitalConstant.ExceptionEum.REFRESH_TOKEN.getMsg();

    public RefreshTokenRefreshFailException(PetHospitalConstant.ResultEnum refreshToken) {
        this.code = refreshToken.getCode();
        this.message = refreshToken.getMsg();
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }


    public RefreshTokenRefreshFailException(PetHospitalConstant.ExceptionEum exceptionEum) {
        this.code = exceptionEum.getCode();
        this.message = exceptionEum.getMsg();
    }

    public RefreshTokenRefreshFailException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public RefreshTokenRefreshFailException() {
    }

    public RefreshTokenRefreshFailException(String message) {
        this.message = message;
    }
}
