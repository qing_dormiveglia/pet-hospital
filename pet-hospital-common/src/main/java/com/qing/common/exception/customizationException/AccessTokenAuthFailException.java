package com.qing.common.exception.customizationException;

import com.qing.common.constant.PetHospitalConstant;
import org.springframework.stereotype.Component;

/**
 * @Author zzww
 * @Date 2023/3/18 15:17
 *
 * 此类为自定义认证失败异常
 *
 *
 */
@Component
public class AccessTokenAuthFailException extends RuntimeException{

    private Integer code = PetHospitalConstant.ExceptionEum.ACCESS_TOKEN.getCode();;
    private String message = PetHospitalConstant.ExceptionEum.ACCESS_TOKEN.getMsg();

    public AccessTokenAuthFailException(PetHospitalConstant.ResultEnum refreshToken) {
        this.code = refreshToken.getCode();
        this.message = refreshToken.getMsg();
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }


    public AccessTokenAuthFailException(PetHospitalConstant.ExceptionEum exceptionEum) {
        this.code = exceptionEum.getCode();
        this.message = exceptionEum.getMsg();
    }

    public AccessTokenAuthFailException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public AccessTokenAuthFailException() {
    }

    public AccessTokenAuthFailException(String message) {
        this.message = message;
    }
}
