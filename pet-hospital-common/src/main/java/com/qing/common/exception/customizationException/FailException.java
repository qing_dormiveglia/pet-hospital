package com.qing.common.exception.customizationException;

import com.qing.common.constant.PetHospitalConstant;
import org.springframework.stereotype.Component;

/**
 * @Author zzww
 * @Date 2023/3/18 15:17
 *
 * 此类为自定义运行时异常类  失败处理异常类
 *
 * 》》作用：提供自定义封装code与message信息，若未传入，将使用默认信息
 */
@Component
public class FailException extends RuntimeException{

    private Integer code = PetHospitalConstant.ExceptionEum.FAIL_EXCEPTION.getCode();;
    private String message = PetHospitalConstant.ExceptionEum.FAIL_EXCEPTION.getMsg();

    public FailException(PetHospitalConstant.ResultEnum refreshToken) {
        this.code = refreshToken.getCode();
        this.message = refreshToken.getMsg();
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }


    public FailException(PetHospitalConstant.ExceptionEum exceptionEum) {
        this.code = exceptionEum.getCode();
        this.message = exceptionEum.getMsg();
    }

    public FailException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public FailException() {
    }

    public FailException(String message) {
        this.message = message;
    }
}
