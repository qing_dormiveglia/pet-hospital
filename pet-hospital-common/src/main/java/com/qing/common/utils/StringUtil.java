package com.qing.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * @Author zzww
 * @Date 2024/2/12 16:48
 */

public class StringUtil
{
    public static List<Long> tagSplit(String tag){
        List<Long> ids = new ArrayList<Long>();
        if (tag==null||"".equals(tag)) {
            return ids;
        }

        try {
            List<String> list = Arrays.asList(tag.split(","));
            ids = list.stream().map(Long::valueOf).collect(Collectors.toList());
        }catch (Exception e){
            return ids;
        }
        return ids;
    }

    public static boolean equals(String str1,String str2){
        if (str1==null||str2==null){
            return false;
        }
        if ("".equals(str1)||"".equals(str2)){
            return false;
        }
        return str1.equals(str2);
    }

    public static String tagSplitList(ArrayList<Long> longs) {
        if (longs==null){
            return null;
        }
        if (longs.size()==0){
            return null;
        }
        if (longs.size()==1){
            return longs.get(0).toString();
        }
        StringJoiner sj = new StringJoiner(",");
        for (Long num : longs) {
            sj.add(num.toString());
        }
        String result = sj.toString();
        return result;
    }
}
