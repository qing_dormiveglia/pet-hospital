package com.qing.common.utils.jwt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Author zzww
 * @Date 2023/11/29 18:41
 */
public class MatchTokenGeneratorUtils
{
    public static String generateMatchToken(String refreshToken) throws NoSuchAlgorithmException {
        // 获取 SHA256 哈希算法实例
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        // 将 refresh_token 转换为字节数组
        byte[] refreshTokenBytes = refreshToken.getBytes();
        // 对字节数组进行哈希计算
        byte[] hashBytes = messageDigest.digest(refreshTokenBytes);
        // 将哈希值转换为十六进制字符串
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : hashBytes) {
            stringBuilder.append(String.format("%02x", b));
        }
        // 截取哈希值的前 16 个字符作为匹配字符串
        String matchToken = stringBuilder.substring(0, 16);
        return matchToken;
    }

}
