package com.qing.common.utils;

import com.qing.common.constant.TokenEnum;
import com.qing.common.exception.customizationException.AccessTokenAuthFailException;
import com.qing.common.utils.jwt.JwtUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;

/**
 * @Author zzww
 * @Date 2024/2/18 17:51
 */

public class WebUtils {
    public static ServletRequestAttributes getServletRequestAttributes() {
        return (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    }

    /**
     * 得到当前线程的请求对象
     *
     * @return
     */
    public static HttpServletRequest getHttpServletRequest() {
        return getServletRequestAttributes().getRequest();
    }

    /**
     * 从请求头中得到accessToken
     *
     * @return
     */
    public static String getAccessToken() {
        return getServletRequestAttributes().getRequest().getHeader(TokenEnum.ACCESS_TOKEN.getValue());
    }

    /**
     * 通过AccessToken获取id
     * @return
     */
    public static Long getMemberIdByAccessToken() {
        String accessToken = getServletRequestAttributes().getRequest().getHeader(TokenEnum.ACCESS_TOKEN.getValue());
        if (ObjectUtils.isEmpty(accessToken) ||!JwtUtils.validateToken(accessToken)){
            throw new AccessTokenAuthFailException();
        }
        String username = JwtUtils.extractUsername(accessToken);
        return Long.valueOf(username);
    }

    /**
     * 通过refreshToken获取id
     * @return
     */
    public static Long getMemberIdByRefreshToken() {
        String refresh = getServletRequestAttributes().getRequest().getHeader(TokenEnum.REFRESH_TOKEN.getValue());
        if (JwtUtils.validateToken(refresh)){
            throw new AccessTokenAuthFailException();
        }
        String username = JwtUtils.extractUsername(refresh);
        return Long.valueOf(username);
    }

    /**
     * 从请求头中得到refreshToken
     *
     * @return
     */
    public static String getRefreshToken() {
        return getServletRequestAttributes().getRequest().getHeader(TokenEnum.REFRESH_TOKEN.getValue());
    }

    /**
     * 得到访问的ip地址
     *
     * @return
     */
    public static String getRequestIp() {
        HttpServletRequest request = getHttpServletRequest();
        String ip = null;
        ip = request.getHeader("x-forwarded-for");
        if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
            ip = request.getRemoteAddr();
            if (ip.equals("127.0.0.1")) {
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ip = inet.getHostAddress();
            }
        }
        if ((ip != null) && (ip.length() > 15)) {
            if (ip.indexOf(",") > 0) {
                ip = ip.substring(0, ip.indexOf(","));
            }
        }
        return ip;
    }

    /**
     * 得到当前线程的响应对象
     */
    public static HttpServletResponse getHttpServletResponse() {
        return getServletRequestAttributes().getResponse();
    }

    /**
     * 得到session对象
     */
    public static HttpSession getHttpSession() {
        return getHttpServletRequest().getSession();
    }

    /**
     * 得到servletContext对象
     */
    public static ServletContext getServletContext() {
        return getHttpServletRequest().getServletContext();
    }
}

