package com.qing.common.to;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/2/19 18:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberTo implements Serializable {

    private static final long serialVersionUID = 8295348195732708112L;


    /** 用户表主键 */
    private Long id;

    /** 昵称 */
    private String nickname;

    /** 头像 */
    private String headerImage;

    /** 性别 */
    private Integer gender;

}
