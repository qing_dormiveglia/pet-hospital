package com.qing.common.to;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author zzww
 * @Date 2024/2/13 18:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalTo implements Serializable {

    private static final long serialVersionUID = 8295348195732708179L;


    /** 医院id */
    private Long id;

    /** 名称 */
    private String name;

    /** 头图 */
    private String headImage;

    /** 区 */
    private String region;

    /** 等级 */
    private String grade;

    /** 介绍 */
    private String introduce;

    /** 地址 */
    private String address;
}
