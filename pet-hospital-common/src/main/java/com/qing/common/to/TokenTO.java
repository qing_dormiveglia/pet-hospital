package com.qing.common.to;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author zzww
 * @Date 2023/11/29 16:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TokenTO {


    private Long id;
    private String username;
    private String nickname;
}
