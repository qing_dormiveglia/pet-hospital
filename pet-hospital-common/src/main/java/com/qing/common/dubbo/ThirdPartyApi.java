package com.qing.common.dubbo;

import org.springframework.web.multipart.MultipartFile;

public interface ThirdPartyApi {

    String upLoadDuBboApi(MultipartFile multipartFile, String bucketName);

}
