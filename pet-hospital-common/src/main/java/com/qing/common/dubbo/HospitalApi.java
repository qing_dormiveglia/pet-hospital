package com.qing.common.dubbo;

import com.qing.common.to.HospitalTo;

public interface HospitalApi {

    HospitalTo getHospitalDuBboApi(Long id);

}
