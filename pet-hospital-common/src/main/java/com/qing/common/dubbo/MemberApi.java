package com.qing.common.dubbo;

import com.qing.common.to.MemberTo;

import java.util.List;

public interface MemberApi {

    List<MemberTo> getMemberDuBboApi(List<Long> ids);

    MemberTo getMemberInfoDuBboApi(Long id);

}
