package com.qing.common.constant;

/**
 * @Author zzww
 * @Date 2023/4/29 22:16
 */
public enum RedisEnum {

    MEMBER_TOKEN("pet-hospital-member-token:","生成的token将以此为前缀存入Redis"),
    SMS_CODE("pet-hospital-sms-code:","短信认证存入Redis的code"),
    REFRESH_TOKEN("pet-hospital-refresh-token:","refresh-token三认证存入Redis的有效字符串"),
    ;


    private String value;
    private String message;

    public String getValue(){
        return value;
    }
    public String getMessage(){
        return message;
    }

    RedisEnum(String value, String message){
        this.value=value;
        this.message = message;
    }

}
