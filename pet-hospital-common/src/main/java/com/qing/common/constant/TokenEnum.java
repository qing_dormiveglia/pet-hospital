package com.qing.common.constant;

public enum TokenEnum {
    ACCESS_TOKEN("accesstoken","认证token"),
    REFRESH_TOKEN("refreshtoken","刷新token"),
    ;


    private String value;
    private String message;

    public String getValue(){
        return value;
    }
    public String getMessage(){
        return message;
    }

    TokenEnum(String value, String message){
        this.value=value;
        this.message = message;
    }

}
