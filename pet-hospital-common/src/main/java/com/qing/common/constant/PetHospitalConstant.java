package com.qing.common.constant;

import java.net.InterfaceAddress;
import java.util.Map;

/**
 * @Author zzww
 * @Date 2023/4/26 13:00
 *  常量类
 */
public class PetHospitalConstant {

    public enum SuccessEnum {


        SEARCH_SUCCESS("查找成功"),

        INSERT_SUCCESS("添加成功"),

        OPERATE_SUCCESS("操作成功")

        ;

        private String message;

        public String getMsg() {
            return message;
        }

        SuccessEnum(String msg) {
            this.message = msg;
        }
    }
    //jwt
    public enum JwtEnum {

        ACCESS_TOKEN("accesstoken","认证token"),
        REFRESH_TOKEN("refreshtoken","刷新token");

        private final String value;
        JwtEnum(String value, String content) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    /**
     * 100 XX : 通用异常码
     *       01 系统未知异常
     *
     * 200 XX : 数据错误码
     *       01 数据库数据缺失  或   无此条记录
     *       02 输入参数错误
     *       03 无此账号
     *       04 密码错误
     *       05 预约失败，联系管理员
     *       06 用户id获取失败，请联系管理员
     *       07 已经预约成功
     * 300 xx : 认证码
     *       01 access-token失效或不合规
     *       02 refresh-token失效或不合规
     *       03 短信验证码错误
     *       04 此号码已被注册
     * 400 xx : api异常
     *       01 服务器内部异常,发送失败
     */
    public enum ExceptionEum {
        DATA_LACK(20001,"数据库数据缺失或无此条记录"),
        ACCOUNT(20003,"无此账号"),
        PASSWORD(20004,"密码错误"),
        ACTIVITY_SUBSCRIBE(20005,"预约失败，联系管理员"),
        ACTIVITY_SUBSCRIBE_OK(20007,"已预约成功"),
        USERNAME_PSW_FAIL(20008,"账号密码错误"),
        THREAD_LOCAL(20006,"用户id获取失败，请联系管理员"),
        SMS_SEND(40001,"服务器内部异常,发送失败"),
        DATA_INPUT(20002,"入参数据错误或无入参数据"),
        REFRESH_TOKEN(30002,"refresh-token失效或不合规"),
        SMS_CODE(30003,"短信验证码错误"),
        PHONE_CODE(30004,"此号码已被注册"),
        ACCESS_TOKEN(30001,"access-token失效或不合规"),
        FAIL_EXCEPTION(10001,"系统未知异常"),
        PAGE_OR_PAGESIZE_NULL(10024,"当前页码或页面大小为空");



        private Integer code;
        private String message;

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return message;
        }

        ExceptionEum(int code, String msg) {
            this.code = code;
            this.message = msg;
        }
    }



    /**
     * Result统一返回枚举
     */
    public enum ResultEnum {
        /**
         * 接口成功返回
         */
        SUCCESS(200,"success"),


        /**
         * 接口失败返回
         */
        FAIL(500,"fail"),

        /**
         * 出现异常后统一返回
         */
        FAIL_EXCEPTION(500,"系统未知异常"),
        REFRESH_TOKEN(403,"refresh-token失效或不合规"),
        ACCESS_TOKEN(401,"access-token失效或不合规");


        private Integer code;
        private String message;

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return message;
        }

        ResultEnum(int code, String msg) {
            this.code = code;
            this.message = msg;
        }
    }

    /**
     * <p>自增涨 id 枚举类</p>
     * <pre>数据中心ID  机器ID java枚举</pre>
     * 在分布式系统中，为了避免多个节点生成相同的ID，
     * 需要对生成ID的节点进行区分。Snowflake算法中，
     * 使用数据中心ID和机器ID两个参数来区分不同的节点。
     */
    public  enum IdWorkerEnum{
        /**
         * 数据中心ID表示一个数据中心（或机房）的唯一标识，
         * 可以理解为一个集群。在同一个数据中心内，不同的机器可以使用相同的数据中心ID，但机器ID必须不同。
         */
        DATACENTER_ID(1l,"数据中心"),
        /**
         * 机器ID表示一个机器的唯一标识，可以理解为一个节点。在同一个数据中心内，
         * 不同的机器必须使用不同的机器ID。
         */
        MACHINE_ID(1l,"机器标识");
        private Long code;
        private String message;

        public Long getCode() {
            return code;
        }

        public String getMsg() {
            return message;
        }

        IdWorkerEnum(Long code, String msg) {
            this.code = code;
            this.message = msg;
        }
    }
    public enum MsgActionEnum{
        CONNECT(1, "第一次(或重连)初始化连接"),
        CHAT(2, "聊天消息"),
        SIGNED(3, "消息签收"),
        KEEPALIVE(4, "客户端保持心跳"),
        PULL_FRIEND(5, "拉取好友");

        public final Integer type;
        public final String content;

        MsgActionEnum(Integer type, String content){
            this.type = type;
            this.content = content;
        }

        public Integer getType() {
            return type;
        }
    }




}
