package com.qing.common.enums;

/**
 * 字典模块，公告类型
 * @Author zzww
 * @Date 2024/2/13 17:31
 */
public enum NoticeTypeEnum
{
    HOSPITAL_ENUM(0,"医院公告"),
    SYSTEM_ENUM(1,"管理员(系统公告)");

    private final Integer code;
    private final String message;

    NoticeTypeEnum(Integer code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }
}
