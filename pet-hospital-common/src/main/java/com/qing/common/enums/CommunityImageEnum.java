package com.qing.common.enums;

public enum CommunityImageEnum
{
    TOP_ENUM(0,"顶置广告"),
    COMMUNITY_ENUM(1,"帖子"),
    COMMENT_ENUM(2,"评论");

    private final Integer code;
    private final String message;

    CommunityImageEnum(Integer code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

}
