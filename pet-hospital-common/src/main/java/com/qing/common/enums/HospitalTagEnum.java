package com.qing.common.enums;

/**
 * 医院模块，标签类型枚举类
 */
public enum HospitalTagEnum
{
    HOSPITAL_TAG_ENUM(1,"医院"),
    DOCTOR_TAG_ENUM(0,"医生"),
    DRUG_TAG_ENUM(2,"药品");

    private final Integer code;
    private final String message;

    HospitalTagEnum(Integer code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }
}
