package com.qing.common.enums;

public enum MsgSignFlagEnum
{
    NOT_SING_FOR_ENUM(0, "未签收"),
    SING_FOR_ENUM(1, "签收");

    public final Integer type;
    public final String content;

    MsgSignFlagEnum(Integer type, String content){
        this.type = type;
        this.content = content;
    }

    public Integer getType() {
        return type;
    }

}
