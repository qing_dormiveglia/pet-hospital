package com.qing.common.enums;

public enum HospitalDrugTypeEnum
{
    DRUG_TYPE_1_ENUM(0,"一级"),
    DRUG_TYPE_2_ENUM(1,"二级"),
    DRUG_TYPE_3_ENUM(2,"三级");

    private final Integer code;
    private final String message;

    HospitalDrugTypeEnum(Integer code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

}
