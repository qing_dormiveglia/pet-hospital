package com.qing.member;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author zzww
 * @Date 2024/2/8 0:05
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@MapperScan("com.qing.**.mapper")
@EnableDubbo(scanBasePackages = "com.qing")
public class Member8201 {

    public static void main(String[] args) {
        SpringApplication.run(Member8201.class,args);
    }
}
