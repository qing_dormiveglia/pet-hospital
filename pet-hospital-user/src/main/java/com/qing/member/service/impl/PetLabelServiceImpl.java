package com.qing.member.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qing.member.domain.PetLabelEntity;
import com.qing.member.domain.UsmPetInfo;
import com.qing.member.mapper.UsmPetInfoMapper;
import com.qing.member.service.IUsmPetInfoService;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.PetLabelMapper;
import com.qing.member.domain.PetLabel;
import com.qing.member.service.IPetLabelService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class PetLabelServiceImpl extends ServiceImpl<PetLabelMapper, PetLabelEntity> implements IPetLabelService
{
    @Resource
    private PetLabelMapper petLabelMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public PetLabel selectPetLabelById(Long id)
    {
        return petLabelMapper.selectPetLabelById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param petLabel 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<PetLabel> selectPetLabelList(PetLabel petLabel)
    {
        return petLabelMapper.selectPetLabelList(petLabel);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param petLabel 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertPetLabel(PetLabel petLabel)
    {
        petLabel.setCreateTime(DateUtils.getNowDate());
        return petLabelMapper.insertPetLabel(petLabel);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param petLabel 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updatePetLabel(PetLabel petLabel)
    {
        petLabel.setUpdateTime(DateUtils.getNowDate());
        return petLabelMapper.updatePetLabel(petLabel);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePetLabelByIds(Long[] ids)
    {
        return petLabelMapper.deletePetLabelByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePetLabelById(Long id)
    {
        return petLabelMapper.deletePetLabelById(id);
    }
}
