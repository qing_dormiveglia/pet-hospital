package com.qing.member.service;

import java.util.List;
import com.qing.member.domain.PetCure;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface IPetCureService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public PetCure selectPetCureById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param petCure 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<PetCure> selectPetCureList(PetCure petCure);

    /**
     * 新增【请填写功能名称】
     * 
     * @param petCure 【请填写功能名称】
     * @return 结果
     */
    public int insertPetCure(PetCure petCure);

    /**
     * 修改【请填写功能名称】
     * 
     * @param petCure 【请填写功能名称】
     * @return 结果
     */
    public int updatePetCure(PetCure petCure);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deletePetCureByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deletePetCureById(Long id);
}
