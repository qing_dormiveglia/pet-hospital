package com.qing.member.service;

import java.util.List;
import com.qing.member.domain.MemberAddress;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface IMemberAddressService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public MemberAddress selectMemberAddressById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberAddress 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MemberAddress> selectMemberAddressList(MemberAddress memberAddress);

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberAddress 【请填写功能名称】
     * @return 结果
     */
    public int insertMemberAddress(MemberAddress memberAddress);

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberAddress 【请填写功能名称】
     * @return 结果
     */
    public int updateMemberAddress(MemberAddress memberAddress);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteMemberAddressByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMemberAddressById(Long id);
}
