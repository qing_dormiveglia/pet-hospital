package com.qing.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.PetImagesMapper;
import com.qing.member.domain.PetImages;
import com.qing.member.service.IPetImagesService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class PetImagesServiceImpl implements IPetImagesService 
{
    @Autowired
    private PetImagesMapper petImagesMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public PetImages selectPetImagesById(Long id)
    {
        return petImagesMapper.selectPetImagesById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param petImages 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<PetImages> selectPetImagesList(PetImages petImages)
    {
        return petImagesMapper.selectPetImagesList(petImages);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param petImages 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertPetImages(PetImages petImages)
    {
        petImages.setCreateTime(DateUtils.getNowDate());
        return petImagesMapper.insertPetImages(petImages);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param petImages 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updatePetImages(PetImages petImages)
    {
        petImages.setUpdateTime(DateUtils.getNowDate());
        return petImagesMapper.updatePetImages(petImages);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePetImagesByIds(Long[] ids)
    {
        return petImagesMapper.deletePetImagesByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePetImagesById(Long id)
    {
        return petImagesMapper.deletePetImagesById(id);
    }
}
