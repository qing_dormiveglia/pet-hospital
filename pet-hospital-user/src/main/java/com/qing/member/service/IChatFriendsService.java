package com.qing.member.service;

import java.util.List;
import com.qing.member.domain.ChatFriends;
import com.qing.member.vo.chat.ChatFriendsVo;
import com.qing.member.vo.member.MemberChatFriendsVo;

/**
 * 好友Service接口
 * 
 * @author zzww
 * @date 2024-02-21
 */
public interface IChatFriendsService 
{
    /**
     * 查询好友
     * 
     * @param id 好友主键
     * @return 好友
     */
    public ChatFriends selectChatFriendsById(Long id);

    /**
     * 查询好友列表
     * 
     * @param chatFriends 好友
     * @return 好友集合
     */
    public List<ChatFriends> selectChatFriendsList(ChatFriends chatFriends);

    /**
     * 新增好友
     * 
     * @param chatFriends 好友
     * @return 结果
     */
    public int insertChatFriends(ChatFriends chatFriends);

    /**
     * 修改好友
     * 
     * @param chatFriends 好友
     * @return 结果
     */
    public int updateChatFriends(ChatFriends chatFriends);

    /**
     * 批量删除好友
     * 
     * @param ids 需要删除的好友主键集合
     * @return 结果
     */
    public int deleteChatFriendsByIds(Long[] ids);

    /**
     * 删除好友信息
     * 
     * @param id 好友主键
     * @return 结果
     */
    public int deleteChatFriendsById(Long id);

    /**
     * 获取消息列表
     * @return
     */
    List<MemberChatFriendsVo> findChatFriendsList();

    /**
     * 获取未通知消息列表
     * @return
     */
    List<MemberChatFriendsVo> findNotInformChatFriendsList();

    /**
     * 获取已通知消息列表
     * @return
     */
    List<MemberChatFriendsVo> findInformChatFriendsList();
}
