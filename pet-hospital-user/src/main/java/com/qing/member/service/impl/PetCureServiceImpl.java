package com.qing.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.PetCureMapper;
import com.qing.member.domain.PetCure;
import com.qing.member.service.IPetCureService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class PetCureServiceImpl implements IPetCureService 
{
    @Autowired
    private PetCureMapper petCureMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public PetCure selectPetCureById(Long id)
    {
        return petCureMapper.selectPetCureById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param petCure 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<PetCure> selectPetCureList(PetCure petCure)
    {
        return petCureMapper.selectPetCureList(petCure);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param petCure 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertPetCure(PetCure petCure)
    {
        petCure.setCreateTime(DateUtils.getNowDate());
        return petCureMapper.insertPetCure(petCure);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param petCure 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updatePetCure(PetCure petCure)
    {
        petCure.setUpdateTime(DateUtils.getNowDate());
        return petCureMapper.updatePetCure(petCure);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePetCureByIds(Long[] ids)
    {
        return petCureMapper.deletePetCureByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePetCureById(Long id)
    {
        return petCureMapper.deletePetCureById(id);
    }
}
