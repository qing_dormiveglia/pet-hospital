package com.qing.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.ChatFriendsRequestMapper;
import com.qing.member.domain.ChatFriendsRequest;
import com.qing.member.service.IChatFriendsRequestService;

/**
 * 发送事件Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-21
 */
@Service
public class ChatFriendsRequestServiceImpl implements IChatFriendsRequestService 
{
    @Autowired
    private ChatFriendsRequestMapper chatFriendsRequestMapper;

    /**
     * 查询发送事件
     * 
     * @param id 发送事件主键
     * @return 发送事件
     */
    @Override
    public ChatFriendsRequest selectChatFriendsRequestById(Long id)
    {
        return chatFriendsRequestMapper.selectChatFriendsRequestById(id);
    }

    /**
     * 查询发送事件列表
     * 
     * @param chatFriendsRequest 发送事件
     * @return 发送事件
     */
    @Override
    public List<ChatFriendsRequest> selectChatFriendsRequestList(ChatFriendsRequest chatFriendsRequest)
    {
        return chatFriendsRequestMapper.selectChatFriendsRequestList(chatFriendsRequest);
    }

    /**
     * 新增发送事件
     * 
     * @param chatFriendsRequest 发送事件
     * @return 结果
     */
    @Override
    public int insertChatFriendsRequest(ChatFriendsRequest chatFriendsRequest)
    {
        chatFriendsRequest.setCreateTime(DateUtils.getNowDate());
        return chatFriendsRequestMapper.insertChatFriendsRequest(chatFriendsRequest);
    }

    /**
     * 修改发送事件
     * 
     * @param chatFriendsRequest 发送事件
     * @return 结果
     */
    @Override
    public int updateChatFriendsRequest(ChatFriendsRequest chatFriendsRequest)
    {
        chatFriendsRequest.setUpdateTime(DateUtils.getNowDate());
        return chatFriendsRequestMapper.updateChatFriendsRequest(chatFriendsRequest);
    }

    /**
     * 批量删除发送事件
     * 
     * @param ids 需要删除的发送事件主键
     * @return 结果
     */
    @Override
    public int deleteChatFriendsRequestByIds(Long[] ids)
    {
        return chatFriendsRequestMapper.deleteChatFriendsRequestByIds(ids);
    }

    /**
     * 删除发送事件信息
     * 
     * @param id 发送事件主键
     * @return 结果
     */
    @Override
    public int deleteChatFriendsRequestById(Long id)
    {
        return chatFriendsRequestMapper.deleteChatFriendsRequestById(id);
    }
}
