package com.qing.member.service;

import java.util.List;
import com.qing.member.domain.MemberLoginLog;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface IMemberLoginLogService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public MemberLoginLog selectMemberLoginLogById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberLoginLog 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MemberLoginLog> selectMemberLoginLogList(MemberLoginLog memberLoginLog);

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberLoginLog 【请填写功能名称】
     * @return 结果
     */
    public int insertMemberLoginLog(MemberLoginLog memberLoginLog);

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberLoginLog 【请填写功能名称】
     * @return 结果
     */
    public int updateMemberLoginLog(MemberLoginLog memberLoginLog);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteMemberLoginLogByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMemberLoginLogById(Long id);
}
