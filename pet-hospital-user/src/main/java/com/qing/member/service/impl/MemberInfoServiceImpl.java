package com.qing.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.MemberInfoMapper;
import com.qing.member.domain.MemberInfo;
import com.qing.member.service.IMemberInfoService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class MemberInfoServiceImpl implements IMemberInfoService 
{
    @Autowired
    private MemberInfoMapper memberInfoMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MemberInfo selectMemberInfoById(Long id)
    {
        return memberInfoMapper.selectMemberInfoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberInfo 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MemberInfo> selectMemberInfoList(MemberInfo memberInfo)
    {
        return memberInfoMapper.selectMemberInfoList(memberInfo);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMemberInfo(MemberInfo memberInfo)
    {
        memberInfo.setCreateTime(DateUtils.getNowDate());
        return memberInfoMapper.insertMemberInfo(memberInfo);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMemberInfo(MemberInfo memberInfo)
    {
        memberInfo.setUpdateTime(DateUtils.getNowDate());
        return memberInfoMapper.updateMemberInfo(memberInfo);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberInfoByIds(Long[] ids)
    {
        return memberInfoMapper.deleteMemberInfoByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberInfoById(Long id)
    {
        return memberInfoMapper.deleteMemberInfoById(id);
    }
}
