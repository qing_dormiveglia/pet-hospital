package com.qing.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.MemberAddressMapper;
import com.qing.member.domain.MemberAddress;
import com.qing.member.service.IMemberAddressService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class MemberAddressServiceImpl implements IMemberAddressService 
{
    @Resource
    private MemberAddressMapper memberAddressMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MemberAddress selectMemberAddressById(Long id)
    {
        return memberAddressMapper.selectMemberAddressById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberAddress 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MemberAddress> selectMemberAddressList(MemberAddress memberAddress)
    {
        return memberAddressMapper.selectMemberAddressList(memberAddress);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberAddress 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMemberAddress(MemberAddress memberAddress)
    {
        memberAddress.setCreateTime(DateUtils.getNowDate());
        return memberAddressMapper.insertMemberAddress(memberAddress);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberAddress 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMemberAddress(MemberAddress memberAddress)
    {
        memberAddress.setUpdateTime(DateUtils.getNowDate());
        return memberAddressMapper.updateMemberAddress(memberAddress);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberAddressByIds(Long[] ids)
    {
        return memberAddressMapper.deleteMemberAddressByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberAddressById(Long id)
    {
        return memberAddressMapper.deleteMemberAddressById(id);
    }
}
