package com.qing.member.service;

import java.util.List;
import com.qing.member.domain.ChatFriendsRequest;

/**
 * 发送事件Service接口
 * 
 * @author zzww
 * @date 2024-02-21
 */
public interface IChatFriendsRequestService 
{
    /**
     * 查询发送事件
     * 
     * @param id 发送事件主键
     * @return 发送事件
     */
    public ChatFriendsRequest selectChatFriendsRequestById(Long id);

    /**
     * 查询发送事件列表
     * 
     * @param chatFriendsRequest 发送事件
     * @return 发送事件集合
     */
    public List<ChatFriendsRequest> selectChatFriendsRequestList(ChatFriendsRequest chatFriendsRequest);

    /**
     * 新增发送事件
     * 
     * @param chatFriendsRequest 发送事件
     * @return 结果
     */
    public int insertChatFriendsRequest(ChatFriendsRequest chatFriendsRequest);

    /**
     * 修改发送事件
     * 
     * @param chatFriendsRequest 发送事件
     * @return 结果
     */
    public int updateChatFriendsRequest(ChatFriendsRequest chatFriendsRequest);

    /**
     * 批量删除发送事件
     * 
     * @param ids 需要删除的发送事件主键集合
     * @return 结果
     */
    public int deleteChatFriendsRequestByIds(Long[] ids);

    /**
     * 删除发送事件信息
     * 
     * @param id 发送事件主键
     * @return 结果
     */
    public int deleteChatFriendsRequestById(Long id);
}
