package com.qing.member.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qing.member.domain.UsmPetInfo;
import com.qing.member.domain.UsmPetInfoEntity;
import com.qing.member.dto.pet.PetInfoSaveDto;
import com.qing.member.vo.pet.PetInfoVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface IUsmPetInfoService extends IService<UsmPetInfoEntity>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public UsmPetInfo selectUsmPetInfoById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<UsmPetInfo> selectUsmPetInfoList(UsmPetInfo usmPetInfo);

    /**
     * 新增【请填写功能名称】
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 结果
     */
    public int insertUsmPetInfo(UsmPetInfo usmPetInfo);

    /**
     * 修改【请填写功能名称】
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 结果
     */
    public int updateUsmPetInfo(UsmPetInfo usmPetInfo);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteUsmPetInfoByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteUsmPetInfoById(Long id);

    /**
     * 获取用户宠物列表
     * @return
     */
    List<UsmPetInfoEntity> selectUsmPetInfoListMy();

    /**
     * 获取详情
     * @param id
     * @return
     */
    PetInfoVo selectUsmPetInfoVoById(Long id);

    /**
     * 添加宠物
     * @param petInfoSaveDto
     * @return
     */
    int insertPetInfoSave(PetInfoSaveDto petInfoSaveDto);

    /**
     * 修改
     * @param petInfoSaveDto
     * @return
     */
    int updatePetInfoSave(PetInfoSaveDto petInfoSaveDto);

}
