package com.qing.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.PetKindMapper;
import com.qing.member.domain.PetKind;
import com.qing.member.service.IPetKindService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class PetKindServiceImpl implements IPetKindService 
{
    @Autowired
    private PetKindMapper petKindMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public PetKind selectPetKindById(Long id)
    {
        return petKindMapper.selectPetKindById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param petKind 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<PetKind> selectPetKindList(PetKind petKind)
    {
        return petKindMapper.selectPetKindList(petKind);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param petKind 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertPetKind(PetKind petKind)
    {
        petKind.setCreateTime(DateUtils.getNowDate());
        return petKindMapper.insertPetKind(petKind);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param petKind 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updatePetKind(PetKind petKind)
    {
        petKind.setUpdateTime(DateUtils.getNowDate());
        return petKindMapper.updatePetKind(petKind);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePetKindByIds(Long[] ids)
    {
        return petKindMapper.deletePetKindByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePetKindById(Long id)
    {
        return petKindMapper.deletePetKindById(id);
    }
}
