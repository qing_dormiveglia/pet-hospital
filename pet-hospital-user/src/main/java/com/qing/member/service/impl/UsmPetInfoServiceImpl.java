package com.qing.member.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qing.common.utils.StringUtil;
import com.qing.common.utils.WebUtils;
import com.qing.member.domain.*;
import com.qing.member.dto.pet.PetInfoSaveDto;
import com.qing.member.vo.pet.PetInfoVo;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.UsmPetInfoMapper;
import com.qing.member.service.IUsmPetInfoService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class UsmPetInfoServiceImpl extends ServiceImpl<UsmPetInfoMapper, UsmPetInfoEntity> implements IUsmPetInfoService
{
    @Resource
    private UsmPetInfoMapper usmPetInfoMapper;
    @Resource
    private PetKindServiceImpl petKindService;
    @Resource
    private PetCureServiceImpl petCureService;
    @Resource
    private PetLabelServiceImpl petLabelService;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public UsmPetInfo selectUsmPetInfoById(Long id)
    {
        return usmPetInfoMapper.selectUsmPetInfoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<UsmPetInfo> selectUsmPetInfoList(UsmPetInfo usmPetInfo)
    {
        return usmPetInfoMapper.selectUsmPetInfoList(usmPetInfo);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertUsmPetInfo(UsmPetInfo usmPetInfo)
    {
        usmPetInfo.setCreateTime(DateUtils.getNowDate());
        return usmPetInfoMapper.insertUsmPetInfo(usmPetInfo);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateUsmPetInfo(UsmPetInfo usmPetInfo)
    {
        usmPetInfo.setUpdateTime(DateUtils.getNowDate());
        return usmPetInfoMapper.updateUsmPetInfo(usmPetInfo);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteUsmPetInfoByIds(Long[] ids)
    {
        return usmPetInfoMapper.deleteUsmPetInfoByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteUsmPetInfoById(Long id)
    {
        return usmPetInfoMapper.deleteUsmPetInfoById(id);
    }

    /**
     * 获取用户宠物列表
     * @return
     */
    @Override
    public List<UsmPetInfoEntity> selectUsmPetInfoListMy() {
        Long memberId = WebUtils.getMemberIdByAccessToken();
        LambdaQueryWrapper<UsmPetInfoEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UsmPetInfoEntity::getMemberId,memberId).orderByDesc(UsmPetInfoEntity::getCreateTime);
        List<UsmPetInfoEntity> list = list(queryWrapper);
        return list;
    }

    /**
     * 获取详情信息
     * @param id
     * @return
     */
    @Override
    public PetInfoVo selectUsmPetInfoVoById(Long id) {
        //获取基本信息
        UsmPetInfo byId = selectUsmPetInfoById(id);
        PetInfoVo petInfoVo = new PetInfoVo();
        BeanUtils.copyProperties(byId,petInfoVo);
        //获取种类信息
        PetKind petKind = petKindService.selectPetKindById(byId.getPetKindId());
        petInfoVo.setPetKind(petKind);
        //获取治疗类型信息
        PetCure petCure = petCureService.selectPetCureById(byId.getPetCureId());
        petInfoVo.setPetCure(petCure);
        //获取标签信息
        String petLabelIds = byId.getPetLabelIds();
        List<Long> longs = StringUtil.tagSplit(petLabelIds);
        List<PetLabelEntity> petLabels = petLabelService.listByIds(longs);
        petInfoVo.setLabels(petLabels);
        return petInfoVo;
    }

    /**
     * 添加宠物
     * @param petInfoSaveDto
     * @return
     */
    @Override
    public int insertPetInfoSave(PetInfoSaveDto petInfoSaveDto) {
        Long id = WebUtils.getMemberIdByAccessToken();
        UsmPetInfo usmPetInfo = new UsmPetInfo();
        BeanUtils.copyProperties(petInfoSaveDto,usmPetInfo);
        List<Long> labels = petInfoSaveDto.getLabels();
        String s = StringUtil.tagSplitList((ArrayList<Long>) labels);
        usmPetInfo.setPetLabelIds(s);
        usmPetInfo.setMemberId(id);
        int i = insertUsmPetInfo(usmPetInfo);
        if (i==0){
            throw new RuntimeException("错误");
        }
        return i;
    }

    /**
     * 修改
     * @param petInfoSaveDto
     * @return
     */
    @Override
    public int updatePetInfoSave(PetInfoSaveDto petInfoSaveDto) {
        Long id = WebUtils.getMemberIdByAccessToken();
        UsmPetInfo usmPetInfo = new UsmPetInfo();
        BeanUtils.copyProperties(petInfoSaveDto,usmPetInfo);
        List<Long> labels = petInfoSaveDto.getLabels();
        String s = StringUtil.tagSplitList((ArrayList<Long>) labels);
        usmPetInfo.setPetLabelIds(s);
        usmPetInfo.setMemberId(id);
        int i = updateUsmPetInfo(usmPetInfo);
        if (i==0){
            throw new RuntimeException("错误");
        }
        return i;
    }
}
