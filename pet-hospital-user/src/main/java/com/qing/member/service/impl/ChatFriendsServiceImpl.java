package com.qing.member.service.impl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.qing.common.enums.MsgSignFlagEnum;
import com.qing.common.utils.WebUtils;
import com.qing.member.domain.ChatMsg;
import com.qing.member.domain.Member;
import com.qing.member.vo.chat.ChatFriendsVo;
import com.qing.member.vo.chat.ChatMsgVo;
import com.qing.member.vo.member.MemberChatFriendsVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.ChatFriendsMapper;
import com.qing.member.domain.ChatFriends;
import com.qing.member.service.IChatFriendsService;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * 好友Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-21
 */
@Service
public class ChatFriendsServiceImpl implements IChatFriendsService 
{
    @Resource
    private ChatFriendsMapper chatFriendsMapper;
    @Resource
    private MemberServiceImpl memberService;
    @Resource
    private ChatMsgServiceImpl chatMsgService;

    /**
     * 查询好友
     * 
     * @param id 好友主键
     * @return 好友
     */
    @Override
    public ChatFriends selectChatFriendsById(Long id)
    {
        return chatFriendsMapper.selectChatFriendsById(id);
    }

    /**
     * 查询好友列表
     * 
     * @param chatFriends 好友
     * @return 好友
     */
    @Override
    public List<ChatFriends> selectChatFriendsList(ChatFriends chatFriends)
    {
        return chatFriendsMapper.selectChatFriendsList(chatFriends);
    }

    /**
     * 新增好友
     * 
     * @param chatFriends 好友
     * @return 结果
     */
    @Override
    public int insertChatFriends(ChatFriends chatFriends)
    {
        chatFriends.setCreateTime(DateUtils.getNowDate());
        return chatFriendsMapper.insertChatFriends(chatFriends);
    }

    /**
     * 修改好友
     * 
     * @param chatFriends 好友
     * @return 结果
     */
    @Override
    public int updateChatFriends(ChatFriends chatFriends)
    {
        chatFriends.setUpdateTime(DateUtils.getNowDate());
        return chatFriendsMapper.updateChatFriends(chatFriends);
    }

    /**
     * 批量删除好友
     * 
     * @param ids 需要删除的好友主键
     * @return 结果
     */
    @Override
    public int deleteChatFriendsByIds(Long[] ids)
    {
        return chatFriendsMapper.deleteChatFriendsByIds(ids);
    }

    /**
     * 删除好友信息
     * 
     * @param id 好友主键
     * @return 结果
     */
    @Override
    public int deleteChatFriendsById(Long id)
    {
        return chatFriendsMapper.deleteChatFriendsById(id);
    }

    /**
     * 获取消息(好友，最新消息)列表（需要登录）
     * @return
     */
    @Override
    public List<MemberChatFriendsVo> findChatFriendsList() {
        Long memberId = WebUtils.getMemberIdByAccessToken();
        ChatFriends chatFriends = new ChatFriends();
        chatFriends.setMemberId(memberId);
        //获取好友消息
        List<ChatFriends> list = chatFriendsMapper.selectChatFriendsList(chatFriends);
        List<MemberChatFriendsVo> collect = list.stream().map(item -> {
            MemberChatFriendsVo memberChatFriendsVo = new MemberChatFriendsVo();
            Long friendMemberId = item.getFriendMemberId();
            memberChatFriendsVo.setChatFriendsId(item.getId());
            //获取用户信息
            Member member = memberService.selectMemberById(item.getFriendMemberId());
            BeanUtils.copyProperties(member, memberChatFriendsVo);
            //获取最新消息信息
            ChatMsg chatMsg = chatMsgService.selectChatMsgByFMIDAndTimeDesc(item.getFriendMemberId(), memberId);
            ChatMsgVo chatMsgVo = new ChatMsgVo();
            if (!ObjectUtils.isEmpty(chatMsg)){
                BeanUtils.copyProperties(chatMsg, chatMsgVo);
            }
            memberChatFriendsVo.setChatMsgVo(chatMsgVo);
            //封装未签收消息数量
            Integer num = chatMsgService.selectChatMsgByIdAndFIdToNum(item.getFriendMemberId(), memberId);
            memberChatFriendsVo.setNum(num);
            return memberChatFriendsVo;
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 获取未通知消息列表
     * @return
     */
    @Override
    public List<MemberChatFriendsVo> findNotInformChatFriendsList() {
        Long memberId = WebUtils.getMemberIdByAccessToken();
        //获取未通知列表好友消息
        List<ChatFriends> friends = chatFriendsMapper
                .selectInformChatFriendsList(memberId,MsgSignFlagEnum.NOT_SING_FOR_ENUM.type);
        List<MemberChatFriendsVo> collect = friends.stream().map(item -> {
            MemberChatFriendsVo memberChatFriendsVo = new MemberChatFriendsVo();
            Long friendMemberId = item.getFriendMemberId();
            memberChatFriendsVo.setChatFriendsId(item.getId());
            //获取好友信息
            Member member = memberService.selectMemberById(friendMemberId);
            BeanUtils.copyProperties(member, memberChatFriendsVo);
            //获取最新消息
            ChatMsg chatMsg = chatMsgService.selectChatMsgByFMIDAndTimeDesc(friendMemberId, memberId);
            ChatMsgVo chatMsgVo = new ChatMsgVo();
            if (!ObjectUtils.isEmpty(chatMsg)){
                BeanUtils.copyProperties(chatMsg, chatMsgVo);
            }
            memberChatFriendsVo.setChatMsgVo(chatMsgVo);
            //获取通知未阅读数量
            Integer num = chatMsgService.selectChatMsgByIdAndFIdToNum(friendMemberId,memberId);
            memberChatFriendsVo.setNum(num);
            return memberChatFriendsVo;
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 获取已通知消息列表
     * @return
     */
    @Override
    public List<MemberChatFriendsVo> findInformChatFriendsList() {
        Long memberId = WebUtils.getMemberIdByAccessToken();
        //获取已通知列表好友消息
        List<ChatFriends> friends = chatFriendsMapper
                .selectInformChatFriendsList(memberId,MsgSignFlagEnum.SING_FOR_ENUM.type);
        List<MemberChatFriendsVo> collect = friends.stream().map(item -> {
            MemberChatFriendsVo memberChatFriendsVo = new MemberChatFriendsVo();
            Long friendMemberId = item.getFriendMemberId();
            memberChatFriendsVo.setChatFriendsId(item.getId());
            //获取好友信息
            Member member = memberService.selectMemberById(friendMemberId);
            BeanUtils.copyProperties(member, memberChatFriendsVo);
            //获取最新消息
            ChatMsg chatMsg = chatMsgService.selectChatMsgByFMIDAndTimeDesc(friendMemberId, memberId);
            ChatMsgVo chatMsgVo = new ChatMsgVo();
            if (!ObjectUtils.isEmpty(chatMsg)){
                BeanUtils.copyProperties(chatMsg, chatMsgVo);
            }
            memberChatFriendsVo.setChatMsgVo(chatMsgVo);
            return memberChatFriendsVo;
        }).collect(Collectors.toList());
        return collect;
    }
}
