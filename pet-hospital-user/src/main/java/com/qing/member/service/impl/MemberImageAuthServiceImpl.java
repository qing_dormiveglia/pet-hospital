package com.qing.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.MemberImageAuthMapper;
import com.qing.member.domain.MemberImageAuth;
import com.qing.member.service.IMemberImageAuthService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class MemberImageAuthServiceImpl implements IMemberImageAuthService 
{
    @Autowired
    private MemberImageAuthMapper memberImageAuthMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MemberImageAuth selectMemberImageAuthById(Long id)
    {
        return memberImageAuthMapper.selectMemberImageAuthById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberImageAuth 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MemberImageAuth> selectMemberImageAuthList(MemberImageAuth memberImageAuth)
    {
        return memberImageAuthMapper.selectMemberImageAuthList(memberImageAuth);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberImageAuth 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMemberImageAuth(MemberImageAuth memberImageAuth)
    {
        memberImageAuth.setCreateTime(DateUtils.getNowDate());
        return memberImageAuthMapper.insertMemberImageAuth(memberImageAuth);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberImageAuth 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMemberImageAuth(MemberImageAuth memberImageAuth)
    {
        memberImageAuth.setUpdateTime(DateUtils.getNowDate());
        return memberImageAuthMapper.updateMemberImageAuth(memberImageAuth);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberImageAuthByIds(Long[] ids)
    {
        return memberImageAuthMapper.deleteMemberImageAuthByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberImageAuthById(Long id)
    {
        return memberImageAuthMapper.deleteMemberImageAuthById(id);
    }
}
