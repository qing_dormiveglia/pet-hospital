package com.qing.member.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.qing.common.enums.MsgSignFlagEnum;
import com.qing.common.utils.WebUtils;
import com.qing.member.vo.chat.ChatMsgVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.ChatMsgMapper;
import com.qing.member.domain.ChatMsg;
import com.qing.member.service.IChatMsgService;

import javax.annotation.Resource;

/**
 * 消息Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-21
 */
@Service
public class ChatMsgServiceImpl implements IChatMsgService 
{
    @Resource
    private ChatMsgMapper chatMsgMapper;

    /**
     * 查询消息
     * 
     * @param id 消息主键
     * @return 消息
     */
    @Override
    public ChatMsg selectChatMsgById(Long id)
    {
        return chatMsgMapper.selectChatMsgById(id);
    }

    /**
     * 查询消息列表
     * 
     * @param chatMsg 消息
     * @return 消息
     */
    @Override
    public List<ChatMsg> selectChatMsgList(ChatMsg chatMsg)
    {
        return chatMsgMapper.selectChatMsgList(chatMsg);
    }

    /**
     * 新增消息
     * 
     * @param chatMsg 消息
     * @return 结果
     */
    @Override
    public int insertChatMsg(ChatMsg chatMsg)
    {
        chatMsg.setCreateTime(DateUtils.getNowDate());
        return chatMsgMapper.insertChatMsg(chatMsg);
    }

    /**
     * 修改消息
     * 
     * @param chatMsg 消息
     * @return 结果
     */
    @Override
    public int updateChatMsg(ChatMsg chatMsg)
    {
        return chatMsgMapper.updateChatMsg(chatMsg);
    }

    /**
     * 批量删除消息
     * 
     * @param ids 需要删除的消息主键
     * @return 结果
     */
    @Override
    public int deleteChatMsgByIds(Long[] ids)
    {
        return chatMsgMapper.deleteChatMsgByIds(ids);
    }

    /**
     * 删除消息信息
     * 
     * @param id 消息主键
     * @return 结果
     */
    @Override
    public int deleteChatMsgById(Long id)
    {
        return chatMsgMapper.deleteChatMsgById(id);
    }

    /**
     * 批量获取消息（为签收消息）根据时间进行降序排序
     * @param ids 用户id集合
     * @return
     */
    @Override
    public List<ChatMsg> selectChatMsgBatchByMemberId(List<Long> ids) {
        List<ChatMsg> list= chatMsgMapper.selectChatMsgBatchByMemberId(ids);
        return list;
    }

    @Override
    public int updateChatMsgBatchByIds(List<Long> longs) {

        return chatMsgMapper.updateChatMsgBatchByIds(longs);
    }

    /**
     * 获取最新消息
     * @param friendsId
     * @param memberId
     * @return
     */
    @Override
    public ChatMsg selectChatMsgByFMIDAndTimeDesc(Long friendsId ,Long memberId) {
        return chatMsgMapper.selectChatMsgByFMIDAndTimeDesc(friendsId,memberId);
    }

    /**
     * 获取未签收的消息数量
     * @param friendMemberId
     * @param memberId
     * @return
     */
    @Override
    public Integer selectChatMsgByIdAndFIdToNum(Long friendMemberId, Long memberId) {
        return chatMsgMapper.selectChatMsgByIdAndFIdToNum(friendMemberId,memberId, MsgSignFlagEnum.NOT_SING_FOR_ENUM.type);
    }

    @Override
    public List<ChatMsgVo> selectChatMsgListByToIdAndUId(Long toUserId) {
        Long id = WebUtils.getMemberIdByAccessToken();
        List<ChatMsg> list = chatMsgMapper.selectChatMsgListByTimeDesc(toUserId,id);
        List<ChatMsgVo> collect = list.stream().map(item -> {
            ChatMsgVo chatMsgVo = new ChatMsgVo();
            BeanUtils.copyProperties(item, chatMsgVo);
            return chatMsgVo;
        }).collect(Collectors.toList());
        return collect;
    }
}
