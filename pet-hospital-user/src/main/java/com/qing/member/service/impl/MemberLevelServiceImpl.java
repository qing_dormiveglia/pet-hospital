package com.qing.member.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.qing.member.vo.member.MemberLevelVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.MemberLevelMapper;
import com.qing.member.domain.MemberLevel;
import com.qing.member.service.IMemberLevelService;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class MemberLevelServiceImpl implements IMemberLevelService 
{
    @Resource
    private MemberLevelMapper memberLevelMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MemberLevel selectMemberLevelById(Long id)
    {
        if (ObjectUtils.isEmpty(id)){
            return new MemberLevel();
        }
        return memberLevelMapper.selectMemberLevelById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberLevel 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MemberLevel> selectMemberLevelList(MemberLevel memberLevel)
    {
        return memberLevelMapper.selectMemberLevelList(memberLevel);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberLevel 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMemberLevel(MemberLevel memberLevel)
    {
        memberLevel.setCreateTime(DateUtils.getNowDate());
        return memberLevelMapper.insertMemberLevel(memberLevel);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberLevel 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMemberLevel(MemberLevel memberLevel)
    {
        memberLevel.setUpdateTime(DateUtils.getNowDate());
        return memberLevelMapper.updateMemberLevel(memberLevel);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberLevelByIds(Long[] ids)
    {
        return memberLevelMapper.deleteMemberLevelByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberLevelById(Long id)
    {
        return memberLevelMapper.deleteMemberLevelById(id);
    }

    /**
     * 获取会员列表
     * @return
     */
    @Override
    public List<MemberLevelVo> findMemberLevelList() {
        List<MemberLevel> memberLevels = memberLevelMapper.selectMemberLevelList(new MemberLevel());
        List<MemberLevelVo> list = memberLevels.stream().map(item -> {
            MemberLevelVo memberLevelVo = new MemberLevelVo();
            BeanUtils.copyProperties(item, memberLevelVo);
            return memberLevelVo;
        }).collect(Collectors.toList());
        return list;
    }
}
