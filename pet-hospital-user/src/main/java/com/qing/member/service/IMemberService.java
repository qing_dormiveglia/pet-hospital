package com.qing.member.service;

import java.util.List;
import com.qing.member.domain.Member;
import com.qing.member.dto.member.LoginDto;
import com.qing.member.dto.member.LoginSmsDto;
import com.qing.member.dto.member.RegisterDto;
import com.qing.member.dto.member.ResetDto;
import com.qing.member.vo.member.LoginVo;
import com.qing.member.vo.member.UserInfoVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface IMemberService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Member selectMemberById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param member 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Member> selectMemberList(Member member);

    /**
     * 新增【请填写功能名称】
     * 
     * @param member 【请填写功能名称】
     * @return 结果
     */
    public int insertMember(Member member);

    /**
     * 修改【请填写功能名称】
     * 
     * @param member 【请填写功能名称】
     * @return 结果
     */
    public int updateMember(Member member);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteMemberByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMemberById(Long id);

    /**
     * 账号密码登录
     * @param loginDto
     * @return
     */
    LoginVo memberLogin(LoginDto loginDto);

    /**
     * 手机号验证码登录
     * @param loginSmsDto
     * @return
     */
    LoginVo memberLoginSms(LoginSmsDto loginSmsDto);

    /**
     * 注册
     * @param registerDto
     * @return
     */
    LoginVo memberRegister(RegisterDto registerDto);

    /**
     * 重置密码，找回密码
     * @param resetDto
     * @return
     */
    LoginVo memberResetPassword(ResetDto resetDto);

    /**
     * 获取用户详情信息
     * @return
     */
    UserInfoVo getUserInfo();

    /**
     * 刷新token
     * @return
     */
    LoginVo refreshToken();

    /**
     * token有效校验
     * @return
     */
    boolean verifyToken();

    /**
     * 批量获取用户信息
     * @param ids
     * @return
     */
    List<Member> selectMemberBatchByIds(List<Long> ids);

    /**
     * 获取好友消息
     * @param toId
     * @return
     */
    UserInfoVo getToUserInfo(Long toId);
}
