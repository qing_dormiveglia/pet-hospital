package com.qing.member.service;

import java.util.List;
import com.qing.member.domain.ChatMsg;
import com.qing.member.vo.chat.ChatMsgVo;

/**
 * 消息Service接口
 * 
 * @author zzww
 * @date 2024-02-21
 */
public interface IChatMsgService 
{
    /**
     * 查询消息
     * 
     * @param id 消息主键
     * @return 消息
     */
    public ChatMsg selectChatMsgById(Long id);

    /**
     * 查询消息列表
     * 
     * @param chatMsg 消息
     * @return 消息集合
     */
    public List<ChatMsg> selectChatMsgList(ChatMsg chatMsg);

    /**
     * 新增消息
     * 
     * @param chatMsg 消息
     * @return 结果
     */
    public int insertChatMsg(ChatMsg chatMsg);

    /**
     * 修改消息
     * 
     * @param chatMsg 消息
     * @return 结果
     */
    public int updateChatMsg(ChatMsg chatMsg);

    /**
     * 批量删除消息
     * 
     * @param ids 需要删除的消息主键集合
     * @return 结果
     */
    public int deleteChatMsgByIds(Long[] ids);

    /**
     * 删除消息信息
     * 
     * @param id 消息主键
     * @return 结果
     */
    public int deleteChatMsgById(Long id);

    /**
     * 批量获取消息（为签收消息）根据时间进行降序排序
     * @param ids 用户id集合
     * @return
     */
    List<ChatMsg> selectChatMsgBatchByMemberId(List<Long> ids);

    /**
     * 批量修改签收状态
     * @param longs
     * @return
     */
    int updateChatMsgBatchByIds(List<Long> longs);

    /**
     * 获取最新消息
     * @param friendsId
     * @param memberId
     * @return
     */
    ChatMsg selectChatMsgByFMIDAndTimeDesc(Long friendsId ,Long memberId);

    /**
     * 获取未阅读的消息数量
     * @param friendMemberId
     * @param memberId
     * @return
     */
    Integer selectChatMsgByIdAndFIdToNum(Long friendMemberId, Long memberId);

    /**
     * 查询消息列表
     * @param toUserId
     * @return
     */
    List<ChatMsgVo> selectChatMsgListByToIdAndUId(Long toUserId);
}
