package com.qing.member.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.MemberLoginLogMapper;
import com.qing.member.domain.MemberLoginLog;
import com.qing.member.service.IMemberLoginLogService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class MemberLoginLogServiceImpl implements IMemberLoginLogService 
{
    @Autowired
    private MemberLoginLogMapper memberLoginLogMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MemberLoginLog selectMemberLoginLogById(Long id)
    {
        return memberLoginLogMapper.selectMemberLoginLogById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberLoginLog 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MemberLoginLog> selectMemberLoginLogList(MemberLoginLog memberLoginLog)
    {
        return memberLoginLogMapper.selectMemberLoginLogList(memberLoginLog);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberLoginLog 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMemberLoginLog(MemberLoginLog memberLoginLog)
    {
        memberLoginLog.setCreateTime(DateUtils.getNowDate());
        return memberLoginLogMapper.insertMemberLoginLog(memberLoginLog);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberLoginLog 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMemberLoginLog(MemberLoginLog memberLoginLog)
    {
        memberLoginLog.setUpdateTime(DateUtils.getNowDate());
        return memberLoginLogMapper.updateMemberLoginLog(memberLoginLog);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberLoginLogByIds(Long[] ids)
    {
        return memberLoginLogMapper.deleteMemberLoginLogByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberLoginLogById(Long id)
    {
        return memberLoginLogMapper.deleteMemberLoginLogById(id);
    }
}
