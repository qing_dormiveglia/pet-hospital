package com.qing.member.service.impl;

import java.util.List;

import com.qing.common.constant.PetHospitalConstant;
import com.qing.common.constant.RedisEnum;
import com.qing.common.constant.TokenEnum;
import com.qing.common.dubbo.HospitalApi;
import com.qing.common.dubbo.MemberApi;
import com.qing.common.exception.customizationException.AccessTokenAuthFailException;
import com.qing.common.exception.customizationException.FailException;
import com.qing.common.exception.customizationException.RefreshTokenRefreshFailException;
import com.qing.common.to.MemberTo;
import com.qing.common.to.TokenTO;
import com.qing.common.utils.StringUtil;
import com.qing.common.utils.WebUtils;
import com.qing.common.utils.jwt.JwtUtils;
import com.qing.member.domain.MemberLevel;
import com.qing.member.dto.member.LoginDto;
import com.qing.member.dto.member.LoginSmsDto;
import com.qing.member.dto.member.RegisterDto;
import com.qing.member.dto.member.ResetDto;
import com.qing.member.vo.member.LoginVo;
import com.qing.member.vo.member.MemberLevelVo;
import com.qing.member.vo.member.UserInfoVo;
import com.ruoyi.common.utils.DateUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.qing.member.mapper.MemberMapper;
import com.qing.member.domain.Member;
import com.qing.member.service.IMemberService;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@DubboService(interfaceClass = MemberApi.class)
@Service
public class MemberServiceImpl implements IMemberService , MemberApi
{
    @Resource
    private MemberMapper memberMapper;
    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private MemberLevelServiceImpl memberLevelService;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Member selectMemberById(Long id)
    {
        return memberMapper.selectMemberById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param member 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Member> selectMemberList(Member member)
    {
        return memberMapper.selectMemberList(member);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param member 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMember(Member member)
    {
        member.setCreateTime(DateUtils.getNowDate());
        return memberMapper.insertMember(member);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param member 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMember(Member member)
    {
        member.setUpdateTime(DateUtils.getNowDate());
        return memberMapper.updateMember(member);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberByIds(Long[] ids)
    {
        return memberMapper.deleteMemberByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMemberById(Long id)
    {
        return memberMapper.deleteMemberById(id);
    }

    /**
     * 账号密码登录
     * @param loginDto
     * @return
     */
    @Override
    public LoginVo memberLogin(LoginDto loginDto) {
        Member member = memberMapper.selectMemberByUsername(loginDto.getUsername());
        if (ObjectUtils.isEmpty(member)) throw new FailException(PetHospitalConstant.ExceptionEum.ACCOUNT);
        boolean matches = bCryptPasswordEncoder
                .matches(loginDto.getPassword(), member.getPassword());
        if (!matches) throw new FailException(PetHospitalConstant.ExceptionEum.USERNAME_PSW_FAIL);
        return generateToken(member);
    }

    /**
     * 手机号验证码登录
        }
        Member member = memberMapper.selectMemberByUsername(loginSmsDto.getPhone());
        return generateToken(member);
    }
     */
     public LoginVo memberLoginSms(LoginSmsDto loginSmsDto) {
     String code = (String)this.stringRedisTemplate.opsForValue().get(RedisEnum.SMS_CODE.getValue() + loginSmsDto.getPhone());
     boolean b = StringUtil.equals(loginSmsDto.getCode(), code);
     if (!b) {
     throw new FailException(PetHospitalConstant.ExceptionEum.SMS_CODE);
     } else {
     Member member = this.memberMapper.selectMemberByUsername(loginSmsDto.getPhone());
     return this.generateToken(member);
     }
     }

    /**
     * 注册
     * @param registerDto
     * @return
     */
    @Override
    public LoginVo memberRegister(RegisterDto registerDto) {
        String code = stringRedisTemplate.opsForValue().get(RedisEnum.SMS_CODE.getValue() + registerDto.getUsername());
        if (!StringUtil.equals(code,registerDto.getCode())) {
            throw new FailException(PetHospitalConstant.ExceptionEum.SMS_CODE);
        }
        String encode = bCryptPasswordEncoder.encode(registerDto.getPassword());
        registerDto.setPassword(encode);
        Member member = new Member();
        BeanUtils.copyProperties(registerDto,member);
        memberMapper.insertMember(member);
        LoginVo loginVo = generateToken(member);
        return loginVo;
    }

    /**
     * 重置密码，找回密码
     * @param resetDto
     * @return
     */
    @Override
    public LoginVo memberResetPassword(ResetDto resetDto) {
        String code = stringRedisTemplate.opsForValue().get(RedisEnum.SMS_CODE + resetDto.getUsername());
        boolean equals = StringUtil.equals(code, resetDto.getCode());
        if (!equals) {
            throw new FailException(PetHospitalConstant.ExceptionEum.SMS_CODE);
        }
        Member member = memberMapper.selectMemberByUsername(resetDto.getUsername());
        String encode = bCryptPasswordEncoder.encode(resetDto.getPassword());
        member.setPassword(encode);
        memberMapper.updateMember(member);
        return generateToken(member);
    }

    public Long getMemberIdByAccessToken(){
        HttpServletRequest request = WebUtils.getHttpServletRequest();
        String accessToken = request.getHeader(TokenEnum.ACCESS_TOKEN.getValue());
        if (accessToken.isEmpty()||!JwtUtils.validateToken(accessToken)){
            throw new AccessTokenAuthFailException();
        }
        String usernameId = JwtUtils.extractUsername(accessToken);
        return Long.valueOf(usernameId);
    }

    /**
     * 获取用户详情信息
     * @return
     */
    @Override
    public UserInfoVo getUserInfo() {
        Long memberId = getMemberIdByAccessToken();
        UserInfoVo userInfoVo = new UserInfoVo();
        Member member = memberMapper.selectMemberById(memberId);
        BeanUtils.copyProperties(member,userInfoVo);
        Long memberLevelId = member.getMemberLevelId();
        MemberLevel memberLevel = memberLevelService.selectMemberLevelById(memberLevelId);
        MemberLevelVo memberLevelVo = new MemberLevelVo();
        BeanUtils.copyProperties(memberLevel,memberLevelVo);
        userInfoVo.setMemberLevelVo(memberLevelVo);
        return userInfoVo;
    }

    /**
     * 刷新token
     * @return
     */
    @Override
    public LoginVo refreshToken() {
        Long id = WebUtils.getMemberIdByRefreshToken();
        Member member = memberMapper.selectMemberById(id);
        LoginVo loginVo = generateToken(member);
        return loginVo;
    }

    /**
     * token有效校验
     * @return
     */
    @Override
    public boolean verifyToken() {
        String accessToken = WebUtils.getAccessToken();
        String refreshToken = WebUtils.getRefreshToken();
        if (ObjectUtils.isEmpty(accessToken)||ObjectUtils.isEmpty(refreshToken)){
            throw new RefreshTokenRefreshFailException();
        }
        if (!JwtUtils.validateToken(accessToken)){
            throw new AccessTokenAuthFailException();
        }
        return JwtUtils.validateToken(refreshToken);
    }

    /**
     * 批量获取用户信息
     * @param ids
     * @return
     */
    @Override
    public List<Member> selectMemberBatchByIds(List<Long> ids) {
        List<Member> list = memberMapper.selectMemberBatchToMemberByIds(ids);
        return list;
    }

    @Override
    public UserInfoVo getToUserInfo(Long toId) {
        Member member = memberMapper.selectMemberById(toId);
        UserInfoVo userInfoVo = new UserInfoVo();
        BeanUtils.copyProperties(member,userInfoVo);
        return userInfoVo;
    }

    /**
     * 生成token
     * @param member
     * @return
     */
    public LoginVo generateToken(Member member){
        if (ObjectUtils.isEmpty(member)) return null;
        String refreshToken = JwtUtils.generateRefreshToken(member.getId());
        TokenTO tokenTO = new TokenTO();
        BeanUtils.copyProperties(member,tokenTO);
        String accessToken = JwtUtils.generateAccessToken(tokenTO);
        return new LoginVo(refreshToken,accessToken);
    }

    /**
     * 批量查询用户信息
     * @param ids
     * @return
     */
    @Override
    public List<MemberTo> getMemberDuBboApi(List<Long> ids) {
        if (ids.isEmpty()) {
            return null;
        }
        List<MemberTo> list = memberMapper.selectMemberBatchByIds(ids);
        return list;
    }

    /**
     * 查询用户信息
     * @param id
     * @return
     */
    @Override
    public MemberTo getMemberInfoDuBboApi(Long id) {
        Member member = memberMapper.selectMemberById(id);
        MemberTo memberTo = new MemberTo();
        BeanUtils.copyProperties(member,memberTo);
        return memberTo;
    }
}
