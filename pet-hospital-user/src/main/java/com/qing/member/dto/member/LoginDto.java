package com.qing.member.dto.member;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/13 11:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto
{
    private String username;

    private String password;
}
