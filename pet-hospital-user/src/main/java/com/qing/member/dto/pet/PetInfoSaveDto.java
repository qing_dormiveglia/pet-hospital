package com.qing.member.dto.pet;

/**
 * @Author zzww
 * @Date 2024/5/6 0:19
 */

import com.qing.member.domain.PetCure;
import lombok.Data;

import java.util.List;

@Data
public class PetInfoSaveDto
{
    private Long id;
    private Integer birthType;
    private Integer vaccine;
    private String name;
    private Integer gender;
    private Long petCureId;
    private Long petKindId;
    private String headerImage;
    private String story;
    private String disease;
    private List<Long> labels;
}
