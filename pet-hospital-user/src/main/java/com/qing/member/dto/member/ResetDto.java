package com.qing.member.dto.member;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/13 15:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResetDto {
    private String username;

    private String code;

    private String password;

}
