package com.qing.member.dto.member;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/13 11:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginSmsDto
{
    private String phone;

    private String code;
}
