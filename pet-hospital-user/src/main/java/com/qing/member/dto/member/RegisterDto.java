package com.qing.member.dto.member;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/13 11:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {

    private String username;

    private String code;

    private String password;

    private String nickname;
}
