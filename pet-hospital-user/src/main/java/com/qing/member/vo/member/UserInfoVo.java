package com.qing.member.vo.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/2/18 17:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoVo
{
    /** 用户表主键 */
    private Long id;

    /** 会员等级id */
    private MemberLevelVo memberLevelVo;

    /** 手机号 */
    private String phone;

    /** 账号(其他类型账号) */
    private String username;

    /** 昵称 */
    private String nickname;

    /** 头像 */
    private String headerImage;

    /** 性别 */
    private Integer gender;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birth;

    /** 城市 */
    private String city;

    /** 职业 */
    private String job;

    /** 个性签名 */
    private String sign;

    /** 积分 */
    private Long integration;

    /** 成长值 */
    private Long growth;

    /** 启用状态 */
    private Integer status;

    /** 账号类型 */
    private Integer type;

    /** 认证状态 */
    private Long authStatus;

    /** 最近登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date lastLoginTime;



    /** 邮箱 */
    private String email;


}
