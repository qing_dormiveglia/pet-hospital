package com.qing.member.vo.pet;

import com.qing.member.domain.*;
import lombok.Data;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/4/26 22:29
 */
@Data
public class PetInfoVo extends UsmPetInfo
{
    private List<PetLabelEntity> labels;
    private PetKind petKind;
    private PetCure petCure;
}
