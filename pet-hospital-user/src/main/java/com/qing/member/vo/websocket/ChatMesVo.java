package com.qing.member.vo.websocket;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/2/21 17:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatMesVo
{
    private Long id;

    /** 发送用户id */
    private Long sendMemberId;

    /** 接收用户id */
    private Long acceptMemberId;

    /** 信息 */
    private String msg;


    /** 信息 */
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
