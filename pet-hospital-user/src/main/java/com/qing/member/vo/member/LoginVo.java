package com.qing.member.vo.member;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/13 11:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginVo {

    private String refreshToken;
    private String accessToken;
}
