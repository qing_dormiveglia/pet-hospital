package com.qing.member.vo.chat;

import com.qing.member.vo.member.MemberChatFriendsVo;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/2/22 1:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatFriendsVo
{
    private Long id;

    /** 用户id */
    private Long memberId;

    /** 用户的好友id */
    private List<MemberChatFriendsVo> memberChatFriendsVoList;
}
