package com.qing.member.vo.member;

import com.qing.member.vo.chat.ChatMsgVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/22 2:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberChatFriendsVo {

    /**
     * 好友表id
     */
    private Long ChatFriendsId;
    /** 好友id */
    private Long id;

    /** 好友昵称 */
    private String nickname;

    /** 好友头像 */
    private String headerImage;
    /** 好友最新消息 */
    private ChatMsgVo chatMsgVo;
    /** 数量 */
    private Integer num;


}
