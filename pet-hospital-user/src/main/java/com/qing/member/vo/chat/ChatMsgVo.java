package com.qing.member.vo.chat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/2/22 2:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatMsgVo
{
    private Long id;

    /** 发送用户id */
    private Long sendMemberId;

    /** 接收用户id */
    private Long acceptMemberId;

    /** 信息 */
    private String msg;

    /** 消息是否签收状态
     1：签收
     0：未签收
     */
    private Integer signFlag;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
