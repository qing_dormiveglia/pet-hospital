package com.qing.member.vo.websocket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class WebSocketVO implements Serializable
{
    //表示用户动作，干什么，签收消息，还是发送消息，.....
    private Integer action;//动作类型
    private WebSocketMessageVO webSocketMessageVo;//用户的聊天内容
    //代表需要去签收的消息id，逗号间隔
    private String extand;//扩展字段

}
