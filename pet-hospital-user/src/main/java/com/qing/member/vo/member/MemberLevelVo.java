package com.qing.member.vo.member;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author zzww
 * @Date 2024/2/18 17:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberLevelVo
{
    /** 用户等级表主键 */
    private Long id;

    /** 等级名称 */
    private String name;

    /** 等级需要的成长值 */
    private Long growthPoint;

    /** 默认等级 */
    private Integer defaultStatus;

    /** 免运费标准 */
    private BigDecimal freeFreightPoint;

    /** 每次评价获取的成长值 */
    private Long commentGrowthPoint;

    /** 是否有免邮特权 */
    private Integer priviledgeFreeFreight;

    /** 是否有会员价格特权 */
    private Integer priviledgeMemberPrice;
}
