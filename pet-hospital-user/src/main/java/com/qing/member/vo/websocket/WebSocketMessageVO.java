package com.qing.member.vo.websocket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * 信息类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class WebSocketMessageVO implements Serializable {
    private Long sendMemberId;//发送者id
    private Long acceptMemberId;//接收者id
    private String msg;//聊天内容
    private Long msgId; //用于消息的签收

}
