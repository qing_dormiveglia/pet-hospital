package com.qing.member.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author zzww
 * @Date 2022/12/10 19:36
 */
@Configuration
public class OssClientConfig {

    @Autowired
    OssMessageConfig ossMessage;

    @Bean(value = "ossClient")
    public OSS oss(){
        return new OSSClientBuilder().build(ossMessage.getEndpoint(), ossMessage.getAccessKeyId(),
                ossMessage.getAccessKeySecret());
    }
}
