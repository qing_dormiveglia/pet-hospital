package com.qing.member.config;

import com.qing.common.constant.TokenEnum;
import com.qing.common.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import java.util.List;
import java.util.Map;

/**
 * @Author zzww
 * @Date 2024/2/20 20:35
 */
@Configuration
@Slf4j
public class WebSocketConfig extends ServerEndpointConfig.Configurator
{
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }

    /**
     * 建立握手时，连接前的操作
     */
    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        // 这个userProperties 可以通过 session.getUserProperties()获取
        final Map<String, Object> userProperties = sec.getUserProperties();
        Map<String, List<String>> headers = request.getHeaders();
        List<String> protocol = headers.get("sec-websocket-protocol");
        response.getHeaders().put("Sec-WebSocket-Protocol" ,protocol);
         // 存放自己想要的header信息
        if(protocol != null){
            log.info(protocol.toString());
            userProperties.put(TokenEnum.ACCESS_TOKEN.getValue(), protocol.get(0));
        }
        super.modifyHandshake(sec, request, response);
    }

    /**
     * 初始化端点对象,也就是被@ServerEndpoint所标注的对象
     */
    @Override
    public <T> T getEndpointInstance(Class<T> clazz) throws InstantiationException {
        return super.getEndpointInstance(clazz);
    }
}
