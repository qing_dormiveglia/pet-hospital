package com.qing.member.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author zzww
 * @Date 2022/12/9 21:07
 */

@Data
@Configuration
@ConfigurationProperties("aliyun.oss.message")
public class OssMessageConfig {
//    https://oss-cn-hangzhou.aliyuncs.com

    private String endpoint;

    private String accessKeyId;

    private String accessKeySecret;

    private String bucketName;



}
