package com.qing.member.controller;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.qing.member.vo.chat.ChatFriendsVo;
import com.qing.member.vo.member.MemberChatFriendsVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.member.domain.ChatFriends;
import com.qing.member.service.IChatFriendsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 好友Controller
 * 
 * @author zzww
 * @date 2024-02-21
 */
@RestController
@RequestMapping("/member/friends")
public class ChatFriendsController extends BaseController
{
    @Resource
    private IChatFriendsService chatFriendsService;

    /**
     * 查询好友列表
     */
    @GetMapping("/list")
    public TableDataInfo list()
    {
        startPage();
        List<MemberChatFriendsVo> list = chatFriendsService.findChatFriendsList();
        return getDataTable(list);
    }

    /**
     * 获取未通知消息列表
     */
    @GetMapping("/not-inform/list")
    public TableDataInfo notInformList()
    {
        startPage();
        List<MemberChatFriendsVo> list = chatFriendsService.findNotInformChatFriendsList();
        return getDataTable(list);
    }

    /**
     * 获取已通知消息列表
     */
    @GetMapping("/inform/list")
    public TableDataInfo InformList()
    {
        startPage();
        List<MemberChatFriendsVo> list = chatFriendsService.findInformChatFriendsList();
        return getDataTable(list);
    }
    /**
     * 通知删除
     */
//    @PreAuthorize("@ss.hasPermi('member:friends:remove')")
//    @Log(title = "好友", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(chatFriendsService.deleteChatFriendsByIds(ids));
    }

    /**
     * 导出好友列表
     */
    @PreAuthorize("@ss.hasPermi('member:friends:export')")
    @Log(title = "好友", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChatFriends chatFriends)
    {
        List<ChatFriends> list = chatFriendsService.selectChatFriendsList(chatFriends);
        ExcelUtil<ChatFriends> util = new ExcelUtil<ChatFriends>(ChatFriends.class);
        util.exportExcel(response, list, "好友数据");
    }

    /**
     * 获取好友详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:friends:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(chatFriendsService.selectChatFriendsById(id));
    }

    /**
     * 新增好友
     */
    @PreAuthorize("@ss.hasPermi('member:friends:add')")
    @Log(title = "好友", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChatFriends chatFriends)
    {
        return toAjax(chatFriendsService.insertChatFriends(chatFriends));
    }

    /**
     * 修改好友
     */
    @PreAuthorize("@ss.hasPermi('member:friends:edit')")
    @Log(title = "好友", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChatFriends chatFriends)
    {
        return toAjax(chatFriendsService.updateChatFriends(chatFriends));
    }


}
