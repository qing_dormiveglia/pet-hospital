package com.qing.member.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.member.domain.ChatFriendsRequest;
import com.qing.member.service.IChatFriendsRequestService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 发送事件Controller
 * 
 * @author zzww
 * @date 2024-02-21
 */
@RestController
@RequestMapping("/member/request")
public class ChatFriendsRequestController extends BaseController
{
    @Autowired
    private IChatFriendsRequestService chatFriendsRequestService;

    /**
     * 查询发送事件列表
     */
    @PreAuthorize("@ss.hasPermi('member:request:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChatFriendsRequest chatFriendsRequest)
    {
        startPage();
        List<ChatFriendsRequest> list = chatFriendsRequestService.selectChatFriendsRequestList(chatFriendsRequest);
        return getDataTable(list);
    }

    /**
     * 导出发送事件列表
     */
    @PreAuthorize("@ss.hasPermi('member:request:export')")
    @Log(title = "发送事件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChatFriendsRequest chatFriendsRequest)
    {
        List<ChatFriendsRequest> list = chatFriendsRequestService.selectChatFriendsRequestList(chatFriendsRequest);
        ExcelUtil<ChatFriendsRequest> util = new ExcelUtil<ChatFriendsRequest>(ChatFriendsRequest.class);
        util.exportExcel(response, list, "发送事件数据");
    }

    /**
     * 获取发送事件详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:request:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(chatFriendsRequestService.selectChatFriendsRequestById(id));
    }

    /**
     * 新增发送事件
     */
    @PreAuthorize("@ss.hasPermi('member:request:add')")
    @Log(title = "发送事件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChatFriendsRequest chatFriendsRequest)
    {
        return toAjax(chatFriendsRequestService.insertChatFriendsRequest(chatFriendsRequest));
    }

    /**
     * 修改发送事件
     */
    @PreAuthorize("@ss.hasPermi('member:request:edit')")
    @Log(title = "发送事件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChatFriendsRequest chatFriendsRequest)
    {
        return toAjax(chatFriendsRequestService.updateChatFriendsRequest(chatFriendsRequest));
    }

    /**
     * 删除发送事件
     */
    @PreAuthorize("@ss.hasPermi('member:request:remove')")
    @Log(title = "发送事件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(chatFriendsRequestService.deleteChatFriendsRequestByIds(ids));
    }
}
