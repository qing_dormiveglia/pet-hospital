package com.qing.member.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.qing.member.vo.chat.ChatMsgVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.member.domain.ChatMsg;
import com.qing.member.service.IChatMsgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 消息Controller
 * 
 * @author zzww
 * @date 2024-02-21
 */
@RestController
@RequestMapping("/member/msg")
public class ChatMsgController extends BaseController
{
    @Autowired
    private IChatMsgService chatMsgService;

    /**
     * 查询消息列表
     */
//    @PreAuthorize("@ss.hasPermi('member:msg:list')")
    @GetMapping("/list/{toUserId}")
    public TableDataInfo list(@PathVariable("toUserId") Long toUserId)
    {
        startPage();
        List<ChatMsgVo> list = chatMsgService.selectChatMsgListByToIdAndUId(toUserId);
        return getDataTable(list);
    }

    /**
     * 导出消息列表
     */
    @PreAuthorize("@ss.hasPermi('member:msg:export')")
    @Log(title = "消息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChatMsg chatMsg)
    {
        List<ChatMsg> list = chatMsgService.selectChatMsgList(chatMsg);
        ExcelUtil<ChatMsg> util = new ExcelUtil<ChatMsg>(ChatMsg.class);
        util.exportExcel(response, list, "消息数据");
    }

    /**
     * 获取消息详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:msg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(chatMsgService.selectChatMsgById(id));
    }

    /**
     * 新增消息
     */
    @PreAuthorize("@ss.hasPermi('member:msg:add')")
    @Log(title = "消息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChatMsg chatMsg)
    {
        return toAjax(chatMsgService.insertChatMsg(chatMsg));
    }

    /**
     * 修改消息
     */
    @PreAuthorize("@ss.hasPermi('member:msg:edit')")
    @Log(title = "消息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChatMsg chatMsg)
    {
        return toAjax(chatMsgService.updateChatMsg(chatMsg));
    }

    /**
     * 删除消息
     */
    @PreAuthorize("@ss.hasPermi('member:msg:remove')")
    @Log(title = "消息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(chatMsgService.deleteChatMsgByIds(ids));
    }
}
