package com.qing.member.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.qing.member.domain.UsmPetInfoEntity;
import com.qing.member.dto.pet.PetInfoSaveDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.member.domain.UsmPetInfo;
import com.qing.member.service.IUsmPetInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author zzww
 * @date 2024-02-08
 */
@RestController
@RequestMapping("/member/pet/info")
public class UsmPetInfoController extends BaseController
{
    @Autowired
    private IUsmPetInfoService usmPetInfoService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('member:info:list')")
    @GetMapping("/list")
    public TableDataInfo list()
    {
        startPage();
        List<UsmPetInfoEntity> list = usmPetInfoService.selectUsmPetInfoListMy();
        return getDataTable(list);
    }

    /**
     * 新增【请填写功能名称】
     */
    @PostMapping("/save-pet-info")
    public AjaxResult save(@RequestBody Map<String,Object> params)
    {
        PetInfoSaveDto petInfoSaveDto = JSONObject.parseObject(JSON.toJSONString(params), PetInfoSaveDto.class);
        return toAjax(usmPetInfoService.insertPetInfoSave(petInfoSaveDto));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PostMapping("/update-pet-info")
    public AjaxResult updateInfo(@RequestBody Map<String,Object> params)
    {
        PetInfoSaveDto petInfoSaveDto = JSONObject.parseObject(JSON.toJSONString(params), PetInfoSaveDto.class);
        return toAjax(usmPetInfoService.updatePetInfoSave(petInfoSaveDto));
    }



    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('member:info:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UsmPetInfo usmPetInfo)
    {
        List<UsmPetInfo> list = usmPetInfoService.selectUsmPetInfoList(usmPetInfo);
        ExcelUtil<UsmPetInfo> util = new ExcelUtil<UsmPetInfo>(UsmPetInfo.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    @GetMapping(value = "/info/{id}")
    public AjaxResult getInfoPet(@PathVariable("id") Long id)
    {
        return success(usmPetInfoService.selectUsmPetInfoVoById(id));
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
//    @PreAuthorize("@ss.hasPermi('member:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(usmPetInfoService.selectUsmPetInfoById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:info:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UsmPetInfo usmPetInfo)
    {
        return toAjax(usmPetInfoService.insertUsmPetInfo(usmPetInfo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:info:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UsmPetInfo usmPetInfo)
    {
        return toAjax(usmPetInfoService.updateUsmPetInfo(usmPetInfo));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:info:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(usmPetInfoService.deleteUsmPetInfoByIds(ids));
    }
}
