package com.qing.member.controller;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.qing.common.dubbo.ThirdPartyApi;
import com.qing.common.utils.WebUtils;
import com.qing.member.config.OssServiceApi;
import com.qing.member.dto.member.LoginDto;
import com.qing.member.dto.member.LoginSmsDto;
import com.qing.member.dto.member.RegisterDto;
import com.qing.member.dto.member.ResetDto;
import com.qing.member.vo.member.LoginVo;
import com.qing.member.vo.member.UserInfoVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.member.domain.Member;
import com.qing.member.service.IMemberService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 【请填写功能名称】Controller
 * 
 * @author zzww
 * @date 2024-02-08
 */
@RestController
@RequestMapping("/member/member")
public class MemberController extends BaseController
{
    @Resource
    private IMemberService memberService;
//    @DubboReference
//    private ThirdPartyApi thirdPartyApi;
    @Resource
    private OssServiceApi ossServiceApi;


    /**
     * 刷新token
     * @return
     */
    @GetMapping("/refresh")
    public AjaxResult refreshToken()
    {
        LoginVo loginVo = memberService.refreshToken();
        return AjaxResult.success(loginVo);
    }

    /**
     * token有效校验
     * @return
     */
    @GetMapping("/verify-access-token")
    public AjaxResult verifyToken()
    {
        boolean bool = memberService.verifyToken();
        return AjaxResult.success(bool);
    }

    /**
     * 修改用户信息
     */
    @PostMapping("update-info-user")
    public AjaxResult updateInfoUser(@RequestBody Map<String,Object> map)
    {
        Member member = JSONObject.parseObject(JSON.toJSONString(map), Member.class);
        Long id = WebUtils.getMemberIdByAccessToken();
        member.setId(id);
        return toAjax(memberService.updateMember(member));
    }


    /**
     * 获取用户详情信息
     */
    @GetMapping("/userInfo")
    public AjaxResult getUserInfo()
    {
        UserInfoVo userInfo = memberService.getUserInfo();
        return AjaxResult.success(userInfo);
    }

    /**
     * 获取好友用户信息
     */
    @GetMapping("/to-user/{toId}")
    public AjaxResult getToUserInfo(@PathVariable("toId") Long toId)
    {
        UserInfoVo userInfo = memberService.getToUserInfo(toId);
        return AjaxResult.success(userInfo);
    }

    /**
     * 账号密码登录
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginDto loginDto)
    {
        LoginVo loginVo = memberService.memberLogin(loginDto);
        return AjaxResult.success(loginVo);
    }

    /**
     * 短信验证码登录
     */
    @PostMapping("/login/sms")
    public AjaxResult loginSms(@RequestBody LoginSmsDto loginSmsDto)
    {
        LoginVo loginVo = memberService.memberLoginSms(loginSmsDto);
        return AjaxResult.success(loginVo);
    }
    /**
     * 注册
     */
    @PostMapping("/register")
    public AjaxResult register(@RequestBody RegisterDto registerDto)
    {
        LoginVo loginVo = memberService.memberRegister(registerDto);
        return AjaxResult.success(loginVo);
    }
    /**
     * 重置密码，找回密码
     */
    @PostMapping("/reset")
    public AjaxResult reset(@RequestBody ResetDto resetDto)
    {
        LoginVo loginVo = memberService.memberResetPassword(resetDto);
        return AjaxResult.success(loginVo);
    }

    @RequestMapping("/load/image-head")
    public AjaxResult up(MultipartFile file, String bucketName){
        String url = ossServiceApi.OssUpload(file,bucketName);
        Long id = WebUtils.getMemberIdByAccessToken();
        Member member = new Member();
        member.setId(id);
        member.setHeaderImage(url);
        int i = memberService.updateMember(member);
        if (i==0){
            throw new RuntimeException("失败");
        }
        return AjaxResult.success();
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('member:member:list')")
    @GetMapping("/list")
    public TableDataInfo list(Member member)
    {
        startPage();
        List<Member> list = memberService.selectMemberList(member);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('member:member:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Member member)
    {
        List<Member> list = memberService.selectMemberList(member);
        ExcelUtil<Member> util = new ExcelUtil<Member>(Member.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:member:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(memberService.selectMemberById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:member:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Member member)
    {
        return toAjax(memberService.insertMember(member));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:member:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Member member)
    {
        return toAjax(memberService.updateMember(member));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:member:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(memberService.deleteMemberByIds(ids));
    }
}
