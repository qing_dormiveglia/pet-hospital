package com.qing.member.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.member.domain.MemberImageAuth;
import com.qing.member.service.IMemberImageAuthService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author zzww
 * @date 2024-02-08
 */
@RestController
@RequestMapping("/member/auth")
public class MemberImageAuthController extends BaseController
{
    @Autowired
    private IMemberImageAuthService memberImageAuthService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('member:auth:list')")
    @GetMapping("/list")
    public TableDataInfo list(MemberImageAuth memberImageAuth)
    {
        startPage();
        List<MemberImageAuth> list = memberImageAuthService.selectMemberImageAuthList(memberImageAuth);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('member:auth:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MemberImageAuth memberImageAuth)
    {
        List<MemberImageAuth> list = memberImageAuthService.selectMemberImageAuthList(memberImageAuth);
        ExcelUtil<MemberImageAuth> util = new ExcelUtil<MemberImageAuth>(MemberImageAuth.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:auth:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(memberImageAuthService.selectMemberImageAuthById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:auth:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MemberImageAuth memberImageAuth)
    {
        return toAjax(memberImageAuthService.insertMemberImageAuth(memberImageAuth));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:auth:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MemberImageAuth memberImageAuth)
    {
        return toAjax(memberImageAuthService.updateMemberImageAuth(memberImageAuth));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('member:auth:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(memberImageAuthService.deleteMemberImageAuthByIds(ids));
    }
}
