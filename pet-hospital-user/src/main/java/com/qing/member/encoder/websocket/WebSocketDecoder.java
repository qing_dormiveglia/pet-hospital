package com.qing.member.encoder.websocket;

import com.alibaba.fastjson2.JSONObject;
import com.qing.member.vo.websocket.ChatMesVo;
import com.qing.member.vo.websocket.WebSocketVO;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 * @Author zzww
 * @Date 2024/2/21 19:05
 */
public class WebSocketDecoder implements Decoder.Text<WebSocketVO>{
    @Override
    public void destroy() {}
    @Override
    public void init(EndpointConfig arg0) {}
    @Override
    public WebSocketVO decode(String s) throws DecodeException {
        return JSONObject.parseObject(s, WebSocketVO.class);
    }
    @Override
    public boolean willDecode(String arg0) {
        return true;
    }
}
