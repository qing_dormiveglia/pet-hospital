package com.qing.member.encoder.websocket;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.qing.member.vo.websocket.ChatMesVo;
import com.qing.member.vo.websocket.WebSocketVO;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 * @Author zzww
 * @Date 2024/2/21 19:08
 */
public class WebSocketEncoder implements Encoder.Text<ChatMesVo>{

    @Override
    public void init(EndpointConfig endpointConfig) {}
    @Override
    public void destroy() {}


    @Override
    public String encode(ChatMesVo object) throws EncodeException {
        String s = "";
        try {
            s = JSON.toJSONString(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }
}
