package com.qing.member.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.member.domain.PetLabel;
import com.qing.member.domain.PetLabelEntity;
import com.qing.member.domain.UsmPetInfo;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface PetLabelMapper extends BaseMapper<PetLabelEntity>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public PetLabel selectPetLabelById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param petLabel 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<PetLabel> selectPetLabelList(PetLabel petLabel);

    /**
     * 新增【请填写功能名称】
     * 
     * @param petLabel 【请填写功能名称】
     * @return 结果
     */
    public int insertPetLabel(PetLabel petLabel);

    /**
     * 修改【请填写功能名称】
     * 
     * @param petLabel 【请填写功能名称】
     * @return 结果
     */
    public int updatePetLabel(PetLabel petLabel);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deletePetLabelById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePetLabelByIds(Long[] ids);
}
