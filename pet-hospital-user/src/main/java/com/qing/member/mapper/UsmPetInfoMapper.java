package com.qing.member.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.member.domain.UsmPetInfo;
import com.qing.member.domain.UsmPetInfoEntity;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface UsmPetInfoMapper extends BaseMapper<UsmPetInfoEntity>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public UsmPetInfo selectUsmPetInfoById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<UsmPetInfo> selectUsmPetInfoList(UsmPetInfo usmPetInfo);

    /**
     * 新增【请填写功能名称】
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 结果
     */
    public int insertUsmPetInfo(UsmPetInfo usmPetInfo);

    /**
     * 修改【请填写功能名称】
     * 
     * @param usmPetInfo 【请填写功能名称】
     * @return 结果
     */
    public int updateUsmPetInfo(UsmPetInfo usmPetInfo);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteUsmPetInfoById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUsmPetInfoByIds(Long[] ids);
}
