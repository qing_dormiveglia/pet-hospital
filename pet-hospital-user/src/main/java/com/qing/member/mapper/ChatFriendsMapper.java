package com.qing.member.mapper;

import java.util.List;
import com.qing.member.domain.ChatFriends;
import org.apache.ibatis.annotations.Param;

/**
 * 好友Mapper接口
 * 
 * @author zzww
 * @date 2024-02-21
 */
public interface ChatFriendsMapper 
{
    /**
     * 查询好友
     * 
     * @param id 好友主键
     * @return 好友
     */
    public ChatFriends selectChatFriendsById(Long id);

    /**
     * 查询好友列表
     * 
     * @param chatFriends 好友
     * @return 好友集合
     */
    public List<ChatFriends> selectChatFriendsList(ChatFriends chatFriends);

    /**
     * 新增好友
     * 
     * @param chatFriends 好友
     * @return 结果
     */
    public int insertChatFriends(ChatFriends chatFriends);

    /**
     * 修改好友
     * 
     * @param chatFriends 好友
     * @return 结果
     */
    public int updateChatFriends(ChatFriends chatFriends);

    /**
     * 删除好友
     * 
     * @param id 好友主键
     * @return 结果
     */
    public int deleteChatFriendsById(Long id);

    /**
     * 批量删除好友
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChatFriendsByIds(Long[] ids);

    /**
     * 获取未签收的好友列表
     * @param memberId
     * @param type
     * @return
     */
    List<ChatFriends> selectInformChatFriendsList(@Param("memberId") Long memberId,@Param("type") Integer type);
}
