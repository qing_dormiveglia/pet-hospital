package com.qing.member.mapper;

import java.util.List;
import com.qing.member.domain.PetImages;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface PetImagesMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public PetImages selectPetImagesById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param petImages 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<PetImages> selectPetImagesList(PetImages petImages);

    /**
     * 新增【请填写功能名称】
     * 
     * @param petImages 【请填写功能名称】
     * @return 结果
     */
    public int insertPetImages(PetImages petImages);

    /**
     * 修改【请填写功能名称】
     * 
     * @param petImages 【请填写功能名称】
     * @return 结果
     */
    public int updatePetImages(PetImages petImages);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deletePetImagesById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePetImagesByIds(Long[] ids);
}
