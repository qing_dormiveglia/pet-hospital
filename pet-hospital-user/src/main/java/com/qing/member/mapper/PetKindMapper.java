package com.qing.member.mapper;

import java.util.List;
import com.qing.member.domain.PetKind;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface PetKindMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public PetKind selectPetKindById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param petKind 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<PetKind> selectPetKindList(PetKind petKind);

    /**
     * 新增【请填写功能名称】
     * 
     * @param petKind 【请填写功能名称】
     * @return 结果
     */
    public int insertPetKind(PetKind petKind);

    /**
     * 修改【请填写功能名称】
     * 
     * @param petKind 【请填写功能名称】
     * @return 结果
     */
    public int updatePetKind(PetKind petKind);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deletePetKindById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePetKindByIds(Long[] ids);
}
