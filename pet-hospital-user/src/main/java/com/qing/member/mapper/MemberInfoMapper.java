package com.qing.member.mapper;

import java.util.List;
import com.qing.member.domain.MemberInfo;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface MemberInfoMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public MemberInfo selectMemberInfoById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberInfo 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MemberInfo> selectMemberInfoList(MemberInfo memberInfo);

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberInfo 【请填写功能名称】
     * @return 结果
     */
    public int insertMemberInfo(MemberInfo memberInfo);

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberInfo 【请填写功能名称】
     * @return 结果
     */
    public int updateMemberInfo(MemberInfo memberInfo);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMemberInfoById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMemberInfoByIds(Long[] ids);
}
