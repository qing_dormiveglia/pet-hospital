package com.qing.member.mapper;

import java.util.List;

import com.qing.common.to.MemberTo;
import com.qing.member.domain.Member;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface MemberMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Member selectMemberById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param member 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Member> selectMemberList(Member member);

    /**
     * 新增【请填写功能名称】
     * 
     * @param member 【请填写功能名称】
     * @return 结果
     */
    public int insertMember(Member member);

    /**
     * 修改【请填写功能名称】
     * 
     * @param member 【请填写功能名称】
     * @return 结果
     */
    public int updateMember(Member member);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMemberById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMemberByIds(Long[] ids);

    /**
     * 根据账号查询用户信息
     * @param username
     * @return
     */
    Member selectMemberByUsername(String username);

    /**
     * 批量获取用户信息
     * @param ids
     * @return
     */
    List<MemberTo> selectMemberBatchByIds(List<Long> ids);

    /**
     * 批量获取用户信息
     * @param ids
     * @return
     */
    List<Member> selectMemberBatchToMemberByIds(List<Long> ids);
}
