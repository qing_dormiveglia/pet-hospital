package com.qing.member.mapper;

import java.util.List;
import com.qing.member.domain.MemberImageAuth;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface MemberImageAuthMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public MemberImageAuth selectMemberImageAuthById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param memberImageAuth 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MemberImageAuth> selectMemberImageAuthList(MemberImageAuth memberImageAuth);

    /**
     * 新增【请填写功能名称】
     * 
     * @param memberImageAuth 【请填写功能名称】
     * @return 结果
     */
    public int insertMemberImageAuth(MemberImageAuth memberImageAuth);

    /**
     * 修改【请填写功能名称】
     * 
     * @param memberImageAuth 【请填写功能名称】
     * @return 结果
     */
    public int updateMemberImageAuth(MemberImageAuth memberImageAuth);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMemberImageAuthById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMemberImageAuthByIds(Long[] ids);
}
