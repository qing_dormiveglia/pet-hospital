package com.qing.member.mapper;

import java.util.List;
import com.qing.member.domain.ChatMsg;
import org.apache.ibatis.annotations.Param;

/**
 * 消息Mapper接口
 * 
 * @author zzww
 * @date 2024-02-21
 */
public interface ChatMsgMapper 
{
    /**
     * 查询消息
     * 
     * @param id 消息主键
     * @return 消息
     */
    public ChatMsg selectChatMsgById(Long id);

    /**
     * 查询消息列表
     * 
     * @param chatMsg 消息
     * @return 消息集合
     */
    public List<ChatMsg> selectChatMsgList(ChatMsg chatMsg);

    /**
     * 新增消息
     * 
     * @param chatMsg 消息
     * @return 结果
     */
    public int insertChatMsg(ChatMsg chatMsg);

    /**
     * 修改消息
     * 
     * @param chatMsg 消息
     * @return 结果
     */
    public int updateChatMsg(ChatMsg chatMsg);

    /**
     * 删除消息
     * 
     * @param id 消息主键
     * @return 结果
     */
    public int deleteChatMsgById(Long id);

    /**
     * 批量删除消息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChatMsgByIds(Long[] ids);

    /**
     * 批量获取消息（根据时间进行排序）
     * @param ids
     * @return
     */
    List<ChatMsg> selectChatMsgBatchByMemberId(List<Long> ids);

    /**
     * 批量修改签收状态信息
     * @param ids
     * @return
     */
    int updateChatMsgBatchByIds(List<Long> ids);

    /**
     * 获取最新消息
     * @param friendsId
     * @param memberId
     * @return
     */
    ChatMsg selectChatMsgByFMIDAndTimeDesc(@Param("friendsId") Long friendsId,@Param("memberId") Long memberId);

    /**
     * 获取未签收消息数量
     * @param friendMemberId
     * @param memberId
     * @param type
     * @return
     */
    Integer selectChatMsgByIdAndFIdToNum(@Param("friendMemberId") Long friendMemberId,@Param("memberId") Long memberId,@Param("type") Integer type);

    List<ChatMsg> selectChatMsgListByTimeDesc(@Param("toUserId") Long toUserId,@Param("id") Long id);
}
