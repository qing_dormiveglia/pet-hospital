package com.qing.member.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 消息对象 ums_chat_msg
 * 
 * @author zzww
 * @date 2024-02-21
 */
public class ChatMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 发送用户id */
    @Excel(name = "发送用户id")
    private Long sendMemberId;

    /** 接收用户id */
    @Excel(name = "接收用户id")
    private Long acceptMemberId;

    /** 信息 */
    @Excel(name = "信息")
    private String msg;

    /** 消息是否签收状态
1：签收
0：未签收
 */
    @Excel(name = "消息是否签收状态 1：签收 0：未签收 ")
    private Integer signFlag;

    /** 逻辑删除 */
    @Excel(name = "逻辑删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSendMemberId(Long sendMemberId) 
    {
        this.sendMemberId = sendMemberId;
    }

    public Long getSendMemberId() 
    {
        return sendMemberId;
    }
    public void setAcceptMemberId(Long acceptMemberId) 
    {
        this.acceptMemberId = acceptMemberId;
    }

    public Long getAcceptMemberId() 
    {
        return acceptMemberId;
    }
    public void setMsg(String msg) 
    {
        this.msg = msg;
    }

    public String getMsg() 
    {
        return msg;
    }
    public void setSignFlag(Integer signFlag) 
    {
        this.signFlag = signFlag;
    }

    public Integer getSignFlag()
    {
        return signFlag;
    }
    public void setIsDelete(Integer isDelete)
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sendMemberId", getSendMemberId())
            .append("acceptMemberId", getAcceptMemberId())
            .append("msg", getMsg())
            .append("signFlag", getSignFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
