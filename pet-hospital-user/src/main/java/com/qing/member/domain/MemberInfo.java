package com.qing.member.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 ums_member_info
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class MemberInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户详情表主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long memberId;

    /** 证件类型 */
    @Excel(name = "证件类型")
    private String certificatesType;

    /** 证件编号 */
    @Excel(name = "证件编号")
    private String certificatesNo;

    /** 证件路径 */
    @Excel(name = "证件路径")
    private String certificatesUrl;

    /** 认证状态 */
    @Excel(name = "认证状态")
    private Integer authStatus;

    /** 姓名 */
    @Excel(name = "姓名")
    private String fullName;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setCertificatesType(String certificatesType) 
    {
        this.certificatesType = certificatesType;
    }

    public String getCertificatesType() 
    {
        return certificatesType;
    }
    public void setCertificatesNo(String certificatesNo) 
    {
        this.certificatesNo = certificatesNo;
    }

    public String getCertificatesNo() 
    {
        return certificatesNo;
    }
    public void setCertificatesUrl(String certificatesUrl) 
    {
        this.certificatesUrl = certificatesUrl;
    }

    public String getCertificatesUrl() 
    {
        return certificatesUrl;
    }
    public void setAuthStatus(Integer authStatus) 
    {
        this.authStatus = authStatus;
    }

    public Integer getAuthStatus() 
    {
        return authStatus;
    }
    public void setFullName(String fullName) 
    {
        this.fullName = fullName;
    }

    public String getFullName() 
    {
        return fullName;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("certificatesType", getCertificatesType())
            .append("certificatesNo", getCertificatesNo())
            .append("certificatesUrl", getCertificatesUrl())
            .append("authStatus", getAuthStatus())
            .append("fullName", getFullName())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
