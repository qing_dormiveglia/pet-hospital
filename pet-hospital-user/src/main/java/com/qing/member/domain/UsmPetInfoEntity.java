package com.qing.member.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/4/26 23:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("usm_pet_info")
public class UsmPetInfoEntity
{
    /** 宠物基本信息表主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long memberId;

    /** 宠物种类id */
    @Excel(name = "宠物种类id")
    private Long petKindId;

    /** 治疗类型表id */
    @Excel(name = "治疗类型表id")
    private Long petCureId;

    /** 宠物标签ids(通过，相隔) */
    @Excel(name = "宠物标签ids(通过，相隔)")
    private String petLabelIds;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 头像 */
    @Excel(name = "头像")
    private String headerImage;

    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;

    /** 疫苗接种状态 */
    @Excel(name = "疫苗接种状态")
    private Integer vaccine;

    /** 成年状态 */
    @Excel(name = "成年状态")
    private Integer birthType;

    /** 故事 */
    @Excel(name = "故事")
    private String story;

    /** 病情描述 */
    @Excel(name = "病情描述")
    private String disease;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 备注 */
    private String remark;
}
