package com.qing.member.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 ums_member_level
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class MemberLevel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户等级表主键 */
    private Long id;

    /** 等级名称 */
    @Excel(name = "等级名称")
    private String name;

    /** 等级需要的成长值 */
    @Excel(name = "等级需要的成长值")
    private Long growthPoint;

    /** 默认等级 */
    @Excel(name = "默认等级")
    private Integer defaultStatus;

    /** 免运费标准 */
    @Excel(name = "免运费标准")
    private BigDecimal freeFreightPoint;

    /** 每次评价获取的成长值 */
    @Excel(name = "每次评价获取的成长值")
    private Long commentGrowthPoint;

    /** 是否有免邮特权 */
    @Excel(name = "是否有免邮特权")
    private Integer priviledgeFreeFreight;

    /** 是否有会员价格特权 */
    @Excel(name = "是否有会员价格特权")
    private Integer priviledgeMemberPrice;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGrowthPoint(Long growthPoint) 
    {
        this.growthPoint = growthPoint;
    }

    public Long getGrowthPoint() 
    {
        return growthPoint;
    }
    public void setDefaultStatus(Integer defaultStatus) 
    {
        this.defaultStatus = defaultStatus;
    }

    public Integer getDefaultStatus() 
    {
        return defaultStatus;
    }
    public void setFreeFreightPoint(BigDecimal freeFreightPoint) 
    {
        this.freeFreightPoint = freeFreightPoint;
    }

    public BigDecimal getFreeFreightPoint() 
    {
        return freeFreightPoint;
    }
    public void setCommentGrowthPoint(Long commentGrowthPoint) 
    {
        this.commentGrowthPoint = commentGrowthPoint;
    }

    public Long getCommentGrowthPoint() 
    {
        return commentGrowthPoint;
    }
    public void setPriviledgeFreeFreight(Integer priviledgeFreeFreight) 
    {
        this.priviledgeFreeFreight = priviledgeFreeFreight;
    }

    public Integer getPriviledgeFreeFreight() 
    {
        return priviledgeFreeFreight;
    }
    public void setPriviledgeMemberPrice(Integer priviledgeMemberPrice) 
    {
        this.priviledgeMemberPrice = priviledgeMemberPrice;
    }

    public Integer getPriviledgeMemberPrice() 
    {
        return priviledgeMemberPrice;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("growthPoint", getGrowthPoint())
            .append("defaultStatus", getDefaultStatus())
            .append("freeFreightPoint", getFreeFreightPoint())
            .append("commentGrowthPoint", getCommentGrowthPoint())
            .append("priviledgeFreeFreight", getPriviledgeFreeFreight())
            .append("priviledgeMemberPrice", getPriviledgeMemberPrice())
            .append("remark", getRemark())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
