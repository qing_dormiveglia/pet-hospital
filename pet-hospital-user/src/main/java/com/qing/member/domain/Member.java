package com.qing.member.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 ums_member
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class Member extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户表主键 */
    private Long id;

    /** 会员等级id */
    @Excel(name = "会员等级id")
    private Long memberLevelId;

    /** 用户的openid(微信小程序) */
    @Excel(name = "用户的openid(微信小程序)")
    private Long openInfoId;

    /** 用户的union_id（用户唯一性） */
    @Excel(name = "用户的union_id", readConverterExp = "用=户唯一性")
    private Long unionId;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 账号(其他类型账号) */
    @Excel(name = "账号(其他类型账号)")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 昵称 */
    @Excel(name = "昵称")
    private String nickname;

    /** 头像 */
    @Excel(name = "头像")
    private String headerImage;

    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birth;

    /** 城市 */
    @Excel(name = "城市")
    private String city;

    /** 职业 */
    @Excel(name = "职业")
    private String job;

    /** 个性签名 */
    @Excel(name = "个性签名")
    private String sign;

    /** 积分 */
    @Excel(name = "积分")
    private Long integration;

    /** 成长值 */
    @Excel(name = "成长值")
    private Long growth;

    /** 启用状态 */
    @Excel(name = "启用状态")
    private Integer status;

    /** 账号类型 */
    @Excel(name = "账号类型")
    private Integer type;

    /** 认证状态 */
    @Excel(name = "认证状态")
    private Long authStatus;

    /** 最近登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最近登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastLoginTime;

    /** 账号权限 */
    @Excel(name = "账号权限")
    private Integer accountPermission;

    /** 用户会话密钥 */
    @Excel(name = "用户会话密钥")
    private String sessionKey;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 授权时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "授权时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date authTime;

    /** 一键登录凭证 */
    @Excel(name = "一键登录凭证")
    private String authCredential;

    /** 逻辑删除 */
    @Excel(name = "逻辑删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMemberLevelId(Long memberLevelId) 
    {
        this.memberLevelId = memberLevelId;
    }

    public Long getMemberLevelId() 
    {
        return memberLevelId;
    }
    public void setOpenInfoId(Long openInfoId) 
    {
        this.openInfoId = openInfoId;
    }

    public Long getOpenInfoId() 
    {
        return openInfoId;
    }
    public void setUnionId(Long unionId) 
    {
        this.unionId = unionId;
    }

    public Long getUnionId() 
    {
        return unionId;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getNickname()
    {
        return nickname;
    }
    public void setHeaderImage(String headerImage)
    {
        this.headerImage = headerImage;
    }

    public String getHeaderImage()
    {
        return headerImage;
    }
    public void setGender(Integer gender) 
    {
        this.gender = gender;
    }

    public Integer getGender() 
    {
        return gender;
    }
    public void setBirth(Date birth) 
    {
        this.birth = birth;
    }

    public Date getBirth() 
    {
        return birth;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setJob(String job) 
    {
        this.job = job;
    }

    public String getJob() 
    {
        return job;
    }
    public void setSign(String sign) 
    {
        this.sign = sign;
    }

    public String getSign() 
    {
        return sign;
    }
    public void setIntegration(Long integration) 
    {
        this.integration = integration;
    }

    public Long getIntegration() 
    {
        return integration;
    }
    public void setGrowth(Long growth) 
    {
        this.growth = growth;
    }

    public Long getGrowth() 
    {
        return growth;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setAuthStatus(Long authStatus) 
    {
        this.authStatus = authStatus;
    }

    public Long getAuthStatus() 
    {
        return authStatus;
    }
    public void setLastLoginTime(Date lastLoginTime) 
    {
        this.lastLoginTime = lastLoginTime;
    }

    public Date getLastLoginTime() 
    {
        return lastLoginTime;
    }
    public void setAccountPermission(Integer accountPermission) 
    {
        this.accountPermission = accountPermission;
    }

    public Integer getAccountPermission() 
    {
        return accountPermission;
    }
    public void setSessionKey(String sessionKey) 
    {
        this.sessionKey = sessionKey;
    }

    public String getSessionKey() 
    {
        return sessionKey;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setAuthTime(Date authTime) 
    {
        this.authTime = authTime;
    }

    public Date getAuthTime() 
    {
        return authTime;
    }
    public void setAuthCredential(String authCredential) 
    {
        this.authCredential = authCredential;
    }

    public String getAuthCredential() 
    {
        return authCredential;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberLevelId", getMemberLevelId())
            .append("openInfoId", getOpenInfoId())
            .append("unionId", getUnionId())
            .append("phone", getPhone())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("nickname", getNickname())
            .append("headerImage", getHeaderImage())
            .append("gender", getGender())
            .append("birth", getBirth())
            .append("city", getCity())
            .append("job", getJob())
            .append("sign", getSign())
            .append("integration", getIntegration())
            .append("growth", getGrowth())
            .append("status", getStatus())
            .append("type", getType())
            .append("authStatus", getAuthStatus())
            .append("lastLoginTime", getLastLoginTime())
            .append("accountPermission", getAccountPermission())
            .append("remark", getRemark())
            .append("sessionKey", getSessionKey())
            .append("email", getEmail())
            .append("authTime", getAuthTime())
            .append("authCredential", getAuthCredential())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
