package com.qing.member.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 ums_member_login_log
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class MemberLoginLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户登录日志表主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long memberId;

    /** ip地址 */
    @Excel(name = "ip地址")
    private String ip;

    /** 城市 */
    @Excel(name = "城市")
    private String city;

    /** 登录类型 */
    @Excel(name = "登录类型")
    private Integer loginType;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public String getIp() 
    {
        return ip;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setLoginType(Integer loginType) 
    {
        this.loginType = loginType;
    }

    public Integer getLoginType() 
    {
        return loginType;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("ip", getIp())
            .append("city", getCity())
            .append("loginType", getLoginType())
            .append("remark", getRemark())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
