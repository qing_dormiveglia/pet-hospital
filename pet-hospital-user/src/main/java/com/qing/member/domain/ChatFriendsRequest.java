package com.qing.member.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 发送事件对象 ums_chat_friends_request
 * 
 * @author zzww
 * @date 2024-02-21
 */
public class ChatFriendsRequest extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 发送用户id */
    @Excel(name = "发送用户id")
    private Long sendMemberId;

    /** 接收用户id */
    @Excel(name = "接收用户id")
    private Long acceptMemberId;

    /** 发送请求的事件 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发送请求的事件", width = 30, dateFormat = "yyyy-MM-dd")
    private Date requestDateTime;

    /** 逻辑删除 */
    @Excel(name = "逻辑删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSendMemberId(Long sendMemberId) 
    {
        this.sendMemberId = sendMemberId;
    }

    public Long getSendMemberId() 
    {
        return sendMemberId;
    }
    public void setAcceptMemberId(Long acceptMemberId) 
    {
        this.acceptMemberId = acceptMemberId;
    }

    public Long getAcceptMemberId() 
    {
        return acceptMemberId;
    }
    public void setRequestDateTime(Date requestDateTime) 
    {
        this.requestDateTime = requestDateTime;
    }

    public Date getRequestDateTime() 
    {
        return requestDateTime;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sendMemberId", getSendMemberId())
            .append("acceptMemberId", getAcceptMemberId())
            .append("requestDateTime", getRequestDateTime())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
