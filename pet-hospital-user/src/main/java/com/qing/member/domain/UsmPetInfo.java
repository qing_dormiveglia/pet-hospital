package com.qing.member.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 usm_pet_info
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class UsmPetInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 宠物基本信息表主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long memberId;

    /** 宠物种类id */
    @Excel(name = "宠物种类id")
    private Long petKindId;

    /** 治疗类型表id */
    @Excel(name = "治疗类型表id")
    private Long petCureId;

    /** 宠物标签ids(通过，相隔) */
    @Excel(name = "宠物标签ids(通过，相隔)")
    private String petLabelIds;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 头像 */
    @Excel(name = "头像")
    private String headerImage;

    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;

    /** 疫苗接种状态 */
    @Excel(name = "疫苗接种状态")
    private Integer vaccine;

    /** 成年状态 */
    @Excel(name = "成年状态")
    private Integer birthType;

    /** 故事 */
    @Excel(name = "故事")
    private String story;

    /** 病情描述 */
    @Excel(name = "病情描述")
    private String disease;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setPetKindId(Long petKindId) 
    {
        this.petKindId = petKindId;
    }

    public Long getPetKindId() 
    {
        return petKindId;
    }
    public void setPetCureId(Long petCureId) 
    {
        this.petCureId = petCureId;
    }

    public Long getPetCureId() 
    {
        return petCureId;
    }
    public void setPetLabelIds(String petLabelIds) 
    {
        this.petLabelIds = petLabelIds;
    }

    public String getPetLabelIds() 
    {
        return petLabelIds;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setHeaderImage(String headerImage) 
    {
        this.headerImage = headerImage;
    }

    public String getHeaderImage() 
    {
        return headerImage;
    }
    public void setGender(Integer gender) 
    {
        this.gender = gender;
    }

    public Integer getGender() 
    {
        return gender;
    }
    public void setVaccine(Integer vaccine) 
    {
        this.vaccine = vaccine;
    }

    public Integer getVaccine() 
    {
        return vaccine;
    }
    public void setBirthType(Integer birthType) 
    {
        this.birthType = birthType;
    }

    public Integer getBirthType() 
    {
        return birthType;
    }
    public void setStory(String story) 
    {
        this.story = story;
    }

    public String getStory() 
    {
        return story;
    }
    public void setDisease(String disease) 
    {
        this.disease = disease;
    }

    public String getDisease() 
    {
        return disease;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("petKindId", getPetKindId())
            .append("petCureId", getPetCureId())
            .append("petLabelIds", getPetLabelIds())
            .append("name", getName())
            .append("headerImage", getHeaderImage())
            .append("gender", getGender())
            .append("vaccine", getVaccine())
            .append("birthType", getBirthType())
            .append("story", getStory())
            .append("disease", getDisease())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
