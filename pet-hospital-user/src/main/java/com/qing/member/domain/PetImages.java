package com.qing.member.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 ums_pet_images
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class PetImages extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 宠物图集表主键 */
    private Long id;

    /** 宠物id */
    @Excel(name = "宠物id")
    private Long petInfoId;

    /** 图片名称 */
    @Excel(name = "图片名称")
    private String name;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String image;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPetInfoId(Long petInfoId) 
    {
        this.petInfoId = petInfoId;
    }

    public Long getPetInfoId() 
    {
        return petInfoId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("petInfoId", getPetInfoId())
            .append("name", getName())
            .append("image", getImage())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
