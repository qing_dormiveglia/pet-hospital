package com.qing.member.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 ums_member_image_auth
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class MemberImageAuth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户认证图集表主键 */
    private Long id;

    /** 用户详情表id */
    @Excel(name = "用户详情表id")
    private Long memberInfoId;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String url;

    /** 图片类型 */
    @Excel(name = "图片类型")
    private Integer type;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMemberInfoId(Long memberInfoId) 
    {
        this.memberInfoId = memberInfoId;
    }

    public Long getMemberInfoId() 
    {
        return memberInfoId;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberInfoId", getMemberInfoId())
            .append("url", getUrl())
            .append("type", getType())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
