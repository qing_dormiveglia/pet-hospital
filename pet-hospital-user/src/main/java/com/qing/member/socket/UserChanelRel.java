package com.qing.member.socket;


import javax.websocket.Session;
import java.util.HashMap;

/**
 * 用户id 与通道 关系处理类
 */
public class UserChanelRel
{
    /**
     * 使用hashmap关系映射的 容器 key：用户id（发送者id） value ：用户通道
     */
    private static HashMap<Long, Session> manage = new HashMap<>();


    /**
     * 向容器中添加 映射关系
     * @param senderId 用户id（发送者id）
     * @param session 建立连接的通道
     */
    public static  void put(Long senderId,Session session){
        manage.put(senderId,session);
    }

    /**
     * 通过 用户id 向容器中获取 该用户通道
     * @param senderId 用户id
     * @return
     */
    public static Session get(Long senderId){
        return manage.get(senderId);
    }

    /**
     * 通过 用户id 向容器中获取 该用户通道
     * @param senderId 用户id
     * @return
     */
    public static Session remove(Long senderId){
        return manage.remove(senderId);
    }

}
