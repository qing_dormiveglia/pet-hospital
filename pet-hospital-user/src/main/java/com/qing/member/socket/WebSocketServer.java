package com.qing.member.socket;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.qing.common.constant.TokenEnum;
import com.qing.common.enums.MsgActionEnum;
import com.qing.common.enums.MsgSignFlagEnum;
import com.qing.common.exception.customizationException.FailException;
import com.qing.common.utils.StringUtil;
import com.qing.common.utils.WebUtils;
import com.qing.common.utils.jwt.JwtUtils;
import com.qing.member.config.WebSocketConfig;
import com.qing.member.domain.ChatMsg;
import com.qing.member.encoder.websocket.WebSocketDecoder;
import com.qing.member.encoder.websocket.WebSocketEncoder;
import com.qing.member.service.impl.ChatMsgServiceImpl;
import com.qing.member.vo.websocket.ChatMesVo;
import com.qing.member.vo.websocket.WebSocketMessageVO;
import com.qing.member.vo.websocket.WebSocketVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.websocket.*;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @Author zzww
 * @Date 2024/2/20 20:36
 */
@Component
@Slf4j
@ServerEndpoint(value = "/member/webSocketServer",decoders = {WebSocketDecoder.class},encoders = {WebSocketEncoder.class},configurator = WebSocketConfig.class)
public class WebSocketServer
{
    /**
     * 与客户端的连接会话，需要通过他来给客户端发消息
     */
    private Session session;

    /**
     * 当前用户ID
     */
    private Long memberId;

    //  这里使用静态，让 service 属于类
    private static ChatMsgServiceImpl chatMsgService;


    // 注入的时候，给类的 service 注入
    @Autowired
    public void setChatService(ChatMsgServiceImpl chatService) {
        WebSocketServer.chatMsgService = chatService;
    }


    /**
     * 记录当前在线连接数
     */
    private static AtomicInteger onlineCount = new AtomicInteger(0);


    /**
     *  concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
     *  虽然@Component默认是单例模式的，但springboot还是会为每个websocket连接初始化一个bean，所以可以用一个静态set保存起来。
     */
    private static CopyOnWriteArraySet<WebSocketServer> webSockets =new CopyOnWriteArraySet<>();

    /**
     *用来存在线连接用户信息
     */
    private static ConcurrentHashMap<String,Session> sessionPool = new ConcurrentHashMap<String,Session>();

    /**
     * 连接成功方法
     * @param session 连接会话
     */
    @OnOpen
    public void onOpen(Session session){
        try {
            String accessToken = (String) session.getUserProperties().get(TokenEnum.ACCESS_TOKEN.getValue());
            String username = JwtUtils.extractUsername(accessToken);
            Long memberIdByAccessToken = Long.valueOf(username);
            if (memberIdByAccessToken==null){
                throw new RuntimeException();
            }
            UserChanelRel.put(memberIdByAccessToken,session);
            this.session = session;
            this.memberId = memberIdByAccessToken;
//            webSockets.add(this);
//            sessionPool.put(userId, session);
            log.info("【websocket消息】 用户：" + memberIdByAccessToken + " 加入连接...");
        } catch (Exception e) {
            log.error("---------------WebSocket连接异常---------------");
        }
    }

    /**
     * 关闭连接
     */
    @OnClose
    public void onClose(){
        try {
            UserChanelRel.remove(this.memberId);
//            UserChanelRel.(userId,session);
//            webSockets.remove(this);
//            sessionPool.remove(this.userId);
            log.info("【websocket消息】 用户："+ this.memberId + " 断开连接...");
        } catch (Exception e) {
            log.error("---------------WebSocket断开异常---------------");
        }
    }

    @OnMessage
    public void onMessage(@PathParam("memberId") String memberId, WebSocketVO webSocketVO){
        try {
            //转为对象
//            WebSocketVO webSocketVO = JSONObject.parseObject(JSON.toJSONString(body), WebSocketVO.class);
            //获取用户动作
            Integer action = webSocketVO.getAction();
            WebSocketMessageVO webSocketMessageVo = webSocketVO.getWebSocketMessageVo();

            //发送消息
            if(action== MsgActionEnum.CHAT.type){
                //  保存消息到数据库，并且标记为未签收
                ChatMsg chatMsg = new ChatMsg();
                BeanUtils.copyProperties(webSocketMessageVo,chatMsg);
                chatMsg.setSignFlag(MsgSignFlagEnum.NOT_SING_FOR_ENUM.type);
                chatMsgService.insertChatMsg(chatMsg);
                //创建发送消息发送对象
                ChatMesVo chatMesVo = new ChatMesVo();
                BeanUtils.copyProperties(chatMsg,chatMesVo);
                //设置聊天内容
                Integer integer = sendOneMessage(chatMesVo);
                if (integer!=null){
                    chatMsg.setSignFlag(MsgSignFlagEnum.SING_FOR_ENUM.type);
                    chatMsgService.updateChatMsg(chatMsg);
                }
                //消息签收
            }else if (action==MsgActionEnum.SIGNED.type){
                String extand = webSocketVO.getExtand();
                List<Long> ids = StringUtil.tagSplit(extand);
                if (ids != null && !ids.isEmpty() && ids.size() > 0){
                    //批量签收
                    List<ChatMsg> list= chatMsgService.selectChatMsgBatchByMemberId(ids);
                    //向接收信息的用户签收消息，数据处理
                    List<ChatMesVo> collect = list.stream().map(item -> {
                        ChatMesVo chatMesVo = new ChatMesVo();
                        BeanUtils.copyProperties(item, chatMesVo);
                        return chatMesVo;
                    }).collect(Collectors.toList());
                    Integer integer = sendOneBatchMessage(collect);
                    if (integer != null){
                        //修改数据库状态
                        List<Long> longs = list.stream().map(item -> {
                            return item.getId();
                        }).collect(Collectors.toList());
                        chatMsgService.updateChatMsgBatchByIds(longs);
                    }
                }
            //客户端保持心跳
            }else if (action == MsgActionEnum.KEEPALIVE.type){

            }


        } catch (Exception e) {
            log.error("---------------WebSocket消息异常---------------");
        }
    }

    /**
     * 配置错误信息处理
     * @param session
     * @param t
     */
    @OnError
    public void onError(Session session, Throwable t) {
        //什么都不想打印都去掉就好了
        log.info("【websocket消息】出现未知错误 ");
        //打印错误信息，如果你不想打印错误信息，去掉就好了
        //这里打印的也是  java.io.EOFException: null
        t.printStackTrace();
    }


    /**
     * 此为广播消息
     * @param message
     */
    public void sendAllMessage(String message) {
        log.info("【websocket消息】广播消息:"+message);
        for(WebSocketServer webSocket : webSockets) {
            try {
                if(webSocket.session.isOpen()) {
                    webSocket.session.getAsyncRemote().sendText(message);
                }
            } catch (Exception e) {
                log.error("---------------WebSocket消息广播异常---------------");
            }
        }
    }

    /**
     * 单点消息
     */
    public Integer sendOneMessage(ChatMesVo chatMesVo) {
        Long receiverId = chatMesVo.getAcceptMemberId();
        Session session = UserChanelRel.get(receiverId);
        if (session != null&&session.isOpen()) {
            try {
                session.getAsyncRemote().sendText(JSON.toJSONString(chatMesVo));
            } catch (Exception e) {
                throw new FailException("消息发送失败");
            }
            return 1;
        }
        return null;
    }

    /**
     * 发送多条单点消息
     */
    public Integer sendOneBatchMessage(List<ChatMesVo> ChatMesVoList) {
        Session session = null;
        for (ChatMesVo chatMesVo:ChatMesVoList
             ) {
            if (chatMesVo.getAcceptMemberId() != null){
                session = UserChanelRel.get(chatMesVo.getAcceptMemberId());
                break;
            }
        }
        if (session != null && session.isOpen()) {
            try {
                session.getAsyncRemote().sendObject(ChatMesVoList);
            } catch (Exception e) {
                throw new FailException("WebSocket批量单点消息发送异常");
            }
            return ChatMesVoList.size();
        }
        return null;
    }

    /**
     * 发送多人单点消息
     * @param userIds
     * @param message
     */
    public void sendMoreMessage(String[] userIds, String message) {
        for(String userId:userIds) {
            Session session = sessionPool.get(userId);
            if (session != null&&session.isOpen()) {
                try {
                    log.info("【websocket消息】 单点消息:"+message);
                    session.getAsyncRemote().sendText(message);
                } catch (Exception e) {
                    log.error("---------------WebSocket多人单点消息发送异常---------------");
                }
            }
        }
    }
}
