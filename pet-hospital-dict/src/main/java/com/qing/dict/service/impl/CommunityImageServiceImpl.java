package com.qing.dict.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.qing.dict.vo.community.CommunityImageVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.CommunityImageMapper;
import com.qing.dict.domain.CommunityImage;
import com.qing.dict.service.ICommunityImageService;

import javax.annotation.Resource;

/**
 * 社区图集Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-19
 */
@Service
public class CommunityImageServiceImpl implements ICommunityImageService 
{
    @Resource
    private CommunityImageMapper communityImageMapper;

    /**
     * 查询社区图集
     * 
     * @param id 社区图集主键
     * @return 社区图集
     */
    @Override
    public CommunityImage selectCommunityImageById(Long id)
    {
        return communityImageMapper.selectCommunityImageById(id);
    }

    /**
     * 查询社区图集列表
     * 
     * @param communityImage 社区图集
     * @return 社区图集
     */
    @Override
    public List<CommunityImage> selectCommunityImageList(CommunityImage communityImage)
    {
        return communityImageMapper.selectCommunityImageList(communityImage);
    }

    /**
     * 新增社区图集
     * 
     * @param communityImage 社区图集
     * @return 结果
     */
    @Override
    public int insertCommunityImage(CommunityImage communityImage)
    {
        communityImage.setCreateTime(DateUtils.getNowDate());
        return communityImageMapper.insertCommunityImage(communityImage);
    }

    /**
     * 修改社区图集
     * 
     * @param communityImage 社区图集
     * @return 结果
     */
    @Override
    public int updateCommunityImage(CommunityImage communityImage)
    {
        communityImage.setUpdateTime(DateUtils.getNowDate());
        return communityImageMapper.updateCommunityImage(communityImage);
    }

    /**
     * 批量删除社区图集
     * 
     * @param ids 需要删除的社区图集主键
     * @return 结果
     */
    @Override
    public int deleteCommunityImageByIds(Long[] ids)
    {
        return communityImageMapper.deleteCommunityImageByIds(ids);
    }

    /**
     * 删除社区图集信息
     * 
     * @param id 社区图集主键
     * @return 结果
     */
    @Override
    public int deleteCommunityImageById(Long id)
    {
        return communityImageMapper.deleteCommunityImageById(id);
    }

    @Override
    public List<CommunityImageVo> findTopList() {
        CommunityImage communityImage = new CommunityImage();
        communityImage.setType(0);
        List<CommunityImage> communityImages = communityImageMapper.selectCommunityImageList(communityImage);
        List<CommunityImageVo> collect = communityImages.stream().map(item -> {
            CommunityImageVo communityImageVo = new CommunityImageVo();
            BeanUtils.copyProperties(item, communityImageVo);
            return communityImageVo;
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 批量获取图集信息
     * @param longs
     * @return
     */
    @Override
    public List<CommunityImageVo> selectCommunityImageBatchByIds(List<Long> longs) {
        if (longs.isEmpty()) {
            return null;
        }
        List<CommunityImageVo> list= communityImageMapper.selectCommunityImageBatchByIds(longs);
        return list;
    }


    @Override
    public int insertCommunityImageBatch(ArrayList<CommunityImage> communityImages) {

        return 0;
    }
}
