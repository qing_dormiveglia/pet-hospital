package com.qing.dict.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.qing.common.dubbo.MemberApi;
import com.qing.common.to.MemberTo;
import com.qing.common.utils.StringUtil;
import com.qing.common.utils.WebUtils;
import com.qing.dict.domain.Community;
import com.qing.dict.domain.CommunityImage;
import com.qing.dict.dto.community.CommentSaveDto;
import com.qing.dict.vo.community.CommentVo;
import com.qing.dict.vo.community.CommunityImageVo;
import com.ruoyi.common.utils.DateUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.CommunityCommentMapper;
import com.qing.dict.domain.CommunityComment;
import com.qing.dict.service.ICommunityCommentService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 社区评论Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-19
 */
@Service
public class CommunityCommentServiceImpl implements ICommunityCommentService 
{
    @Resource
    private CommunityCommentMapper communityCommentMapper;
    @Resource
    private CommunityServiceImpl communityService;
    @Resource
    private CommunityImageServiceImpl communityImageService;
    @DubboReference
    private MemberApi memberApi;
    /**
     * 查询社区评论
     * 
     * @param id 社区评论主键
     * @return 社区评论
     */
    @Override
    public CommunityComment selectCommunityCommentById(Long id)
    {
        return communityCommentMapper.selectCommunityCommentById(id);
    }

    /**
     * 查询社区评论列表
     * 
     * @param communityComment 社区评论
     * @return 社区评论
     */
    @Override
    public List<CommunityComment> selectCommunityCommentList(CommunityComment communityComment)
    {
        return communityCommentMapper.selectCommunityCommentList(communityComment);
    }

    /**
     * 新增社区评论
     * 
     * @param communityComment 社区评论
     * @return 结果
     */
    @Override
    public int insertCommunityComment(CommunityComment communityComment)
    {
        communityComment.setCreateTime(DateUtils.getNowDate());
        return communityCommentMapper.insertCommunityComment(communityComment);
    }

    /**
     * 修改社区评论
     * 
     * @param communityComment 社区评论
     * @return 结果
     */
    @Override
    public int updateCommunityComment(CommunityComment communityComment)
    {
        communityComment.setUpdateTime(DateUtils.getNowDate());
        return communityCommentMapper.updateCommunityComment(communityComment);
    }

    /**
     * 批量删除社区评论
     * 
     * @param ids 需要删除的社区评论主键
     * @return 结果
     */
    @Override
    public int deleteCommunityCommentByIds(Long[] ids)
    {
        return communityCommentMapper.deleteCommunityCommentByIds(ids);
    }

    /**
     * 删除社区评论信息
     * 
     * @param id 社区评论主键
     * @return 结果
     */
    @Override
    public int deleteCommunityCommentById(Long id)
    {
        return communityCommentMapper.deleteCommunityCommentById(id);
    }

    /**
     * 查询评论列表
     * @param id
     * @return
     */
    @Override
    public List<CommentVo> selectCommentVoList(Long id) {
        CommunityComment communityComment = new CommunityComment();
        communityComment.setCommunityId(id);
        //获取评论信息
        List<CommunityComment> communityComments = communityCommentMapper.selectCommunityCommentList(communityComment);
        //批量获取用户信息
        List<Long> ids = communityComments.stream()
                .map((Function<CommunityComment, Long>) CommunityComment::getMemberId)
                .collect(Collectors.toList());
        List<MemberTo> memberDuBboApi = memberApi.getMemberDuBboApi(ids);
        //用户信息
        List<CommentVo> collect = communityComments.stream().map(item -> {
            //封装用户信息
            CommentVo commentVo = new CommentVo();
            for (MemberTo member : memberDuBboApi
            ) {
                if (member.getId() .equals( item.getMemberId() )) {
                    commentVo.setMember(member);
                    break;
                }
            }
            //封装评论信息
            BeanUtils.copyProperties(item, commentVo);
            //封装评论用户信息
            Long commentId = item.getCommentId();
            if (commentId != 0L) {
                CommunityComment communityComment1 = communityCommentMapper.selectCommunityCommentById(commentId);
                commentVo.setCommunityCommentTo(communityComment1);
                Long memberId = communityComment1.getMemberId();
                MemberTo memberInfoDuBboApi = memberApi.getMemberInfoDuBboApi(memberId);
                commentVo.setCommentMember(memberInfoDuBboApi);
            } else {
                commentVo.setCommentMember(null);
            }
            //图集
            String images = item.getImages();
            List<Long> longs = StringUtil.tagSplit(images);
            List<CommunityImageVo> list = communityImageService.selectCommunityImageBatchByIds(longs);
            if (list!=null){
                List<String> s = list.stream().map((Function<CommunityImageVo, String>) CommunityImageVo::getUrl).collect(Collectors.toList());
                commentVo.setImages(s);
            }
            return commentVo;

        }).collect(Collectors.toList());

        List<CommentVo> topList = collect.stream().filter((item) -> {
            return item.getCommentId() == 0;
        }).collect(Collectors.toList());
        List<CommentVo> childrenList = collect.stream().filter((item) -> {
            return item.getCommentId() != 0;
        }).collect(Collectors.toList());
        List<CommentVo> list = topList.stream().map(item -> {
            ArrayList<CommentVo> commentVos = new ArrayList<>();
            for (CommentVo comment : childrenList
            ) {
                if (comment.getPId() .equals( item.getId())) {
                    commentVos.add(comment);
                }
            }
            item.setChildrenList(commentVos);
            return item;
        }).collect(Collectors.toList());
        return list;
    }

    /**
     * 添加评论
     * @param commentSaveDto
     * @return
     */
    @Transactional
    @Override
    public int saveCommunityComment(CommentSaveDto commentSaveDto) {
        Long memberId = WebUtils.getMemberIdByAccessToken();
        CommunityComment communityComment = new CommunityComment();
        BeanUtils.copyProperties(commentSaveDto,communityComment);
        communityComment.setMemberId(memberId);
        if (memberId.equals(commentSaveDto.getAuthorMemberId())){
            communityComment.setType(0);
        }else{
            communityComment.setType(1);
        }
        List<String> images = commentSaveDto.getImages();
        ArrayList<Long> longs = new ArrayList<>();
        for (int i = 0;i<images.size();i++){
            CommunityImage communityImage = new CommunityImage();
            communityImage.setName("评论图集");
            communityImage.setUrl(images.get(i));
            communityImage.setNum(Long.valueOf(i)+1);
            communityImage.setType(2);
            int image = communityImageService.insertCommunityImage(communityImage);
            if (image==0){
                throw new RuntimeException("失败");
            }
            longs.add(communityImage.getId());

        }
        String s = StringUtil.tagSplitList(longs);
        communityComment.setImages(s);
        int i = insertCommunityComment(communityComment);
        if (i==0){
            throw new RuntimeException("失败");
        }
        return i;
    }
}
