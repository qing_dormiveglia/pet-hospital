package com.qing.dict.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.PetMapMapper;
import com.qing.dict.domain.PetMap;
import com.qing.dict.service.IPetMapService;

/**
 * 地图Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class PetMapServiceImpl implements IPetMapService 
{
    @Autowired
    private PetMapMapper petMapMapper;

    /**
     * 查询地图
     * 
     * @param id 地图主键
     * @return 地图
     */
    @Override
    public PetMap selectPetMapById(Long id)
    {
        return petMapMapper.selectPetMapById(id);
    }

    /**
     * 查询地图列表
     * 
     * @param petMap 地图
     * @return 地图
     */
    @Override
    public List<PetMap> selectPetMapList(PetMap petMap)
    {
        return petMapMapper.selectPetMapList(petMap);
    }

    /**
     * 新增地图
     * 
     * @param petMap 地图
     * @return 结果
     */
    @Override
    public int insertPetMap(PetMap petMap)
    {
        petMap.setCreateTime(DateUtils.getNowDate());
        return petMapMapper.insertPetMap(petMap);
    }

    /**
     * 修改地图
     * 
     * @param petMap 地图
     * @return 结果
     */
    @Override
    public int updatePetMap(PetMap petMap)
    {
        petMap.setUpdateTime(DateUtils.getNowDate());
        return petMapMapper.updatePetMap(petMap);
    }

    /**
     * 批量删除地图
     * 
     * @param ids 需要删除的地图主键
     * @return 结果
     */
    @Override
    public int deletePetMapByIds(Long[] ids)
    {
        return petMapMapper.deletePetMapByIds(ids);
    }

    /**
     * 删除地图信息
     * 
     * @param id 地图主键
     * @return 结果
     */
    @Override
    public int deletePetMapById(Long id)
    {
        return petMapMapper.deletePetMapById(id);
    }
}
