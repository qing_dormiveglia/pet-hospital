package com.qing.dict.service;

import java.util.List;
import com.qing.dict.domain.CommunityTag;
import com.qing.dict.vo.community.CommunityTagVo;

/**
 * 社区标签Service接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface ICommunityTagService 
{
    /**
     * 查询社区标签
     * 
     * @param id 社区标签主键
     * @return 社区标签
     */
    public CommunityTag selectCommunityTagById(Long id);

    /**
     * 查询社区标签列表
     * 
     * @param communityTag 社区标签
     * @return 社区标签集合
     */
    public List<CommunityTag> selectCommunityTagList(CommunityTag communityTag);

    /**
     * 新增社区标签
     * 
     * @param communityTag 社区标签
     * @return 结果
     */
    public int insertCommunityTag(CommunityTag communityTag);

    /**
     * 修改社区标签
     * 
     * @param communityTag 社区标签
     * @return 结果
     */
    public int updateCommunityTag(CommunityTag communityTag);

    /**
     * 批量删除社区标签
     * 
     * @param ids 需要删除的社区标签主键集合
     * @return 结果
     */
    public int deleteCommunityTagByIds(Long[] ids);

    /**
     * 删除社区标签信息
     * 
     * @param id 社区标签主键
     * @return 结果
     */
    public int deleteCommunityTagById(Long id);

    /**
     * 批量获取标签信息
     * @param longs1
     * @return
     */
    List<CommunityTagVo> selectCommunityTagBatchByIds(List<Long> longs1);
}
