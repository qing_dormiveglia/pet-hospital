package com.qing.dict.service.impl;

import java.util.List;

import com.qing.dict.vo.community.CommunityTagVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.CommunityTagMapper;
import com.qing.dict.domain.CommunityTag;
import com.qing.dict.service.ICommunityTagService;

import javax.annotation.Resource;

/**
 * 社区标签Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-19
 */
@Service
public class CommunityTagServiceImpl implements ICommunityTagService 
{
    @Resource
    private CommunityTagMapper communityTagMapper;

    /**
     * 查询社区标签
     * 
     * @param id 社区标签主键
     * @return 社区标签
     */
    @Override
    public CommunityTag selectCommunityTagById(Long id)
    {
        return communityTagMapper.selectCommunityTagById(id);
    }

    /**
     * 查询社区标签列表
     * 
     * @param communityTag 社区标签
     * @return 社区标签
     */
    @Override
    public List<CommunityTag> selectCommunityTagList(CommunityTag communityTag)
    {
        return communityTagMapper.selectCommunityTagList(communityTag);
    }

    /**
     * 新增社区标签
     * 
     * @param communityTag 社区标签
     * @return 结果
     */
    @Override
    public int insertCommunityTag(CommunityTag communityTag)
    {
        communityTag.setCreateTime(DateUtils.getNowDate());
        return communityTagMapper.insertCommunityTag(communityTag);
    }

    /**
     * 修改社区标签
     * 
     * @param communityTag 社区标签
     * @return 结果
     */
    @Override
    public int updateCommunityTag(CommunityTag communityTag)
    {
        communityTag.setUpdateTime(DateUtils.getNowDate());
        return communityTagMapper.updateCommunityTag(communityTag);
    }

    /**
     * 批量删除社区标签
     * 
     * @param ids 需要删除的社区标签主键
     * @return 结果
     */
    @Override
    public int deleteCommunityTagByIds(Long[] ids)
    {
        return communityTagMapper.deleteCommunityTagByIds(ids);
    }

    /**
     * 删除社区标签信息
     * 
     * @param id 社区标签主键
     * @return 结果
     */
    @Override
    public int deleteCommunityTagById(Long id)
    {
        return communityTagMapper.deleteCommunityTagById(id);
    }

    /**
     * 批量获取标签信息
     * @param longs1
     * @return
     */
    @Override
    public List<CommunityTagVo> selectCommunityTagBatchByIds(List<Long> longs1) {
        if (longs1.isEmpty()) {
            return null;
        }
        List<CommunityTagVo> list= communityTagMapper.selectCommunityTagBatchByIds(longs1);
        return list;
    }
}
