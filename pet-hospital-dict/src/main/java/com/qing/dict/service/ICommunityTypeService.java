package com.qing.dict.service;

import java.util.List;
import com.qing.dict.domain.CommunityType;
import com.qing.dict.vo.community.CommunityTypeVo;

/**
 * 社区类型Service接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface ICommunityTypeService 
{
    /**
     * 查询社区类型
     * 
     * @param id 社区类型主键
     * @return 社区类型
     */
    public CommunityType selectCommunityTypeById(Long id);

    /**
     * 查询社区类型列表
     * 
     * @param communityType 社区类型
     * @return 社区类型集合
     */
    public List<CommunityType> selectCommunityTypeList(CommunityType communityType);

    /**
     * 新增社区类型
     * 
     * @param communityType 社区类型
     * @return 结果
     */
    public int insertCommunityType(CommunityType communityType);

    /**
     * 修改社区类型
     * 
     * @param communityType 社区类型
     * @return 结果
     */
    public int updateCommunityType(CommunityType communityType);

    /**
     * 批量删除社区类型
     * 
     * @param ids 需要删除的社区类型主键集合
     * @return 结果
     */
    public int deleteCommunityTypeByIds(Long[] ids);

    /**
     * 删除社区类型信息
     * 
     * @param id 社区类型主键
     * @return 结果
     */
    public int deleteCommunityTypeById(Long id);

    /**
     * 获取社区类型列表
     * @return
     */
    List<CommunityTypeVo> selectCommunityTypeVoList();
}
