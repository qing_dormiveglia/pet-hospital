package com.qing.dict.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.qing.common.dubbo.HospitalApi;
import com.qing.common.enums.NoticeTypeEnum;
import com.qing.common.to.HospitalTo;
import com.qing.dict.vo.notice.NoticeInfoVo;
import com.qing.dict.vo.notice.NoticeVo;
import com.ruoyi.common.utils.DateUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.NoticeMapper;
import com.qing.dict.domain.Notice;
import com.qing.dict.service.INoticeService;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class NoticeServiceImpl implements INoticeService 
{
    @Resource
    private NoticeMapper noticeMapper;
    @DubboReference
    private HospitalApi hospitalApi;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Notice selectNoticeById(Long id)
    {
        return noticeMapper.selectNoticeById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param notice 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Notice> selectNoticeList(Notice notice)
    {
        return noticeMapper.selectNoticeList(notice);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param notice 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertNotice(Notice notice)
    {
        notice.setCreateTime(DateUtils.getNowDate());
        return noticeMapper.insertNotice(notice);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param notice 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateNotice(Notice notice)
    {
        notice.setUpdateTime(DateUtils.getNowDate());
        return noticeMapper.updateNotice(notice);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(Long[] ids)
    {
        return noticeMapper.deleteNoticeByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteNoticeById(Long id)
    {
        return noticeMapper.deleteNoticeById(id);
    }

    /**
     * 获取公告信息（简介）（不包括详情内容）（分页）
     * @return
     */
    @Override
    public List<NoticeVo> findNoticeList() {
        List<Notice> notices = noticeMapper.selectNoticeList(new Notice());
        List<NoticeVo> list = notices.stream().map(item -> {
            NoticeVo noticeVo = new NoticeVo();
            BeanUtils.copyProperties(item, noticeVo);
            return noticeVo;
        }).collect(Collectors.toList());
        return list;
    }

    /**
     * 查询公告详情
     * @param id
     * @return
     */
    @Override
    public NoticeInfoVo selectNoticeInfoById(Long id) {
        //获取公告基本信息
        Notice notice = noticeMapper.selectNoticeById(id);
        //获取发起人基本信息
        if (NoticeTypeEnum.HOSPITAL_ENUM.getCode().equals(notice.getType())) {
            //获取医院信息
            NoticeInfoVo<HospitalTo> hospitalToNoticeInfoVo = new NoticeInfoVo<>();
            HospitalTo hospitalDuBboApi = hospitalApi.getHospitalDuBboApi(notice.getpId());
            hospitalToNoticeInfoVo.setT(hospitalDuBboApi);
            BeanUtils.copyProperties(notice,hospitalToNoticeInfoVo);
            return hospitalToNoticeInfoVo;
        }else{
            NoticeInfoVo<String> hospitalToNoticeInfoVo = new NoticeInfoVo<>();
            BeanUtils.copyProperties(notice,hospitalToNoticeInfoVo);
            return hospitalToNoticeInfoVo;
        }
    }
}
