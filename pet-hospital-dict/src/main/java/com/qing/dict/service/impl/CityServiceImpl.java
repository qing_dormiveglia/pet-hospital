package com.qing.dict.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.qing.dict.vo.city.CityTreeListVo;
import com.qing.dict.vo.city.CityTreeVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.CityMapper;
import com.qing.dict.domain.City;
import com.qing.dict.service.ICityService;

import javax.annotation.Resource;

/**
 * 城市Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class CityServiceImpl implements ICityService 
{
    @Resource
    private CityMapper cityMapper;

    /**
     * 查询城市
     * 
     * @param id 城市主键
     * @return 城市
     */
    @Override
    public City selectCityById(Long id)
    {
        return cityMapper.selectCityById(id);
    }

    /**
     * 查询城市列表
     * 
     * @param city 城市
     * @return 城市
     */
    @Override
    public List<City> selectCityList(City city)
    {
        return cityMapper.selectCityList(city);
    }

    /**
     * 新增城市
     * 
     * @param city 城市
     * @return 结果
     */
    @Override
    public int insertCity(City city)
    {
        city.setCreateTime(DateUtils.getNowDate());
        return cityMapper.insertCity(city);
    }

    /**
     * 修改城市
     * 
     * @param city 城市
     * @return 结果
     */
    @Override
    public int updateCity(City city)
    {
        city.setUpdateTime(DateUtils.getNowDate());
        return cityMapper.updateCity(city);
    }

    /**
     * 批量删除城市
     * 
     * @param ids 需要删除的城市主键
     * @return 结果
     */
    @Override
    public int deleteCityByIds(Long[] ids)
    {
        return cityMapper.deleteCityByIds(ids);
    }

    /**
     * 删除城市信息
     * 
     * @param id 城市主键
     * @return 结果
     */
    @Override
    public int deleteCityById(Long id)
    {
        return cityMapper.deleteCityById(id);
    }

    @Override
    public List<CityTreeVo> selectCityTreeList() {
        List<City> cities = cityMapper.selectCityList(new City());
        List<City> collect = cities.stream().filter(item->{
            return item.getpId() == 0;
        }).collect(Collectors.toList());
        List<CityTreeVo> CityTreeVoList = collect.stream().map(item -> {
            CityTreeVo cityTreeVo = new CityTreeVo();
            ArrayList<CityTreeListVo> cityList = new ArrayList<>();
            for (City city:cities
                 ) {
                if (city.getpId() == item.getId()) {
                    CityTreeListVo cityTreeListVo = new CityTreeListVo();
                    BeanUtils.copyProperties(city, cityTreeListVo);
                    cityList.add(cityTreeListVo);
                }
            }
            cityTreeVo.setLetter(item.getLetter());
            cityTreeVo.setCityList(cityList);
            return cityTreeVo;
        }).collect(Collectors.toList());
        return CityTreeVoList;
    }
}
