package com.qing.dict.service;

import java.util.List;
import com.qing.dict.domain.Community;
import com.qing.dict.dto.community.CommunityDto;
import com.qing.dict.dto.community.CommunitySaveDto;
import com.qing.dict.dto.community.CommunityUpdateDto;
import com.qing.dict.vo.community.CommunityVo;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 帖子Service接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface ICommunityService 
{
    /**
     * 查询帖子
     * 
     * @param id 帖子主键
     * @return 帖子
     */
    public Community selectCommunityById(Long id);

    /**
     * 查询帖子列表
     * 
     * @param community 帖子
     * @return 帖子集合
     */
    public List<Community> selectCommunityList(Community community);

    /**
     * 新增帖子
     * 
     * @param community 帖子
     * @return 结果
     */
    public int insertCommunity(Community community);

    /**
     * 修改帖子
     * 
     * @param community 帖子
     * @return 结果
     */
    public int updateCommunity(Community community);

    /**
     * 批量删除帖子
     * 
     * @param ids 需要删除的帖子主键集合
     * @return 结果
     */
    public int deleteCommunityByIds(Long[] ids);

    /**
     * 删除帖子信息
     * 
     * @param id 帖子主键
     * @return 结果
     */
    public int deleteCommunityById(Long id);

    /**
     * 获取帖子列表
     * @param communityDto
     * @return
     */
    TableDataInfo findCommunityVoList(CommunityDto communityDto);

    /**
     * 帖子详情接口
     * @param id
     * @return
     */
    CommunityVo selectCommunityVoById(Long id);

    /**
     * 新增帖子
     * @param communitySaveDto
     * @return
     */
    int saveCommunity(CommunitySaveDto communitySaveDto);

    /**
     * 查看我的帖子列表
     * @return
     */
    TableDataInfo findCommunityVoMyList();

    /**
     * 删除帖子
     * @param id
     * @return
     */
    int deleteById(Long id);

    /**
     * 修改帖子
     * @param parseObject
     * @return
     */
    int updateCommunityDto(CommunityUpdateDto parseObject);
}
