package com.qing.dict.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.DictMapper;
import com.qing.dict.domain.Dict;
import com.qing.dict.service.IDictService;

/**
 * 组织架构Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-08
 */
@Service
public class DictServiceImpl implements IDictService 
{
    @Autowired
    private DictMapper dictMapper;

    /**
     * 查询组织架构
     * 
     * @param id 组织架构主键
     * @return 组织架构
     */
    @Override
    public Dict selectDictById(Long id)
    {
        return dictMapper.selectDictById(id);
    }

    /**
     * 查询组织架构列表
     * 
     * @param dict 组织架构
     * @return 组织架构
     */
    @Override
    public List<Dict> selectDictList(Dict dict)
    {
        return dictMapper.selectDictList(dict);
    }

    /**
     * 新增组织架构
     * 
     * @param dict 组织架构
     * @return 结果
     */
    @Override
    public int insertDict(Dict dict)
    {
        dict.setCreateTime(DateUtils.getNowDate());
        return dictMapper.insertDict(dict);
    }

    /**
     * 修改组织架构
     * 
     * @param dict 组织架构
     * @return 结果
     */
    @Override
    public int updateDict(Dict dict)
    {
        dict.setUpdateTime(DateUtils.getNowDate());
        return dictMapper.updateDict(dict);
    }

    /**
     * 批量删除组织架构
     * 
     * @param ids 需要删除的组织架构主键
     * @return 结果
     */
    @Override
    public int deleteDictByIds(Long[] ids)
    {
        return dictMapper.deleteDictByIds(ids);
    }

    /**
     * 删除组织架构信息
     * 
     * @param id 组织架构主键
     * @return 结果
     */
    @Override
    public int deleteDictById(Long id)
    {
        return dictMapper.deleteDictById(id);
    }
}
