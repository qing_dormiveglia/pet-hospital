package com.qing.dict.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.github.pagehelper.PageInfo;
import com.qing.common.dubbo.MemberApi;
import com.qing.common.to.MemberTo;
import com.qing.common.utils.StringUtil;
import com.qing.common.utils.WebUtils;
import com.qing.dict.domain.CommunityImage;
import com.qing.dict.domain.CommunityType;
import com.qing.dict.dto.community.CommunityDto;
import com.qing.dict.dto.community.CommunitySaveDto;
import com.qing.dict.dto.community.CommunityUpdateDto;
import com.qing.dict.vo.community.CommunityImageVo;
import com.qing.dict.vo.community.CommunityTagVo;
import com.qing.dict.vo.community.CommunityVo;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.DateUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.CommunityMapper;
import com.qing.dict.domain.Community;
import com.qing.dict.service.ICommunityService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * 帖子Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-19
 */
@Service
public class CommunityServiceImpl implements ICommunityService 
{
    @Resource
    private CommunityMapper communityMapper;
    @Resource
    private CommunityImageServiceImpl communityImageService;
    @Resource
    private CommunityTagServiceImpl communityTagService;
    @DubboReference
    private MemberApi memberApi;
    /**
     * 查询帖子
     * 
     * @param id 帖子主键
     * @return 帖子
     */
    @Override
    public Community selectCommunityById(Long id)
    {
        return communityMapper.selectCommunityById(id);
    }

    /**
     * 查询帖子列表
     * 
     * @param community 帖子
     * @return 帖子
     */
    @Override
    public List<Community> selectCommunityList(Community community)
    {
        return communityMapper.selectCommunityList(community);
    }

    /**
     * 新增帖子
     * 
     * @param community 帖子
     * @return 结果
     */
    @Override
    public int insertCommunity(Community community)
    {
        community.setCreateTime(DateUtils.getNowDate());
        return communityMapper.insertCommunity(community);
    }

    /**
     * 修改帖子
     * 
     * @param community 帖子
     * @return 结果
     */
    @Override
    public int updateCommunity(Community community)
    {
        community.setUpdateTime(DateUtils.getNowDate());
        return communityMapper.updateCommunity(community);
    }

    /**
     * 批量删除帖子
     * 
     * @param ids 需要删除的帖子主键
     * @return 结果
     */
    @Override
    public int deleteCommunityByIds(Long[] ids)
    {
        return communityMapper.deleteCommunityByIds(ids);
    }

    /**
     * 删除帖子信息
     * 
     * @param id 帖子主键
     * @return 结果
     */
    @Override
    public int deleteCommunityById(Long id)
    {
        return communityMapper.deleteCommunityById(id);
    }

    /**
     * 获取帖子列表
     * @param communityDto
     * @return
     */
    @Override
    public TableDataInfo findCommunityVoList(CommunityDto communityDto) {
        Community community = new Community();
        if (!ObjectUtils.isEmpty(communityDto)){
            Long typeId = communityDto.getTypeId();
            community.setCommunityTypeId(typeId);
        }
        //获取帖子信息
        List<Community> communities = communityMapper.selectCommunityList(community);
        //获取用户信息
        List<Long> ids = communities.stream().map(item -> {
            return item.getMemberId();
        }).collect(Collectors.toList());
        List<MemberTo> memberDuBboApi = memberApi.getMemberDuBboApi(ids);
        List<CommunityVo> communityVoList = communities.stream().map(item -> {
            CommunityVo communityVo = new CommunityVo();
            BeanUtils.copyProperties(item,communityVo);
            //获取图集信息
            String imageIds = item.getImageIds();
            List<Long> longs = StringUtil.tagSplit(imageIds);
            List<CommunityImageVo> list = communityImageService.selectCommunityImageBatchByIds(longs);
            List<String> collect = list.stream().map(img -> {
                return img.getUrl();
            }).collect(Collectors.toList());
            communityVo.setImageVos(collect);
            //获取标签信息
            String tagIds = item.getTagIds();
            List<Long> longs1 = StringUtil.tagSplit(tagIds);
            List<CommunityTagVo> tagVoList = communityTagService.selectCommunityTagBatchByIds(longs1);
            communityVo.setTagVoList(tagVoList);

            //封装用户信息
            for (MemberTo memberTo: memberDuBboApi){
                if (memberTo.getId()==item.getMemberId()){
                    communityVo.setMember(memberTo);
                }
            }
            return communityVo;
        }).collect(Collectors.toList());
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(communityVoList);
        rspData.setTotal(new PageInfo(communities).getTotal());
        return rspData;
//        return communityVoList;
    }

    /**
     * 帖子详情接口
     * @param id
     * @return
     */
    @Override
    public CommunityVo selectCommunityVoById(Long id) {
        //帖子信息
        Community community = communityMapper.selectCommunityById(id);
        CommunityVo communityVo = new CommunityVo();
        BeanUtils.copyProperties(community,communityVo);
        //查询用户
        MemberTo memberInfoDuBboApi = memberApi.getMemberInfoDuBboApi(community.getMemberId());
        communityVo.setMember(memberInfoDuBboApi);
        //图集信息
        String imageIds = community.getImageIds();
        List<Long> longs = StringUtil.tagSplit(imageIds);
        List<CommunityImageVo> list = communityImageService.selectCommunityImageBatchByIds(longs);
        communityVo.setImageVoList(list);
        List<String> collect = list.stream().map(img -> {
            return img.getUrl();
        }).collect(Collectors.toList());
        communityVo.setImageVos(collect);
        //标签信息
        String tagIds = community.getTagIds();
        List<Long> longs1 = StringUtil.tagSplit(tagIds);
        List<CommunityTagVo> tagVoList = communityTagService.selectCommunityTagBatchByIds(longs1);
        communityVo.setTagVoList(tagVoList);

        return communityVo;
    }

    /**
     * 新增帖子
     * @param communitySaveDto
     * @return
     */
    @Transactional
    @Override
    public int saveCommunity(CommunitySaveDto communitySaveDto) {
        Long memberId = WebUtils.getMemberIdByAccessToken();
        Community community = new Community();
        community.setCommunityTypeId(communitySaveDto.getCommunityTypeId());
        community.setContent(communitySaveDto.getContent());
        community.setMemberId(memberId);
        List<String> images = communitySaveDto.getImages();
        ArrayList<Long> longs = new ArrayList<>();
        for (int i=0;i<images.size();i++){
            CommunityImage communityImage = new CommunityImage();
            communityImage.setType(1);
            communityImage.setNum(Long.valueOf(i)+1);
            communityImage.setUrl(images.get(i));
            communityImage.setName("论坛图");
            int image = communityImageService.insertCommunityImage(communityImage);
            if (image==0){
                throw new RuntimeException("失败");
            }
            longs.add(communityImage.getId());
        }
        String str = StringUtil.tagSplitList(longs);
        community.setImageIds(str);
        int i = insertCommunity(community);
        if (i==0){
            throw new RuntimeException("失败");
        }
        return i;
    }

    /**
     * 查看我的帖子列表
     * @return
     */
    @Override
    public TableDataInfo findCommunityVoMyList() {
        Long id = WebUtils.getMemberIdByAccessToken();
        Community community = new Community();
        community.setMemberId(id);
        //获取帖子信息
        List<Community> communities = communityMapper.selectCommunityList(community);

        List<CommunityVo> communityVoList = communities.stream().map(item -> {
            CommunityVo communityVo = new CommunityVo();
            BeanUtils.copyProperties(item,communityVo);
            //获取图集信息
            String imageIds = item.getImageIds();
            List<Long> longs = StringUtil.tagSplit(imageIds);
            List<CommunityImageVo> list = communityImageService.selectCommunityImageBatchByIds(longs);
            List<String> collect = list.stream().map(img -> {
                return img.getUrl();
            }).collect(Collectors.toList());
            communityVo.setImageVos(collect);
            //获取标签信息
            String tagIds = item.getTagIds();
            List<Long> longs1 = StringUtil.tagSplit(tagIds);
            List<CommunityTagVo> tagVoList = communityTagService.selectCommunityTagBatchByIds(longs1);
            communityVo.setTagVoList(tagVoList);

            return communityVo;
        }).collect(Collectors.toList());
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(communityVoList);
        rspData.setTotal(new PageInfo(communities).getTotal());
        return rspData;
//        return communityVoList;
    }

    /**
     * 删除帖子
     * @param id
     * @return
     */
    @Override
    public int deleteById(Long id) {
        Community community = selectCommunityById(id);
        String imageIds = community.getImageIds();
        List<Long> longs = StringUtil.tagSplit(imageIds);
        Long[] longArray = new Long[longs.size()];
        for (int i = 0; i < longs.size(); i++) {
            longArray[i] = longs.get(i);
        }
        int i = communityImageService.deleteCommunityImageByIds(longArray);
        int i1 = deleteCommunityById(id);
        if (i1==0){
            throw new RuntimeException("错误");
        }
        return i1;
    }

    /**
     * 修改帖子
     * @param parseObject
     * @return
     */
    @Transactional
    @Override
    public int updateCommunityDto(CommunityUpdateDto parseObject) {
        Long id = WebUtils.getMemberIdByAccessToken();
        Community community = new Community();
        BeanUtils.copyProperties(parseObject,community);
        community.setMemberId(id);
        String imageIds = parseObject.getImageIds();
        if (imageIds!=null){
            List<Long> split = StringUtil.tagSplit(imageIds);
            Long[] longArray = new Long[split.size()];
            for (int i = 0; i < split.size(); i++) {
                longArray[i] = split.get(i);
            }
            int i1 = communityImageService.deleteCommunityImageByIds(longArray);
        }
        List<String> images = parseObject.getImages();
        ArrayList<Long> longs = new ArrayList<>();
        for (int i=0;i<images.size();i++){
            CommunityImage communityImage = new CommunityImage();
            communityImage.setType(1);
            communityImage.setNum(Long.valueOf(i)+1);
            communityImage.setUrl(images.get(i));
            communityImage.setName("论坛图");
            int image = communityImageService.insertCommunityImage(communityImage);
            if (image==0){
                throw new RuntimeException("失败");
            }
            longs.add(communityImage.getId());
        }
        String s = StringUtil.tagSplitList(longs);
        community.setImageIds(s);
        int i = updateCommunity(community);
        if (i==0){
           throw new RuntimeException("失败");
        }
        return i;
    }
}
