package com.qing.dict.service;

import java.util.List;
import com.qing.dict.domain.Notice;
import com.qing.dict.vo.notice.NoticeInfoVo;
import com.qing.dict.vo.notice.NoticeVo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface INoticeService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Notice selectNoticeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param notice 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Notice> selectNoticeList(Notice notice);

    /**
     * 新增【请填写功能名称】
     * 
     * @param notice 【请填写功能名称】
     * @return 结果
     */
    public int insertNotice(Notice notice);

    /**
     * 修改【请填写功能名称】
     * 
     * @param notice 【请填写功能名称】
     * @return 结果
     */
    public int updateNotice(Notice notice);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteNoticeByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteNoticeById(Long id);

    /**
     * 获取公告信息（简介）（不包括详情内容）（分页）
     * @return
     */
    List<NoticeVo> findNoticeList();

    /**
     * 查询公告详情
     * @param id
     * @return
     */
    NoticeInfoVo selectNoticeInfoById(Long id);
}
