package com.qing.dict.service;

import java.util.ArrayList;
import java.util.List;
import com.qing.dict.domain.CommunityImage;
import com.qing.dict.vo.community.CommunityImageVo;

/**
 * 社区图集Service接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface ICommunityImageService 
{
    /**
     * 查询社区图集
     * 
     * @param id 社区图集主键
     * @return 社区图集
     */
    public CommunityImage selectCommunityImageById(Long id);

    /**
     * 查询社区图集列表
     * 
     * @param communityImage 社区图集
     * @return 社区图集集合
     */
    public List<CommunityImage> selectCommunityImageList(CommunityImage communityImage);

    /**
     * 新增社区图集
     * 
     * @param communityImage 社区图集
     * @return 结果
     */
    public int insertCommunityImage(CommunityImage communityImage);

    /**
     * 修改社区图集
     * 
     * @param communityImage 社区图集
     * @return 结果
     */
    public int updateCommunityImage(CommunityImage communityImage);

    /**
     * 批量删除社区图集
     * 
     * @param ids 需要删除的社区图集主键集合
     * @return 结果
     */
    public int deleteCommunityImageByIds(Long[] ids);

    /**
     * 删除社区图集信息
     * 
     * @param id 社区图集主键
     * @return 结果
     */
    public int deleteCommunityImageById(Long id);

    /**
     * 获取顶置广告（）
     * @return
     */
    List<CommunityImageVo> findTopList();

    /**
     * 批量获取图集信息根据
     * @param longs
     * @return
     */
    List<CommunityImageVo> selectCommunityImageBatchByIds(List<Long> longs);

    /**
     * 批量添加
     * @param communityImages
     * @return
     */
    int insertCommunityImageBatch(ArrayList<CommunityImage> communityImages);
}
