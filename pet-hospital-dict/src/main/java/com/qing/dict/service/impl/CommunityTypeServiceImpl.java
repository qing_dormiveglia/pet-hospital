package com.qing.dict.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.qing.dict.vo.community.CommunityTypeVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qing.dict.mapper.CommunityTypeMapper;
import com.qing.dict.domain.CommunityType;
import com.qing.dict.service.ICommunityTypeService;

import javax.annotation.Resource;

/**
 * 社区类型Service业务层处理
 * 
 * @author zzww
 * @date 2024-02-19
 */
@Service
public class CommunityTypeServiceImpl implements ICommunityTypeService 
{
    @Resource
    private CommunityTypeMapper communityTypeMapper;

    /**
     * 查询社区类型
     * 
     * @param id 社区类型主键
     * @return 社区类型
     */
    @Override
    public CommunityType selectCommunityTypeById(Long id)
    {
        return communityTypeMapper.selectCommunityTypeById(id);
    }

    /**
     * 查询社区类型列表
     * 
     * @param communityType 社区类型
     * @return 社区类型
     */
    @Override
    public List<CommunityType> selectCommunityTypeList(CommunityType communityType)
    {
        return communityTypeMapper.selectCommunityTypeList(communityType);
    }

    /**
     * 新增社区类型
     * 
     * @param communityType 社区类型
     * @return 结果
     */
    @Override
    public int insertCommunityType(CommunityType communityType)
    {
        communityType.setCreateTime(DateUtils.getNowDate());
        return communityTypeMapper.insertCommunityType(communityType);
    }

    /**
     * 修改社区类型
     * 
     * @param communityType 社区类型
     * @return 结果
     */
    @Override
    public int updateCommunityType(CommunityType communityType)
    {
        communityType.setUpdateTime(DateUtils.getNowDate());
        return communityTypeMapper.updateCommunityType(communityType);
    }

    /**
     * 批量删除社区类型
     * 
     * @param ids 需要删除的社区类型主键
     * @return 结果
     */
    @Override
    public int deleteCommunityTypeByIds(Long[] ids)
    {
        return communityTypeMapper.deleteCommunityTypeByIds(ids);
    }

    /**
     * 删除社区类型信息
     * 
     * @param id 社区类型主键
     * @return 结果
     */
    @Override
    public int deleteCommunityTypeById(Long id)
    {
        return communityTypeMapper.deleteCommunityTypeById(id);
    }

    @Override
    public List<CommunityTypeVo> selectCommunityTypeVoList() {
        List<CommunityType> communityTypeList = communityTypeMapper.selectCommunityTypeList(new CommunityType());
        List<CommunityTypeVo> collect = communityTypeList.stream().map(item -> {
            CommunityTypeVo communityTypeVo = new CommunityTypeVo();
            BeanUtils.copyProperties(item, communityTypeVo);
            return communityTypeVo;
        }).collect(Collectors.toList());
        CommunityTypeVo communityTypeVo = new CommunityTypeVo(0l, "全部");
        collect.add(0,communityTypeVo);
        return collect;
    }
}
