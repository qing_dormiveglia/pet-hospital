package com.qing.dict.service;

import java.util.List;
import com.qing.dict.domain.CommunityComment;
import com.qing.dict.dto.community.CommentSaveDto;
import com.qing.dict.vo.community.CommentVo;

/**
 * 社区评论Service接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface ICommunityCommentService 
{
    /**
     * 查询社区评论
     * 
     * @param id 社区评论主键
     * @return 社区评论
     */
    public CommunityComment selectCommunityCommentById(Long id);

    /**
     * 查询社区评论列表
     * 
     * @param communityComment 社区评论
     * @return 社区评论集合
     */
    public List<CommunityComment> selectCommunityCommentList(CommunityComment communityComment);

    /**
     * 新增社区评论
     * 
     * @param communityComment 社区评论
     * @return 结果
     */
    public int insertCommunityComment(CommunityComment communityComment);

    /**
     * 修改社区评论
     * 
     * @param communityComment 社区评论
     * @return 结果
     */
    public int updateCommunityComment(CommunityComment communityComment);

    /**
     * 批量删除社区评论
     * 
     * @param ids 需要删除的社区评论主键集合
     * @return 结果
     */
    public int deleteCommunityCommentByIds(Long[] ids);

    /**
     * 删除社区评论信息
     * 
     * @param id 社区评论主键
     * @return 结果
     */
    public int deleteCommunityCommentById(Long id);

    /**
     * 查询评论列表
     * @param id
     * @return
     */
    List<CommentVo> selectCommentVoList(Long id);

    /**
     * 添加评论
     * @param commentSaveDto
     * @return
     */
    int saveCommunityComment(CommentSaveDto commentSaveDto);

}
