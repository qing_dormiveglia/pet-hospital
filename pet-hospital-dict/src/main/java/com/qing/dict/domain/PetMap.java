package com.qing.dict.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 地图对象 dms_pet_map
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class PetMap extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 地图id */
    private Long id;

    /** 活动表id */
    @Excel(name = "活动表id")
    private Long pId;

    /** 地图类型（0-博物馆，1-文物，2-活动） */
    @Excel(name = "地图类型", readConverterExp = "0=-博物馆，1-文物，2-活动")
    private Integer mapType;

    /** 原始经度 */
    @Excel(name = "原始经度")
    private BigDecimal lon;

    /** 原始纬度 */
    @Excel(name = "原始纬度")
    private BigDecimal lat;

    /** 高德经度 */
    @Excel(name = "高德经度")
    private BigDecimal gdLon;

    /** 高德纬度 */
    @Excel(name = "高德纬度")
    private BigDecimal gdLat;

    /** 传入经度 */
    @Excel(name = "传入经度")
    private BigDecimal inputLon;

    /** 传入纬度 */
    @Excel(name = "传入纬度")
    private BigDecimal inputLat;

    /** 传入地图（0-高德，1-百度，2-谷歌） */
    @Excel(name = "传入地图", readConverterExp = "0=-高德，1-百度，2-谷歌")
    private Integer inputMap;

    /** 逻辑删除，0为未删除，1为删除 */
    @Excel(name = "逻辑删除，0为未删除，1为删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setpId(Long pId) 
    {
        this.pId = pId;
    }

    public Long getpId() 
    {
        return pId;
    }
    public void setMapType(Integer mapType) 
    {
        this.mapType = mapType;
    }

    public Integer getMapType() 
    {
        return mapType;
    }
    public void setLon(BigDecimal lon) 
    {
        this.lon = lon;
    }

    public BigDecimal getLon() 
    {
        return lon;
    }
    public void setLat(BigDecimal lat) 
    {
        this.lat = lat;
    }

    public BigDecimal getLat() 
    {
        return lat;
    }
    public void setGdLon(BigDecimal gdLon) 
    {
        this.gdLon = gdLon;
    }

    public BigDecimal getGdLon() 
    {
        return gdLon;
    }
    public void setGdLat(BigDecimal gdLat) 
    {
        this.gdLat = gdLat;
    }

    public BigDecimal getGdLat() 
    {
        return gdLat;
    }
    public void setInputLon(BigDecimal inputLon) 
    {
        this.inputLon = inputLon;
    }

    public BigDecimal getInputLon() 
    {
        return inputLon;
    }
    public void setInputLat(BigDecimal inputLat) 
    {
        this.inputLat = inputLat;
    }

    public BigDecimal getInputLat() 
    {
        return inputLat;
    }
    public void setInputMap(Integer inputMap) 
    {
        this.inputMap = inputMap;
    }

    public Integer getInputMap() 
    {
        return inputMap;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pId", getpId())
            .append("mapType", getMapType())
            .append("lon", getLon())
            .append("lat", getLat())
            .append("gdLon", getGdLon())
            .append("gdLat", getGdLat())
            .append("inputLon", getInputLon())
            .append("inputLat", getInputLat())
            .append("inputMap", getInputMap())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
