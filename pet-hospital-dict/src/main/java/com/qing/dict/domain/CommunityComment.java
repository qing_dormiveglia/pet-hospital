package com.qing.dict.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 社区评论对象 dms_community_comment
 * 
 * @author zzww
 * @date 2024-02-19
 */
public class CommunityComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 帖子id */
    @Excel(name = "帖子id")
    private Long communityId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long memberId;

    /** 评论id(用户id) */
    @Excel(name = "评论id")
    private Long commentId;

    /** 上级id */
    @Excel(name = "上级id")
    private Long pId;

    /** 图集id（逗号隔开） */
    @Excel(name = "图集id", readConverterExp = "逗=号隔开")
    private String images;

    /** 类型（0-作者） */
    @Excel(name = "类型", readConverterExp = "0=-作者")
    private Integer type;

    /** 点赞数 */
    @Excel(name = "点赞数")
    private Long upvote;

    /** 评论 */
    @Excel(name = "评论")
    private String coment;

    /** 逻辑删除 */
    @Excel(name = "逻辑删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCommunityId(Long communityId) 
    {
        this.communityId = communityId;
    }

    public Long getCommunityId() 
    {
        return communityId;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setCommentId(Long commentId) 
    {
        this.commentId = commentId;
    }

    public Long getCommentId() 
    {
        return commentId;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setUpvote(Long upvote) 
    {
        this.upvote = upvote;
    }

    public Long getUpvote() 
    {
        return upvote;
    }
    public void setComent(String coment) 
    {
        this.coment = coment;
    }

    public String getComent() 
    {
        return coment;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    public Long getPId() {
        return pId;
    }

    public void setPId(Long pId) {
        this.pId = pId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("communityId", getCommunityId())
            .append("memberId", getMemberId())
            .append("commentId", getCommentId())
            .append("pId", getPId())
            .append("images", getImages())
            .append("type", getType())
            .append("upvote", getUpvote())
            .append("coment", getComent())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
