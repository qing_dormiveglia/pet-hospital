package com.qing.dict.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 组织架构对象 dms_dict
 * 
 * @author zzww
 * @date 2024-02-08
 */
public class Dict extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 上级id */
    @Excel(name = "上级id")
    private Long parentId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 值 */
    @Excel(name = "值")
    private Long value;

    /** 编码 */
    @Excel(name = "编码")
    private String dictCode;

    /** 删除标记（0:不可用 1:可用） */
    @Excel(name = "删除标记", readConverterExp = "0=:不可用,1=:可用")
    private Long isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setValue(Long value) 
    {
        this.value = value;
    }

    public Long getValue() 
    {
        return value;
    }
    public void setDictCode(String dictCode) 
    {
        this.dictCode = dictCode;
    }

    public String getDictCode() 
    {
        return dictCode;
    }
    public void setIsDeleted(Long isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Long getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("name", getName())
            .append("value", getValue())
            .append("dictCode", getDictCode())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
