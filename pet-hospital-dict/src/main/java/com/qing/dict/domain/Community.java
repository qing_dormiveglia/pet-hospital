package com.qing.dict.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 帖子对象 dms_community
 * 
 * @author zzww
 * @date 2024-02-19
 */
public class Community extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long memberId;

    /** 类型id */
    @Excel(name = "类型id")
    private Long communityTypeId;

    /** 转发数 */
    @Excel(name = "转发数")
    private Long transpond;

    /** 评论数 */
    @Excel(name = "评论数")
    private Long comment;

    /** 浏览量 */
    @Excel(name = "浏览量")
    private Long browse;

    /** 点赞数 */
    @Excel(name = "点赞数")
    private Long upvote;

    /** 图集id（逗号隔开） */
    @Excel(name = "图集id", readConverterExp = "逗=号隔开")
    private String imageIds;

    /** 帖子信息 */
    @Excel(name = "帖子信息")
    private String content;

    /** 标签id（逗号隔开） */
    @Excel(name = "标签id", readConverterExp = "逗=号隔开")
    private String tagIds;

    /** 地址名称 */
    @Excel(name = "地址名称")
    private String mapAddress;

    /** 顶置 */
    @Excel(name = "顶置")
    private Integer topType;

    /** 逻辑删除 */
    @Excel(name = "逻辑删除")
    private Integer isDelete;

    public Integer getTopType() {
        return topType;
    }

    public void setTopType(Integer topType) {
        this.topType = topType;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setCommunityTypeId(Long communityTypeId) 
    {
        this.communityTypeId = communityTypeId;
    }

    public Long getCommunityTypeId() 
    {
        return communityTypeId;
    }
    public void setTranspond(Long transpond) 
    {
        this.transpond = transpond;
    }

    public Long getTranspond() 
    {
        return transpond;
    }
    public void setComment(Long comment) 
    {
        this.comment = comment;
    }

    public Long getComment() 
    {
        return comment;
    }
    public void setBrowse(Long browse) 
    {
        this.browse = browse;
    }

    public Long getBrowse() 
    {
        return browse;
    }
    public void setUpvote(Long upvote) 
    {
        this.upvote = upvote;
    }

    public Long getUpvote() 
    {
        return upvote;
    }
    public void setImageIds(String imageIds) 
    {
        this.imageIds = imageIds;
    }

    public String getImageIds() 
    {
        return imageIds;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setTagIds(String tagIds) 
    {
        this.tagIds = tagIds;
    }

    public String getTagIds() 
    {
        return tagIds;
    }
    public void setMapAddress(String mapAddress) 
    {
        this.mapAddress = mapAddress;
    }

    public String getMapAddress() 
    {
        return mapAddress;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("communityTypeId", getCommunityTypeId())
            .append("transpond", getTranspond())
            .append("comment", getComment())
            .append("browse", getBrowse())
            .append("upvote", getUpvote())
            .append("imageIds", getImageIds())
            .append("content", getContent())
            .append("tagIds", getTagIds())
            .append("mapAddress", getMapAddress())
            .append("topType", getTopType())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
