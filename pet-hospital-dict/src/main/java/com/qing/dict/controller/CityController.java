package com.qing.dict.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.qing.dict.vo.city.CityTreeVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.dict.domain.City;
import com.qing.dict.service.ICityService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 城市Controller
 * 
 * @author zzww
 * @date 2024-02-08
 */
@RestController
@RequestMapping("/dict/city")
public class CityController extends BaseController
{
    @Autowired
    private ICityService cityService;


    /**
     * 查询城市列表（树形）（全部）
     * pet/dict/city/list/tree
     */
    @GetMapping("/list/tree")
    public AjaxResult listTree()
    {
        List<CityTreeVo> list = cityService.selectCityTreeList();
        return AjaxResult.success(list);
    }


    /**
     * 查询城市列表
     */
    @PreAuthorize("@ss.hasPermi('dict:city:list')")
    @GetMapping("/list")
    public TableDataInfo list(City city)
    {
        startPage();
        List<City> list = cityService.selectCityList(city);
        return getDataTable(list);
    }

    /**
     * 导出城市列表
     */
    @PreAuthorize("@ss.hasPermi('dict:city:export')")
    @Log(title = "城市", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, City city)
    {
        List<City> list = cityService.selectCityList(city);
        ExcelUtil<City> util = new ExcelUtil<City>(City.class);
        util.exportExcel(response, list, "城市数据");
    }

    /**
     * 获取城市详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:city:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cityService.selectCityById(id));
    }

    /**
     * 新增城市
     */
    @PreAuthorize("@ss.hasPermi('dict:city:add')")
    @Log(title = "城市", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody City city)
    {
        return toAjax(cityService.insertCity(city));
    }

    /**
     * 修改城市
     */
    @PreAuthorize("@ss.hasPermi('dict:city:edit')")
    @Log(title = "城市", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody City city)
    {
        return toAjax(cityService.updateCity(city));
    }

    /**
     * 删除城市
     */
    @PreAuthorize("@ss.hasPermi('dict:city:remove')")
    @Log(title = "城市", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cityService.deleteCityByIds(ids));
    }
}
