package com.qing.dict.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.qing.dict.dto.community.CommentSaveDto;
import com.qing.dict.dto.community.CommunityDto;
import com.qing.dict.vo.community.CommentVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.dict.domain.CommunityComment;
import com.qing.dict.service.ICommunityCommentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 社区评论Controller
 * 
 * @author zzww
 * @date 2024-02-19
 */
@RestController
@RequestMapping("/dict/comment")
public class CommunityCommentController extends BaseController
{
    @Autowired
    private ICommunityCommentService communityCommentService;

    /**
     * 查询社区评论列表
     */
    @GetMapping("/list/{id}")
    public TableDataInfo list(@PathVariable("id") Long id)
    {
        startPage();
        List<CommentVo> list = communityCommentService.selectCommentVoList(id);
        return getDataTable(list);
    }

    /**
     * 添加评论
     */
    @PostMapping("/save-comment")
    public AjaxResult saveComment(@RequestBody Map<String,Object> params)
    {
        CommentSaveDto commentSaveDto = JSONObject.parseObject(JSON.toJSONString(params), CommentSaveDto.class);

        return toAjax(communityCommentService.saveCommunityComment(commentSaveDto));
    }

    /**
     * 导出社区评论列表
     */
    @PreAuthorize("@ss.hasPermi('dict:comment:export')")
    @Log(title = "社区评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CommunityComment communityComment)
    {
        List<CommunityComment> list = communityCommentService.selectCommunityCommentList(communityComment);
        ExcelUtil<CommunityComment> util = new ExcelUtil<CommunityComment>(CommunityComment.class);
        util.exportExcel(response, list, "社区评论数据");
    }

    /**
     * 获取社区评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:comment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(communityCommentService.selectCommunityCommentById(id));
    }

    /**
     * 新增社区评论
     */
    @PreAuthorize("@ss.hasPermi('dict:comment:add')")
    @Log(title = "社区评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CommunityComment communityComment)
    {
        return toAjax(communityCommentService.insertCommunityComment(communityComment));
    }

    /**
     * 修改社区评论
     */
    @PreAuthorize("@ss.hasPermi('dict:comment:edit')")
    @Log(title = "社区评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CommunityComment communityComment)
    {
        return toAjax(communityCommentService.updateCommunityComment(communityComment));
    }

    /**
     * 删除社区评论
     */
    @PreAuthorize("@ss.hasPermi('dict:comment:remove')")
    @Log(title = "社区评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(communityCommentService.deleteCommunityCommentByIds(ids));
    }
}
