package com.qing.dict.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.dict.domain.Dict;
import com.qing.dict.service.IDictService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据字典Controller
 * 
 * @author zzww
 * @date 2024-02-08
 */
@RestController
@RequestMapping("/dict/dict")
public class DictController extends BaseController
{
    @Autowired
    private IDictService dictService;

    /**
     * 查询组织架构列表
     */
    @PreAuthorize("@ss.hasPermi('dict:dict:list')")
    @GetMapping("/list")
    public TableDataInfo list(Dict dict)
    {
        startPage();
        List<Dict> list = dictService.selectDictList(dict);
        return getDataTable(list);
    }

    /**
     * 导出组织架构列表
     */
    @PreAuthorize("@ss.hasPermi('dict:dict:export')")
    @Log(title = "组织架构", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Dict dict)
    {
        List<Dict> list = dictService.selectDictList(dict);
        ExcelUtil<Dict> util = new ExcelUtil<Dict>(Dict.class);
        util.exportExcel(response, list, "组织架构数据");
    }

    /**
     * 获取组织架构详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:dict:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dictService.selectDictById(id));
    }

    /**
     * 新增组织架构
     */
    @PreAuthorize("@ss.hasPermi('dict:dict:add')")
    @Log(title = "组织架构", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Dict dict)
    {
        return toAjax(dictService.insertDict(dict));
    }

    /**
     * 修改组织架构
     */
    @PreAuthorize("@ss.hasPermi('dict:dict:edit')")
    @Log(title = "组织架构", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Dict dict)
    {
        return toAjax(dictService.updateDict(dict));
    }

    /**
     * 删除组织架构
     */
    @PreAuthorize("@ss.hasPermi('dict:dict:remove')")
    @Log(title = "组织架构", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dictService.deleteDictByIds(ids));
    }
}
