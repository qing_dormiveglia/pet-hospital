package com.qing.dict.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.qing.dict.vo.community.CommunityImageVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.dict.domain.CommunityImage;
import com.qing.dict.service.ICommunityImageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 社区图集Controller
 * 
 * @author zzww
 * @date 2024-02-19
 */
@RestController
@RequestMapping("/dict/image")
public class CommunityImageController extends BaseController
{
    @Autowired
    private ICommunityImageService communityImageService;

    /**
     * 查询社区图集列表
     */
    @GetMapping("/top/list")
    public AjaxResult topList()
    {
        List<CommunityImageVo> list = communityImageService.findTopList();
        return AjaxResult.success(list);
    }

    /**
     * 查询社区图集列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CommunityImage communityImage)
    {
        startPage();
        List<CommunityImage> list = communityImageService.selectCommunityImageList(communityImage);
        return getDataTable(list);
    }

    /**
     * 导出社区图集列表
     */
    @PreAuthorize("@ss.hasPermi('dict:image:export')")
    @Log(title = "社区图集", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CommunityImage communityImage)
    {
        List<CommunityImage> list = communityImageService.selectCommunityImageList(communityImage);
        ExcelUtil<CommunityImage> util = new ExcelUtil<CommunityImage>(CommunityImage.class);
        util.exportExcel(response, list, "社区图集数据");
    }

    /**
     * 获取社区图集详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:image:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(communityImageService.selectCommunityImageById(id));
    }

    /**
     * 新增社区图集
     */
    @PreAuthorize("@ss.hasPermi('dict:image:add')")
    @Log(title = "社区图集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CommunityImage communityImage)
    {
        return toAjax(communityImageService.insertCommunityImage(communityImage));
    }

    /**
     * 修改社区图集
     */
    @PreAuthorize("@ss.hasPermi('dict:image:edit')")
    @Log(title = "社区图集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CommunityImage communityImage)
    {
        return toAjax(communityImageService.updateCommunityImage(communityImage));
    }

    /**
     * 删除社区图集
     */
    @PreAuthorize("@ss.hasPermi('dict:image:remove')")
    @Log(title = "社区图集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(communityImageService.deleteCommunityImageByIds(ids));
    }
}
