package com.qing.dict.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.qing.dict.dto.community.CommunityDto;
import com.qing.dict.dto.community.CommunitySaveDto;
import com.qing.dict.dto.community.CommunityUpdateDto;
import com.qing.dict.vo.community.CommunityVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.dict.domain.Community;
import com.qing.dict.service.ICommunityService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 帖子Controller
 * 
 * @author zzww
 * @date 2024-02-19
 */
@RestController
@RequestMapping("/dict/community")
public class CommunityController extends BaseController
{
    @Autowired
    private ICommunityService communityService;



    /**
     * 删除帖子
     */
    @GetMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id)
    {
        return toAjax(communityService.deleteById(id));
    }


    /**
     * 获取帖子详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        CommunityVo communityVo = communityService.selectCommunityVoById(id);
        return success(communityVo);
    }

    /**
     * 查看我的帖子列表
     */
    @GetMapping("/list-my")
    public TableDataInfo findTopCommunityMyList()
    {
        startPage();
//        CommunityDto communityDto = JSONObject.parseObject(JSON.toJSONString(params), CommunityDto.class);
        return communityService.findCommunityVoMyList();
    }

    /**
     * 查询帖子列表
     */
    @GetMapping("/list")
    public TableDataInfo findTopCommunityList(@RequestParam Map<String,Object> params)
    {
        startPage();
        CommunityDto communityDto = JSONObject.parseObject(JSON.toJSONString(params), CommunityDto.class);
        return communityService.findCommunityVoList(communityDto);
    }

    /**
     * 新增帖子
     */
    @PostMapping("/save-community")
    public AjaxResult save(@RequestBody Map<String,Object> params)
    {
        CommunitySaveDto communitySaveDto = JSONObject.parseObject(JSON.toJSONString(params), CommunitySaveDto.class);
        return toAjax(communityService.saveCommunity(communitySaveDto));
    }



    /**
     * 导出帖子列表
     */
    @PreAuthorize("@ss.hasPermi('dict:community:export')")
    @Log(title = "帖子", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Community community)
    {
        List<Community> list = communityService.selectCommunityList(community);
        ExcelUtil<Community> util = new ExcelUtil<Community>(Community.class);
        util.exportExcel(response, list, "帖子数据");
    }


    /**
     * 新增帖子
     */
    @PreAuthorize("@ss.hasPermi('dict:community:add')")
    @Log(title = "帖子", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Community community)
    {
        return toAjax(communityService.insertCommunity(community));
    }

    /**
     * 修改帖子
     */
//    @PreAuthorize("@ss.hasPermi('dict:community:edit')")
//    @Log(title = "帖子", businessType = BusinessType.UPDATE)
    @PostMapping("/update-community")
    public AjaxResult edit(@RequestBody Map<String,Object> params)
    {
        params.remove("createTime");
        params.remove("updateTime");
        CommunityUpdateDto parseObject = JSONObject.parseObject(JSON.toJSONString(params), CommunityUpdateDto.class);
        return toAjax(communityService.updateCommunityDto(parseObject));
    }

    /**
     * 删除帖子
     */
    @PreAuthorize("@ss.hasPermi('dict:community:remove')")
    @Log(title = "帖子", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(communityService.deleteCommunityByIds(ids));
    }
}
