package com.qing.dict.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.dict.domain.PetMap;
import com.qing.dict.service.IPetMapService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 地图Controller
 * 
 * @author zzww
 * @date 2024-02-08
 */
@RestController
@RequestMapping("/dict/map")
public class PetMapController extends BaseController
{
    @Autowired
    private IPetMapService petMapService;

    /**
     * 查询地图列表
     */
    @PreAuthorize("@ss.hasPermi('dict:map:list')")
    @GetMapping("/list")
    public TableDataInfo list(PetMap petMap)
    {
        startPage();
        List<PetMap> list = petMapService.selectPetMapList(petMap);
        return getDataTable(list);
    }

    /**
     * 导出地图列表
     */
    @PreAuthorize("@ss.hasPermi('dict:map:export')")
    @Log(title = "地图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PetMap petMap)
    {
        List<PetMap> list = petMapService.selectPetMapList(petMap);
        ExcelUtil<PetMap> util = new ExcelUtil<PetMap>(PetMap.class);
        util.exportExcel(response, list, "地图数据");
    }

    /**
     * 获取地图详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:map:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(petMapService.selectPetMapById(id));
    }

    /**
     * 新增地图
     */
    @PreAuthorize("@ss.hasPermi('dict:map:add')")
    @Log(title = "地图", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PetMap petMap)
    {
        return toAjax(petMapService.insertPetMap(petMap));
    }

    /**
     * 修改地图
     */
    @PreAuthorize("@ss.hasPermi('dict:map:edit')")
    @Log(title = "地图", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PetMap petMap)
    {
        return toAjax(petMapService.updatePetMap(petMap));
    }

    /**
     * 删除地图
     */
    @PreAuthorize("@ss.hasPermi('dict:map:remove')")
    @Log(title = "地图", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(petMapService.deletePetMapByIds(ids));
    }
}
