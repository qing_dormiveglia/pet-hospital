package com.qing.dict.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.dict.domain.CommunityTag;
import com.qing.dict.service.ICommunityTagService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 社区标签Controller
 * 
 * @author zzww
 * @date 2024-02-19
 */
@RestController
@RequestMapping("/dict/tag")
public class CommunityTagController extends BaseController
{
    @Autowired
    private ICommunityTagService communityTagService;

    /**
     * 查询社区标签列表
     */
    @PreAuthorize("@ss.hasPermi('dict:tag:list')")
    @GetMapping("/list")
    public TableDataInfo list(CommunityTag communityTag)
    {
        startPage();
        List<CommunityTag> list = communityTagService.selectCommunityTagList(communityTag);
        return getDataTable(list);
    }

    /**
     * 导出社区标签列表
     */
    @PreAuthorize("@ss.hasPermi('dict:tag:export')")
    @Log(title = "社区标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CommunityTag communityTag)
    {
        List<CommunityTag> list = communityTagService.selectCommunityTagList(communityTag);
        ExcelUtil<CommunityTag> util = new ExcelUtil<CommunityTag>(CommunityTag.class);
        util.exportExcel(response, list, "社区标签数据");
    }

    /**
     * 获取社区标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:tag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(communityTagService.selectCommunityTagById(id));
    }

    /**
     * 新增社区标签
     */
    @PreAuthorize("@ss.hasPermi('dict:tag:add')")
    @Log(title = "社区标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CommunityTag communityTag)
    {
        return toAjax(communityTagService.insertCommunityTag(communityTag));
    }

    /**
     * 修改社区标签
     */
    @PreAuthorize("@ss.hasPermi('dict:tag:edit')")
    @Log(title = "社区标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CommunityTag communityTag)
    {
        return toAjax(communityTagService.updateCommunityTag(communityTag));
    }

    /**
     * 删除社区标签
     */
    @PreAuthorize("@ss.hasPermi('dict:tag:remove')")
    @Log(title = "社区标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(communityTagService.deleteCommunityTagByIds(ids));
    }
}
