package com.qing.dict.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.qing.dict.vo.community.CommunityTypeVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.qing.dict.domain.CommunityType;
import com.qing.dict.service.ICommunityTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 社区类型Controller
 * 
 * @author zzww
 * @date 2024-02-19
 */
@RestController
@RequestMapping("/dict/community/type")
public class CommunityTypeController extends BaseController
{
    @Autowired
    private ICommunityTypeService communityTypeService;

    /**
     * 查询社区类型列表
     */
    @GetMapping("/list")
    public AjaxResult list()
    {
        List<CommunityTypeVo> list = communityTypeService.selectCommunityTypeVoList();
        return AjaxResult.success(list);
    }

    /**
     * 导出社区类型列表
     */
    @PreAuthorize("@ss.hasPermi('dict:type:export')")
    @Log(title = "社区类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CommunityType communityType)
    {
        List<CommunityType> list = communityTypeService.selectCommunityTypeList(communityType);
        ExcelUtil<CommunityType> util = new ExcelUtil<CommunityType>(CommunityType.class);
        util.exportExcel(response, list, "社区类型数据");
    }

    /**
     * 获取社区类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('dict:type:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(communityTypeService.selectCommunityTypeById(id));
    }

    /**
     * 新增社区类型
     */
    @PreAuthorize("@ss.hasPermi('dict:type:add')")
    @Log(title = "社区类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CommunityType communityType)
    {
        return toAjax(communityTypeService.insertCommunityType(communityType));
    }

    /**
     * 修改社区类型
     */
    @PreAuthorize("@ss.hasPermi('dict:type:edit')")
    @Log(title = "社区类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CommunityType communityType)
    {
        return toAjax(communityTypeService.updateCommunityType(communityType));
    }

    /**
     * 删除社区类型
     */
    @PreAuthorize("@ss.hasPermi('dict:type:remove')")
    @Log(title = "社区类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(communityTypeService.deleteCommunityTypeByIds(ids));
    }
}
