package com.qing.dict;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author zzww
 * @Date 2024/2/8 0:46
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@MapperScan("com.qing.**.mapper")
@EnableDubbo(scanBasePackages = "com.qing")
@ServletComponentScan
public class Dict8301 {
    public static void main(String[] args) {
        SpringApplication.run(Dict8301.class,args);
    }
}
