package com.qing.dict.mapper;

import java.util.List;
import com.qing.dict.domain.PetMap;

/**
 * 地图Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface PetMapMapper 
{
    /**
     * 查询地图
     * 
     * @param id 地图主键
     * @return 地图
     */
    public PetMap selectPetMapById(Long id);

    /**
     * 查询地图列表
     * 
     * @param petMap 地图
     * @return 地图集合
     */
    public List<PetMap> selectPetMapList(PetMap petMap);

    /**
     * 新增地图
     * 
     * @param petMap 地图
     * @return 结果
     */
    public int insertPetMap(PetMap petMap);

    /**
     * 修改地图
     * 
     * @param petMap 地图
     * @return 结果
     */
    public int updatePetMap(PetMap petMap);

    /**
     * 删除地图
     * 
     * @param id 地图主键
     * @return 结果
     */
    public int deletePetMapById(Long id);

    /**
     * 批量删除地图
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePetMapByIds(Long[] ids);
}
