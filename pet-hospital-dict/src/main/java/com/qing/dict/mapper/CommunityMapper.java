package com.qing.dict.mapper;

import java.util.List;
import com.qing.dict.domain.Community;
import com.qing.dict.vo.community.CommunityVo;

/**
 * 帖子Mapper接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface CommunityMapper 
{
    /**
     * 查询帖子
     * 
     * @param id 帖子主键
     * @return 帖子
     */
    public Community selectCommunityById(Long id);

    /**
     * 查询帖子列表
     * 
     * @param community 帖子
     * @return 帖子集合
     */
    public List<Community> selectCommunityList(Community community);

    /**
     * 新增帖子
     * 
     * @param community 帖子
     * @return 结果
     */
    public int insertCommunity(Community community);

    /**
     * 修改帖子
     * 
     * @param community 帖子
     * @return 结果
     */
    public int updateCommunity(Community community);

    /**
     * 删除帖子
     * 
     * @param id 帖子主键
     * @return 结果
     */
    public int deleteCommunityById(Long id);

    /**
     * 批量删除帖子
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCommunityByIds(Long[] ids);

    /**
     * 获取帖子列表
     * @param id
     * @return
     */
    List<CommunityVo> findCommunityVoList(Long id);
}
