package com.qing.dict.mapper;

import java.util.List;
import com.qing.dict.domain.CommunityType;

/**
 * 社区类型Mapper接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface CommunityTypeMapper 
{
    /**
     * 查询社区类型
     * 
     * @param id 社区类型主键
     * @return 社区类型
     */
    public CommunityType selectCommunityTypeById(Long id);

    /**
     * 查询社区类型列表
     * 
     * @param communityType 社区类型
     * @return 社区类型集合
     */
    public List<CommunityType> selectCommunityTypeList(CommunityType communityType);

    /**
     * 新增社区类型
     * 
     * @param communityType 社区类型
     * @return 结果
     */
    public int insertCommunityType(CommunityType communityType);

    /**
     * 修改社区类型
     * 
     * @param communityType 社区类型
     * @return 结果
     */
    public int updateCommunityType(CommunityType communityType);

    /**
     * 删除社区类型
     * 
     * @param id 社区类型主键
     * @return 结果
     */
    public int deleteCommunityTypeById(Long id);

    /**
     * 批量删除社区类型
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCommunityTypeByIds(Long[] ids);
}
