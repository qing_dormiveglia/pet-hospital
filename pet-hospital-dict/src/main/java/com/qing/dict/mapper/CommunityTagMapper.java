package com.qing.dict.mapper;

import java.util.List;
import com.qing.dict.domain.CommunityTag;
import com.qing.dict.vo.community.CommunityTagVo;

/**
 * 社区标签Mapper接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface CommunityTagMapper 
{
    /**
     * 查询社区标签
     * 
     * @param id 社区标签主键
     * @return 社区标签
     */
    public CommunityTag selectCommunityTagById(Long id);

    /**
     * 查询社区标签列表
     * 
     * @param communityTag 社区标签
     * @return 社区标签集合
     */
    public List<CommunityTag> selectCommunityTagList(CommunityTag communityTag);

    /**
     * 新增社区标签
     * 
     * @param communityTag 社区标签
     * @return 结果
     */
    public int insertCommunityTag(CommunityTag communityTag);

    /**
     * 修改社区标签
     * 
     * @param communityTag 社区标签
     * @return 结果
     */
    public int updateCommunityTag(CommunityTag communityTag);

    /**
     * 删除社区标签
     * 
     * @param id 社区标签主键
     * @return 结果
     */
    public int deleteCommunityTagById(Long id);

    /**
     * 批量删除社区标签
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCommunityTagByIds(Long[] ids);

    /**
     * 批量获取标签列表
     * @param ids
     * @return
     */
    List<CommunityTagVo> selectCommunityTagBatchByIds(List<Long> ids);
}
