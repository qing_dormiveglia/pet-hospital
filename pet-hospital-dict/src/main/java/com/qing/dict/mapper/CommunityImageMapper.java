package com.qing.dict.mapper;

import java.util.List;
import com.qing.dict.domain.CommunityImage;
import com.qing.dict.vo.community.CommunityImageVo;

/**
 * 社区图集Mapper接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface CommunityImageMapper 
{
    /**
     * 查询社区图集
     * 
     * @param id 社区图集主键
     * @return 社区图集
     */
    public CommunityImage selectCommunityImageById(Long id);

    /**
     * 查询社区图集列表
     * 
     * @param communityImage 社区图集
     * @return 社区图集集合
     */
    public List<CommunityImage> selectCommunityImageList(CommunityImage communityImage);

    /**
     * 新增社区图集
     * 
     * @param communityImage 社区图集
     * @return 结果
     */
    public int insertCommunityImage(CommunityImage communityImage);

    /**
     * 修改社区图集
     * 
     * @param communityImage 社区图集
     * @return 结果
     */
    public int updateCommunityImage(CommunityImage communityImage);

    /**
     * 删除社区图集
     * 
     * @param id 社区图集主键
     * @return 结果
     */
    public int deleteCommunityImageById(Long id);

    /**
     * 批量删除社区图集
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCommunityImageByIds(Long[] ids);

    /**
     * 批量获取社区图集
     * @param ids
     * @return
     */
    List<CommunityImageVo> selectCommunityImageBatchByIds(List<Long> ids);
}
