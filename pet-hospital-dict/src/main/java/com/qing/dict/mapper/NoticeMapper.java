package com.qing.dict.mapper;

import java.util.List;
import com.qing.dict.domain.Notice;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface NoticeMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Notice selectNoticeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param notice 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Notice> selectNoticeList(Notice notice);

    /**
     * 新增【请填写功能名称】
     * 
     * @param notice 【请填写功能名称】
     * @return 结果
     */
    public int insertNotice(Notice notice);

    /**
     * 修改【请填写功能名称】
     * 
     * @param notice 【请填写功能名称】
     * @return 结果
     */
    public int updateNotice(Notice notice);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteNoticeById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteNoticeByIds(Long[] ids);
}
