package com.qing.dict.mapper;

import java.util.List;
import com.qing.dict.domain.CommunityComment;

/**
 * 社区评论Mapper接口
 * 
 * @author zzww
 * @date 2024-02-19
 */
public interface CommunityCommentMapper 
{
    /**
     * 查询社区评论
     * 
     * @param id 社区评论主键
     * @return 社区评论
     */
    public CommunityComment selectCommunityCommentById(Long id);

    /**
     * 查询社区评论列表
     * 
     * @param communityComment 社区评论
     * @return 社区评论集合
     */
    public List<CommunityComment> selectCommunityCommentList(CommunityComment communityComment);

    /**
     * 新增社区评论
     * 
     * @param communityComment 社区评论
     * @return 结果
     */
    public int insertCommunityComment(CommunityComment communityComment);

    /**
     * 修改社区评论
     * 
     * @param communityComment 社区评论
     * @return 结果
     */
    public int updateCommunityComment(CommunityComment communityComment);

    /**
     * 删除社区评论
     * 
     * @param id 社区评论主键
     * @return 结果
     */
    public int deleteCommunityCommentById(Long id);

    /**
     * 批量删除社区评论
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCommunityCommentByIds(Long[] ids);
}
