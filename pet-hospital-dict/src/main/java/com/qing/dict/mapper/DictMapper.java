package com.qing.dict.mapper;

import java.util.List;
import com.qing.dict.domain.Dict;

/**
 * 组织架构Mapper接口
 * 
 * @author zzww
 * @date 2024-02-08
 */
public interface DictMapper 
{
    /**
     * 查询组织架构
     * 
     * @param id 组织架构主键
     * @return 组织架构
     */
    public Dict selectDictById(Long id);

    /**
     * 查询组织架构列表
     * 
     * @param dict 组织架构
     * @return 组织架构集合
     */
    public List<Dict> selectDictList(Dict dict);

    /**
     * 新增组织架构
     * 
     * @param dict 组织架构
     * @return 结果
     */
    public int insertDict(Dict dict);

    /**
     * 修改组织架构
     * 
     * @param dict 组织架构
     * @return 结果
     */
    public int updateDict(Dict dict);

    /**
     * 删除组织架构
     * 
     * @param id 组织架构主键
     * @return 结果
     */
    public int deleteDictById(Long id);

    /**
     * 批量删除组织架构
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDictByIds(Long[] ids);
}
