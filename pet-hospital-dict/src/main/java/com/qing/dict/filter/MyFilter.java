package com.qing.dict.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

/**
 * @Author zzww
 * @Date 2024/5/16 4:26
 */
@Configuration
public class MyFilter
{
    @Bean
    public FilterRegistrationBean<Filter> xssFilter() {
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new XssFilter());
        registrationBean.addUrlPatterns("/*"); // 设置过滤器的URL模式
        registrationBean.setOrder(1); // 设置过滤器的顺序
        return registrationBean;
    }
}
