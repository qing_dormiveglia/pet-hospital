package com.qing.dict.filter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.AntPathMatcher;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.FilterConfig;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author lihua
 * @date 2023/2/21 11:49
 * 配置XSS过滤器
 **/
@WebFilter(urlPatterns = "/*")
//@Configuration
public class XssFilter implements Filter {
    /**
     * 排除链接
     */
    public List<String> excludes = new ArrayList<>();


    /**
     * init初始化方法在系统启动前运行
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //获取FilterConfig中配置的排除项
        String tempExcludes = filterConfig.getInitParameter("excludes");
        if (StringUtils.isNotEmpty(tempExcludes)) {
            String[] url = tempExcludes.split(",");
            Collections.addAll(excludes, url);
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        //排除url处理逻辑
        if (handleExcludeURL(req, resp)) {
            chain.doFilter(request, response);
            return;
        }
        //XSS处理策略
        XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper((HttpServletRequest) request);
        chain.doFilter(xssRequest, response);

    }

    /**
     * 排除url处理逻辑
     * @param request
     * @param response
     * @return
     */
    private boolean handleExcludeURL(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getServletPath();
        String method = request.getMethod();
        // GET DELETE 不过滤
//        if (method == null || method.matches("GET") || method.matches("DELETE")) {
//            return true;
//        }
        return matches(url, excludes);
    }

    @Override
    public void destroy() {

    }

    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     *
     * @param str  指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
    public static boolean matches(String str, List<String> strs) {

        AntPathMatcher antPathMatcher = new AntPathMatcher();
        if (Objects.isNull(str) || str.isEmpty() || Objects.isNull(strs) || strs.isEmpty()) {
            return false;
        }
        for (String pattern : strs) {
            if (antPathMatcher.match(pattern, str)) {
                return true;
            }
        }
        return false;
    }
}
