package com.qing.dict.vo.city;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/8 16:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityTreeListVo {

    /** 城市表ID */
    private Long id;

    /** 名称 */
    private String cityName;

    /** 值 */
    private String keyword;
}
