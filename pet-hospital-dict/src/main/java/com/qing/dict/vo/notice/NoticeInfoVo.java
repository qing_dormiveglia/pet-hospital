package com.qing.dict.vo.notice;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/2/8 17:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeInfoVo<T> {
    /** 主建 */
    private Long id;

    /** 发布人信息 */
    private T t;

    /** 标题 */
    private String title;

    /** 副标题 */
    private String subtitle;

    /** 内容 */
    private String content;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date createTime;
}
