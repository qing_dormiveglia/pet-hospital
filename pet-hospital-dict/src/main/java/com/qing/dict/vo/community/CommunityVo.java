package com.qing.dict.vo.community;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qing.common.to.MemberTo;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @Author zzww
 * @Date 2024/2/19 17:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommunityVo
{
    /** id */
    private Long id;

    /** 用户*/
    private MemberTo member;

    /** 类型id */
    private Long communityTypeId;

    /** 转发数 */
    private Long transpond;

    /** 评论数 */
    private Long comment;

    /** 浏览量 */
    private Long browse;

    /** 点赞数 */
    private Long upvote;

    /** 图集id（逗号隔开） */
    private List<String> imageVos;

    private List<CommunityImageVo> imageVoList;

    /** 帖子信息 */
    private String content;

    /** 标签*/
    private List<CommunityTagVo> tagVoList;

    /** 地址名称 */
    private String mapAddress;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date createTime;

}
