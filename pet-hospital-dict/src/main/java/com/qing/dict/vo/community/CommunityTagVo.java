package com.qing.dict.vo.community;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/19 18:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommunityTagVo {
    /** id */
    private Long id;

    /** 标签名称 */
    private String name;

    /** 描述 */
    private String comment;

    /** 排序 */
    private Long num;
}
