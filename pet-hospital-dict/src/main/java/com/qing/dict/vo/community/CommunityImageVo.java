package com.qing.dict.vo.community;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/19 17:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommunityImageVo
{
    /** id */
    private Long id;

    /** 名称 */
    private String name;

    /** 地址 */
    private String url;

    /** 排序 */
    private Long num;

    /** 类型 */
    private Integer type;
}
