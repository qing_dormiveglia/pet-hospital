package com.qing.dict.vo.notice;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author zzww
 * @Date 2024/2/8 17:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeVo {

    /** 主建 */
    private Long id;

    /** 标题 */
    private String title;

    /** 副标题 */
    private String subtitle;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date createTime;
}
