package com.qing.dict.vo.community;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/19 16:52
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommunityTypeVo {

    /** id */
    private Long id;

    /** 类型名称 */
    private String name;
}
