package com.qing.dict.vo.city;

import com.qing.dict.domain.City;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/2/8 16:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityTreeVo {

    /** 标题（A B...） */
    private String letter;
    /** 城市列表 */
    private List<CityTreeListVo> cityList;

}
