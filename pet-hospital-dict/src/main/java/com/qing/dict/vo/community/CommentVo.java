package com.qing.dict.vo.community;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qing.common.to.MemberTo;
import com.qing.dict.domain.CommunityComment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @Author zzww
 * @Date 2024/2/20 13:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentVo
{
    /** id */
    private Long id;

    /** 帖子id */
    private Long communityId;

    /** 评论id(用户id) */
    private Long commentId;

    /** 上级id */
    private Long pId;

    /** 用户信息 */
    private MemberTo member;

    /** 评论用户信息 */
    private MemberTo commentMember;

    private CommunityComment communityCommentTo;

    /** 图集id（逗号隔开） */
    private List<String> images;

    /** 0-一级 1-二级 */
    private List<CommentVo> childrenList;

    /** 类型（0-作者） */
    private Integer type;

    /** 点赞数 */
    private Long upvote;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date createTime;

    /** 评论 */
    private String coment;
}
