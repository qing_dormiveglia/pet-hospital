package com.qing.dict.dto.community;

import lombok.Data;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/5/5 16:58
 */
@Data
public class CommunitySaveDto
{
    private Long communityTypeId;
    private List<String> images;
    private String content;
}
