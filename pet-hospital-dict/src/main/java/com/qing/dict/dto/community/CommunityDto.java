package com.qing.dict.dto.community;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zzww
 * @Date 2024/2/19 17:45
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class CommunityDto {

    private Long typeId;
//    community_type_id
}
