package com.qing.dict.dto.community;

import lombok.Data;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/5/5 19:35
 */
@Data
public class CommentSaveDto
{
    private Long communityId;
    private Long pId;
    private Long commentId;
    private List<String> images;
    private String coment;
    private Long authorMemberId;
}
