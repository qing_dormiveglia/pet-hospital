package com.qing.dict.dto.community;

import com.qing.dict.domain.Community;
import lombok.Data;

import java.util.List;

/**
 * @Author zzww
 * @Date 2024/5/6 11:05
 */
@Data
public class CommunityUpdateDto extends Community
{
    private List<String> images;
}
